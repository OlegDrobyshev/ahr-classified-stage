<?php

namespace App\Events;

use App\Events\Event;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

/**
 * Class SubscriptionPaymentAttempt
 * @package App\Events
 */
class SubscriptionPaymentAttempt extends Event
{
    use SerializesModels;

    /**
     * @var
     */
    public $details;

    /**
     * @var
     */
    public $user;

    /**
     * @param $details
     * @param $user
     */
    public function __construct($details, $user)
    {
        $this->details = $details;

        $this->user = $user;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
