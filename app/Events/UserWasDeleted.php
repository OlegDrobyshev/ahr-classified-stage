<?php

namespace App\Events;

use Classifieds\Users\User;
use Illuminate\Queue\SerializesModels;

/**
 * Class UserWasDeleted
 * @package App\Events
 */
class UserWasDeleted extends Event
{
    use SerializesModels;

    public $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Get the channels the event should be broadcast on.
     *
     * @return array
     */
    public function broadcastOn()
    {
        return [];
    }
}
