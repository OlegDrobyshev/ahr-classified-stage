<?php

function shouldDisplayContacts(Illuminate\Foundation\Auth\User $user)
{
    return $user->status == 1;
}

/**
 * @return bool|string
 */
function lastMonth()
{
    return date('Y-m-d', strtotime( '-1 month', time()));
}

/**
 * @return bool|string
 */
function lastWeek()
{
    return date('Y-m-d', strtotime( '-1 week', time()));
}

/**
 * @param \Illuminate\Foundation\Auth\User $user
 * @param bool $firstNameFirst
 * @return string
 */
function fullUserName(Illuminate\Foundation\Auth\User $user, $firstNameFirst = false)
{
    if ($firstNameFirst)
    {
        return $user->first_name . ' ' . $user->surname;
    }
    else
    {
        return $user->surname . ' ' . $user->first_name;
    }
}

/**
 * Checks if ad was saved in cookie for comparison
 * returns class "active" if true
 *
 * @param $adId
 * @return string
 */
function ifAdForComparison($adId)
{
    if ( ! isset($_COOKIE['compare'])) return false;

    $class = '';

    $compareList = $_COOKIE['compare'];

    $compareList = json_decode($compareList);

    if ($compareList)
    {
        if (in_array($adId, $compareList))
        {
            $class = 'active';
        }
    }

    return $class;
}

/**
 * @param $provider
 * @param $vid
 * @return bool|string
 */
function getVideoUrl($provider, $vid)
{
    switch ($provider)
    {
        case 'youtube':
            return 'https://www.youtube.com/watch?v=' . $vid;

        case 'vimeo':
            return 'https://vimeo.com/' . $vid;

        default: return false;
    }
}

/**
 * @param $uri
 * @param $name
 * @param string $disk
 * @return array|string
 */
function getThumbnail($uri, $name, $disk = 'public')
{
    $uri = explode('.', $uri);

    $thumbnailUri = $uri[0] . '-' . $name . '.' . $uri[1];

    if (file_exists(public_path($thumbnailUri)))
    {
        return $thumbnailUri;
    }

    return implode('.', $uri);
}

/**
 * Returns a file size limit in bytes based on the PHP
 * upload_max_filesize, post_max_size and memory_limit
 *
 * @return float
 */
function getFileUploadMaxSize()
{
    $maxUpload = (int) (ini_get('upload_max_filesize'));

    $maxPost = (int) (ini_get('post_max_size'));

    $memoryLimit = (int) (ini_get('memory_limit'));

    $fileUploadMaxSizeMB = min($maxUpload, $maxPost, $memoryLimit);

    return $fileUploadMaxSizeMB * 1024;
}

/**
 * Format size in bytes
 *
 * @param $bytes
 * @return string
 */
function formatSizeUnits($bytes)
{
    if ($bytes >= 1073741824)
    {
        $bytes = number_format($bytes / 1073741824, 2) . ' GB';
    }
    elseif ($bytes >= 1048576)
    {
        $bytes = number_format($bytes / 1048576, 2) . ' MB';
    }
    elseif ($bytes >= 1024)
    {
        $bytes = number_format($bytes / 1024, 2) . ' kB';
    }
    elseif ($bytes > 1)
    {
        $bytes = $bytes . ' bytes';
    }
    elseif ($bytes == 1)
    {
        $bytes = $bytes . ' byte';
    }
    else
    {
        $bytes = '0 bytes';
    }

    return $bytes;
}

/**
 * @param $start
 * @param $end
 * @return array
 */
function getSequence($start, $end)
{
    $sequence = [];

    for ($i = $start; $i <= $end; $i++)
    {
        $sequence[$i] = $i;
    }

    return $sequence;
}


/**
 * @param \Classifieds\Ads\Ad $ad
 * @return string
 */
function adLink(Classifieds\Ads\Ad $ad)
{
    return '/ad/' . $ad->ad_id . '/' . adSlug($ad);
}

/**
 * @param \Classifieds\Ads\Ad $ad
 * @return string
 */
function printAdLink(Classifieds\Ads\Ad $ad)
{
    return '/print-ad/' . $ad->ad_id . '/' . adSlug($ad);
}

/**
 * @param \Classifieds\Ads\Ad $ad
 * @return string
 */
function adTitle(Classifieds\Ads\Ad $ad)
{
    return $ad->year . ' ' . $ad->make->name . ' ' . $ad->model->name;
}

/**
 * @param \Classifieds\Ads\Ad $ad
 * @return string
 */
function adSlug(Classifieds\Ads\Ad $ad)
{
    $slug = $ad->year . '-' . $ad->make->name . '-' . $ad->model->name;

    return str_slug($slug);
}

/**
 * @param $key
 * @param $values
 * @param $data
 * @param string $separator
 * @return array
 */
function formatList($key, $values, $data, $separator = ' ')
{
    $list = [];

    foreach ($data as $item)
    {
        if (is_array($values))
        {
            $temp = [];

            foreach ($values as $value)
            {
                $temp[] = $item->{$value};
            }

            $result = implode($separator, $temp);
        }
        else
        {
            $result = $item->{$values};
        }

        $list[$item->{$key}] = $result;
    }

    return $list;
}