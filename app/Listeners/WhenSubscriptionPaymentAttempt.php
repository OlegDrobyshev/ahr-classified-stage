<?php

namespace App\Listeners;

use App\Events\SubscriptionPaymentAttempt;
use App\Jobs\SavePaymentDetails;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Auth;

/**
 * Class WhenSubscriptionPaymentAttempt
 * @package App\Listeners
 */
class WhenSubscriptionPaymentAttempt
{
    use DispatchesJobs;

    public function __construct()
    {
        //
    }

    /**
     * @param SubscriptionPaymentAttempt $event
     */
    public function handle(SubscriptionPaymentAttempt $event)
    {
        $payment = $this->buildPaymentData($event);

        $this->dispatchNow(new SavePaymentDetails($payment));
    }

    /**
     * @param SubscriptionPaymentAttempt $event
     * @return array
     */
    private function buildPaymentData(SubscriptionPaymentAttempt $event)
    {
        return [
            'user_id'    => $event->user->user_id,
            'plan_id'    => $event->details['plan_id'],
            'amount'     => $event->details['amount'],
            'status'     => $event->details['approved'],
            'gateway_id' => $event->details['gateway_id'],
        ];
    }
}
