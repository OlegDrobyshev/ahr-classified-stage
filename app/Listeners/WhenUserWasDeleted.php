<?php

namespace App\Listeners;

use App\Events\UserWasDeleted;
use App\Jobs\DeleteUserAds;
use Classifieds\Ads\AdRepositoryInterface;
use Classifieds\AdsPhotos\AdsPhotoRepositoryInterface;
use Illuminate\Contracts\Bus\Dispatcher;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Class WhenUserWasDeleted
 * @package App\Listeners
 */
class WhenUserWasDeleted
{
    public $adRepo;

    private $dispatcher;

    public function __construct(Dispatcher $dispatcher,
                                AdRepositoryInterface $adRepo)
    {

        $this->adRepo = $adRepo;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param UserWasDeleted $event
     */
    public function handle(UserWasDeleted $event)
    {
        $this->dispatcher->dispatchNow(new DeleteUserAds($event->user));
    }
}
