<?php

namespace App\Listeners;

use App\Events\NewAdWasPosted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewAdPostedTest
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewAdWasPosted  $event
     * @return void
     */
    public function handle(NewAdWasPosted $event)
    {
//        dd($event);
    }
}
