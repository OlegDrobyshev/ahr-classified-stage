<?php  namespace Classifieds\AdsPhotos;

/**
 * Interface AdsPhotoRepositoryInterface
 * @package Classifieds\AdsPhotos
 */
interface AdsPhotoRepositoryInterface
{
    public function getAll();
    public function getByAdId($adId);
    public function getById($adPhotoId);
    public function create($data);
    public function update($adPhotoId, $data);
    public function delete($adPhotoId);
}