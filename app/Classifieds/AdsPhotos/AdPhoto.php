<?php

namespace Classifieds\AdsPhotos;

use Illuminate\Database\Eloquent\Model;

/**
 * Class AdPhoto
 * @package Classifieds\AdsPhotos
 */
class AdPhoto extends Model
{

    /**
     * @var string
     */
    protected $table = 'ads_photos';

    /**
     * @var string
     */
    protected $primaryKey = 'photo_id';

    /**
     * @var array
     */
    protected $fillable = ['ad_id', 'desc', 'uri', 'mime', 'size'];
}
