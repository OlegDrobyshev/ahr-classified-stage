<?php  namespace Classifieds\AdsPhotos;

/**
 * Class EloquentAdsPhotoRepository
 * @package Classifieds\AdsPhotos
 */
class EloquentAdsPhotoRepository implements AdsPhotoRepositoryInterface
{
    private $model;

    function __construct(AdPhoto $model)
    {
        $this->model = $model;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAll()
    {
        return $this->model->all();
    }

    /**
     * @param $adId
     * @return mixed
     */
    public function getByAdId($adId)
    {
        return $this->model->whereAdId($adId)->orderBy('photo_id', 'asc')->get();
    }

    /**
     * @param $adPhotoId
     * @return mixed
     */
    public function getById($adPhotoId)
    {
        $method = is_array($adPhotoId) ? 'whereIn' : 'where';

        return $this->model->{$method}('photo_id', $adPhotoId)->get();
    }

    /**
     * @param $data
     * @return static
     */
    public function create($data)
    {
        return $this->model->create($data);
    }

    /**
     * @param $adPhotoId
     * @param $data
     * @return mixed
     */
    public function update($adPhotoId, $data)
    {
        return $this->model->wherePhotoId($adPhotoId)->update($data);
    }

    /**
     * @param $adPhotoId
     * @return mixed
     */
    public function delete($adPhotoId)
    {
        return $this->model->wherePhotoId($adPhotoId)->delete();
    }
}