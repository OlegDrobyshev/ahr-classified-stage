<?php  namespace Classifieds\DriveTypes;

/**
 * Class EloquentDriveTypeRepository
 * @package Classifieds\DriveTypes
 */
class EloquentDriveTypeRepository implements DriveTypeRepositoryInterface
{
    private $model;

    function __construct(DriveType $model)
    {
        $this->model = $model;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAll()
    {
        return $this->model->all();
    }
}