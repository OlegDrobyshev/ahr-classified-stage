<?php  namespace Classifieds\DriveTypes;

/**
 * Interface DriveTypeRepositoryInterface
 * @package Classifieds\DriveTypes
 */
interface DriveTypeRepositoryInterface
{
    public function getAll();
}