<?php

namespace Classifieds\DriveTypes;

use Illuminate\Database\Eloquent\Model;

/**
 * Class DriveType
 * @package Classifieds\DriveTypes
 */
class DriveType extends Model
{
    /**
     * @var string
     */
    protected $primaryKey = 'drive_type_id';

    /**
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * @var bool
     */
    public $timestamps = false;
}
