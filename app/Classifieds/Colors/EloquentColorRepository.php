<?php  namespace Classifieds\Colors;

/**
 * Class EloquentColorRepository
 * @package Classifieds\Colors
 */
class EloquentColorRepository implements  ColorRepositoryInterface
{
    private $model;

    function __construct(Color $model)
    {
        $this->model = $model;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAll()
    {
        return $this->model->all();
    }
}