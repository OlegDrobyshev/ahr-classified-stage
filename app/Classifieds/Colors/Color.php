<?php

namespace Classifieds\Colors;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Color
 * @package App\Classifieds\Colors
 */
class Color extends Model
{
    /**
     * @var string
     */
    protected $primaryKey = 'color_id';

    /**
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * @var bool
     */
    public $timestamps = false;
}
