<?php  namespace Classifieds\Colors;

/**
 * Interface ColorRepositoryInterface
 * @package Classifieds\Colors
 */
interface ColorRepositoryInterface
{
    public function getAll();
}