<?php  namespace Classifieds\Services; 

use Config;
use Intervention\Image\Facades\Image;
use Storage;

/**
 * Class AdMediaService
 * @package Classifieds\Services
 */
class AdMediaService
{

    /**
     * @param $provider
     * @param $vid
     * @param $directory
     * @return string
     */
    public function saveVideoThumbnail($provider, $vid, $directory)
    {
        $thumbnail = $this->getVideoThumbnail($provider, $vid);

        $fileName = $this->generateFileName();

        $storagePath = "{$directory}/{$fileName}.jpg";

        Storage::disk('public')->put($storagePath, $thumbnail);

        $this->createThumbnails($directory, $fileName, 'jpg');

        return [
            'provider' => $provider,
            'vid' => $vid,
            'url' => getVideoUrl($provider, $vid),
            'image' => '/storage/' . $storagePath
        ];
    }

    /**
     * @param $provider
     * @param $vid
     * @return bool|string
     */
    public function getVideoThumbnail($provider, $vid)
    {
        switch ($provider)
        {
            case 'youtube':
                return file_get_contents("http://img.youtube.com/vi/{$vid}/0.jpg");

            case 'vimeo':
                $json = file_get_contents("http://vimeo.com/api/v2/video/{$vid}.json");
                $data = json_decode($json);

                return file_get_contents($data[0]->thumbnail_large);

            default: return false;
        }
    }

    /**
     * @param $input
     * @param $inputName
     * @param $dir
     * @return string
     */
    public function uploadPhotos($input, $inputName, $dir)
    {
        $result = ['uploaded' => [], 'message' => ''];

        $totalFiles = count($input[$inputName]);

        $badFiles = 0;

        foreach($input[$inputName] as $key => $file)
        {
            if ($this->validateImage($input, $inputName, $key))
            {
                $badFiles++; continue;
            }

            list($name, $ext) = $this->uploadImage($file, $dir);

            $this->createThumbnails($dir, $name, $ext);

            $result['uploaded'][] = [
                'uri' => '/storage/' . $dir . $name . '.' . $ext,
                'mime' => $file->getClientMimeType(),
                'size' => $file->getClientSize(),
                'thumbnail' => '/storage/' . $dir . $name . '-small.' . $ext
            ];
        }

        $result['message'] = $this->getResultMessage($totalFiles, $badFiles);

        return $result;
    }

    /**
     * @param $input
     * @param $fileInputName
     * @param $key
     * @return \Illuminate\Validation\Validator
     */
    private function validateImage($input, $fileInputName, $key)
    {
        $validation = \Validator::make($input, [
            $fileInputName . '.' . $key => 'required|image|max:' . getFileUploadMaxSize()
        ]);

        return $validation->fails();
    }

    /**
     * @param $file
     * @param $directory
     * @return array
     */
    private function uploadImage($file, $directory)
    {
        $extension = $file->getClientOriginalExtension();

        $newFileName = $this->generateFileName();

        $storagePath = "{$directory}/{$newFileName}.{$extension}";

        Storage::disk('public')->put($storagePath, file_get_contents($file));

        return [$newFileName, $extension];
    }

    /**
     * @param $dir
     * @param $name
     * @param $ext
     */
    public function createThumbnails($dir, $name, $ext)
    {
        $thumbnailsSizes = Config::get('image.thumbnails_sizes');

        foreach ($thumbnailsSizes as $thumbName => $size)
        {
            // Create image object from original file
            $originalFilePath = "{$dir}/{$name}.{$ext}";

            $originalFile = Storage::disk('public')->get($originalFilePath);

            $image = Image::make($originalFile);

            $this->createThumbnail($image, $size);

            $this->addWaterMark($image, $size);

            $canvas = $this->putToCanvas($image, $size);

            // Save thumbnail
            $thumbnailFileName = $name . '-' . $thumbName . '.' . $ext;

            $path = public_path('storage/' . $dir . $thumbnailFileName);

            $canvas->save($path, 70);
        }
    }

    /**
     * @param $image
     * @param $size
     * @return mixed
     */
    private function createThumbnail($image, $size)
    {
        $newHeight = ($size['width'] * $image->height()) / $image->width();

        if ($newHeight > $size['height'])
        {
            $image->resize(null, $size['height'], function ($constraint)
            {
                $constraint->aspectRatio();

                $constraint->upsize();
            });
        }
        else
        {
            $image->resize($size['width'], null, function ($constraint)
            {
                $constraint->aspectRatio();

                $constraint->upsize();
            });
        }

        return $image;
    }

    /**
     * @param $image
     * @param $size
     * @return mixed
     */
    private function addWaterMark($image, $size)
    {
        $wmWidth = round($size['width'] / 100 * 8);

        $wmMargin = round($size['width'] / 100 * 2);

        $watermark = Image::make($this->getWaterMakPath());

        $watermark->resize($wmWidth, null, function ($constraint)
        {
            $constraint->aspectRatio();
        });

        return $image->insert($watermark, 'bottom-right', $wmMargin, $wmMargin);
    }

    /**
     * @param $image
     * @param $size
     * @return mixed
     */
    private function putToCanvas($image, $size)
    {
        $canvas = Image::canvas($size['width'], $size['height'], '#fff');
//        $canvas = Image::make(public_path(Config::get('image.canvas-bg')))->resize($size['width'], $size['height']);

        return $canvas->insert($image, 'center');
    }


    /**
     * @param $totalFiles
     * @param $badFiles
     * @return string
     */
    private function getResultMessage($totalFiles, $badFiles)
    {
        $totalUploaded = $totalFiles - $badFiles;

        return $badFiles
            ? "<b>{$totalUploaded}</b> of <b>{$totalFiles}</b> files were uploaded"
            : "<b>{$totalUploaded}</b> file(s) uploaded";
    }

    /**
     * @param string $string
     * @return string
     */
    private function generateFileName($string = '')
    {
        return md5(microtime() . $string);
    }

    /**
     * @return string
     */
    private function getWaterMakPath()
    {
        return public_path(Config::get('image.watermark'));
    }
}