<?php  namespace Classifieds\Services; 

use App\Jobs\SendPromotionEmails;
use Classifieds\Users\UserRepositoryInterface;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

/**
 * Class EmailsControllerService
 * @package Classifieds\Services
 */
class EmailsControllerService
{
    use DispatchesJobs;

    /**
     * @var UserRepositoryInterface
     */
    private $userRepo;

    /**
     * @param UserRepositoryInterface $userRepo
     */
    function __construct(UserRepositoryInterface $userRepo)
    {
        $this->userRepo = $userRepo;
    }

    /**
     * @return mixed
     */
    public function getTotalUsers()
    {
        return $this->userRepo->filter([
            'where' => [
                'type' => 'seller',
                'status' => [1, 2]
            ]
        ], true);
    }

    /**
     * @param $input
     * @return mixed
     */
    public function sendEmailToUsers($input)
    {
        $validation = $this->_validateEmailForm($input);

        if ($validation->fails())
        {
            return Redirect::back()->withInput()->withErrors($validation->messages());
        }

        $this->_handleEmailSending($input);

        return Redirect::back();
    }

    /**
     * @param $input
     * @return mixed
     */
    private function _validateEmailForm($input)
    {
        return Validator::make($input, [
            'subject' => 'required',
            'text'    => 'required'
        ]);
    }

    /**
     *
     */
    private function _handleEmailSending($input)
    {
        $this->_dispatchSendEmailsJob($input);

        $message = [
            'type' => 'success',
            'text' => 'Email sending successfully initiated'
        ];

        Session::flash('popUpMessages', [$message]);
    }

    /**
     * @return mixed
     */
    private function _dispatchSendEmailsJob($input)
    {
        $job = (new SendPromotionEmails($input['subject'], $input['text']));

        return $this->dispatchNow($job);
    }
}