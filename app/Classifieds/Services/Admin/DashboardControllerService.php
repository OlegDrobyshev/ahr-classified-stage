<?php  namespace Classifieds\Services\Admin; 

use Classifieds\Ads\AdRepositoryInterface;
use Classifieds\AdsPhotos\AdsPhotoRepositoryInterface;
use Classifieds\AdsVideos\AdsVideoRepositoryInterface;
use Classifieds\Payments\PaymentRepositoryInterface;
use Classifieds\Plans\PlanRepositoryInterface;
use Classifieds\Users\UserRepositoryInterface;

class DashboardControllerService {

    /**
     * @var AdRepositoryInterface
     */
    private $adRepo;

    /**
     * @var AdsPhotoRepositoryInterface
     */
    private $adPhotoRepo;

    /**
     * @var AdsVideoRepositoryInterface
     */
    private $adVideoRepo;

    /**
     * @var UserRepositoryInterface
     */
    private $userRepo;

    /**
     * @var PaymentRepositoryInterface
     */
    private $paymentRepo;

    /**
     * @var PlanRepositoryInterface
     */
    private $planRepo;

    function __construct(AdRepositoryInterface $adRepo,
                         AdsPhotoRepositoryInterface $adPhotoRepo,
                         AdsVideoRepositoryInterface $adVideoRepo,
                         UserRepositoryInterface $userRepo,
                         PaymentRepositoryInterface $paymentRepo,
                         PlanRepositoryInterface $planRepo)
    {
        $this->adRepo = $adRepo;

        $this->adPhotoRepo = $adPhotoRepo;

        $this->adVideoRepo = $adVideoRepo;

        $this->userRepo = $userRepo;

        $this->paymentRepo = $paymentRepo;

        $this->planRepo = $planRepo;
    }

    /**
     * @return mixed
     */
    public function getSubscriptionPrice()
    {
        $paidPlan = $this->planRepo->getBySKU('PAID');

        return $paidPlan->price;
    }

    /**
     * @param $price
     * @return mixed
     */
    public function updateSubscriptionPrice($price)
    {
        $message = [
            'type' => 'danger',
            'text' => 'Oops, wrong price format'
        ];

        if ( ! is_numeric($price)) return $message;

        $status = $this->planRepo->update(2, ['price' => $price]);

        if ($status)
        {
            $message = [
                'type' => 'success',
                'text' => 'Price was successfully changed'
            ];
        }

        return $message;
    }

    /**
     * @return array
     */
    public function getRegisteredUsersStat()
    {
        $lastMonth = lastMonth();

        $lastWeek = lastWeek();

        $params = ['where' => ['type' => 'seller']];

        // All time
        $allTime = $this->userRepo->filter($params, true);

        // Last month
        $params['whereBetween'] = [
            'created_at' => [date('Y-m-01', strtotime($lastMonth)), date('Y-m-t', strtotime($lastMonth))]
        ];

        $lastMonth = $this->userRepo->filter($params, true);

        // Last week
        $params['whereBetween'] = [
            'created_at' => [date('Y-m-01', strtotime($lastWeek)), date('Y-m-t', strtotime($lastWeek))]
        ];

        $lastWeek = $this->userRepo->filter($params, true);

        return compact('allTime', 'lastMonth', 'lastWeek');
    }

    /**
     * @return array
     */
    public function getAdsStat()
    {
        $lastMonth = lastMonth();

        $lastWeek = lastWeek();

        $allTime = $this->adRepo->filter([], true);

        $lastMonth = $this->adRepo->filter([
            'whereBetween' => [
                'created_at' => [date('Y-m-01', strtotime($lastMonth)), date('Y-m-t', strtotime($lastMonth))]
            ]
        ], true);

        $lastWeek = $this->adRepo->filter([
            'whereBetween' => [
                'created_at' => [date('Y-m-01', strtotime($lastWeek)), date('Y-m-t', strtotime($lastWeek))]
            ]
        ], true);

        return compact('allTime', 'lastMonth', 'lastWeek');
    }

    /**
     * @return array
     */
    public function getAdsUsersChartData()
    {
        $result = [
            'users' => [],
            'ads' => []
        ];

        $dateRanges = $this->_getMonthsRangesOfCurrentYear();

        foreach ($dateRanges as $key => $range)
        {
            $result['users'][$key] = [
                strtotime($range[0]) * 1000,
                $this->userRepo->filter([
                    'where' => ['type' => 'seller'],
                    'whereBetween' => [
                        'created_at' => [$range]
                    ]
                ], true)
            ];
        }

        foreach ($dateRanges as $key => $range)
        {
            $result['ads'][$key] = [
                strtotime($range[0]) * 1000,
                $this->adRepo->filter([
                    'whereBetween' => [
                        'created_at' => [$range]
                    ]
                ], true)
            ];
        }

        return $result;
    }

    /**
     * @return array
     */
    public function getProfitChartData()
    {
        $result = [];

        $dateRanges = $this->_getMonthsRangesOfCurrentYear();

        foreach ($dateRanges as $key => $range)
        {
            $result[$key] = [
                strtotime($range[0]) * 1000,
                floor($this->paymentRepo->totalProfit($range))
            ];
        }

        return $result;
    }

    /**
     * @return mixed
     */
    public function getProfitStat()
    {
        $lastMonth = lastMonth();

        $lastWeek = lastWeek();

        $allTime = $this->paymentRepo->totalProfit();

        $lastMonth = $this->paymentRepo->totalProfit([
            date('Y-m-01', strtotime($lastMonth)), date('Y-m-t', strtotime($lastMonth))
        ]);

        $lastWeek = $this->paymentRepo->totalProfit([
            date('Y-m-01', strtotime($lastWeek)), date('Y-m-t', strtotime($lastWeek))
        ]);

        return compact('allTime', 'lastMonth', 'lastWeek');
    }

    /**
     * @param $params
     * @return array
     */
    public function getAds($params)
    {
        $result = [];

        $ads = $this->adRepo->filter($params);

        foreach ($ads as $ad)
        {
            $ad->photos = $this->adPhotoRepo->getByAdId($ad->ad_id);

            $ad->videos = $this->adVideoRepo->getByAdId($ad->ad_id);

            $result[] = $ad;
        }

        return $result;
    }

    /**
     * @param $params
     * @return mixed
     */
    public function getUsers($params)
    {
        return $this->userRepo->filter($params);
    }

    /**
     * @return array
     */
    private function _getMonthsRangesOfCurrentYear()
    {
        $dateRanges = [];

        $month = strtotime(date('Y-01-01'));

        foreach (range(1, 12) as $key)
        {
            if ($key > 1)
            {
                $month = strtotime('+1 month', $month);
            }

            $dateRanges[] = [
                date('Y-m-01', $month),
                date('Y-m-t', $month)
            ];
        }

        return $dateRanges;
    }
}