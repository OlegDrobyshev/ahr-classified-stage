<?php

namespace Classifieds\Services\Admin;

use Classifieds\Ads\AdRepositoryInterface;
use App\Http\Requests;
use Classifieds\AdsPhotos\AdsPhotoRepositoryInterface;
use Classifieds\AdsVideos\AdsVideoRepositoryInterface;
use Classifieds\Makes\MakeRepositoryInterface;
use Classifieds\Models\ModelRepositoryInterface;
use Classifieds\Users\UserRepositoryInterface;

/**
 * Class AdsControllerService
 * @package Classifieds\Services\Admin
 */
class AdsControllerService
{

    private $makeRepo;

    private $modelRepo;

    private $adRepo;

    private $userRepo;

    function __construct(MakeRepositoryInterface $makeRepo,
                         ModelRepositoryInterface $modelRepo,
                         AdRepositoryInterface $adRepo,
                         UserRepositoryInterface $userRepo)
    {
        $this->makeRepo = $makeRepo;

        $this->modelRepo = $modelRepo;

        $this->adRepo = $adRepo;

        $this->userRepo = $userRepo;
    }

    /**
     * @return array
     */
    public function getAdsFiltersValues()
    {
        return [
            'makes' => ['any' => 'Make'] + formatList('make_id', 'name', $this->makeRepo->getAll()),
            'models' => ['any' => 'Model'],
            'status' => ['any' => 'Status', 'approved' => 'Approved', 'blocked' => 'Blocked', 'new' => 'New'],
            'email' => ['any' => 'Seller Email']
        ];
    }

    /**
     * @param $makeId
     * @return array
     */
    public function getModelsByMakeId($makeId)
    {
        $models = $this->modelRepo->getByMakeId($makeId);

        return ['any' => 'Model'] + formatList('model_id', 'name', $models);
    }

    /**
     * @param $q
     * @return array
     */
    public function searchSellerByEmail($q)
    {
        $data = [];

        $users = $this->userRepo->filter([
            'whereLike' => ['email' => $q],
            'order' => ['email' => 'asc']
        ]);

        foreach ($users as $user)
        {
            $data[] = [
                'id' => $user->user_id,
                'text' => '<small><b>' . fullUserName($user) . '</b></small> <small>&lt;'. $user->email . '&gt;</small>'
            ];
        }

        return $data;
    }

    /**
     * @param $adId
     * @param $newStatus
     * @return array
     */
    public function updateApprovalStatus($adId, $newStatus)
    {
        $updated = $this->adRepo->update($adId, [
            'disabled' => $newStatus
        ]);

        $messages = [];

        if ($updated && $newStatus)
        {
            $messages[] = [
                'type' => 'warning',
                'text' => 'Add was blocked successfully'
            ];
        }
        else if ($updated && ! $newStatus)
        {
            $messages[] = [
                'type' => 'success',
                'text' => 'Add was approved successfully'
            ];
        }

        return ['messages' => $messages];
    }
}
