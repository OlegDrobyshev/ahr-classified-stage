<?php  namespace Classifieds\Services\Admin; 

use Classifieds\Users\UserRepositoryInterface;

class UsersControllerService
{
    /**
     * @var UserRepositoryInterface
     */
    private $userRepo;

    function __construct(UserRepositoryInterface $userRepo)
    {
        $this->userRepo = $userRepo;
    }

    /**
     * @param $userId
     * @return mixed
     */
    public function getUserById($userId)
    {
        return $this->userRepo->getById($userId)[0];
    }
}