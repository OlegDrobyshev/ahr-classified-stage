<?php  namespace Classifieds\Services\Admin;

use Classifieds\Gateways\GatewayRepositoryInterface;
use Classifieds\Payments\PaymentRepositoryInterface;

/**
 * Class PaymentsControllerService
 * @package Classifieds\Services\Admin
 */
class PaymentsControllerService
{
    private $paymentRepo;

    private $gatewayRepo;

    function __construct(PaymentRepositoryInterface $paymentRepo,
                         GatewayRepositoryInterface $gatewayRepo)
    {
        $this->paymentRepo = $paymentRepo;

        $this->gatewayRepo = $gatewayRepo;
    }

    /**
     * @return mixed
     */
    public function getGateways()
    {
        $gateways = $this->gatewayRepo->filter([]);

        $gateways = formatList('gateway_id', 'name', $gateways);

        return [0 => 'Gateway'] + $gateways;
    }

    /**
     * @return array
     */
    public function getPaymentStatuses()
    {
        return [
            'any' => 'Status',
            0 => 'Declined',
            1 => 'Approved'
        ];
    }
}