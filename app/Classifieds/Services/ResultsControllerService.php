<?php  namespace Classifieds\Services;

use Classifieds\Ads\AdRepositoryInterface;
use Classifieds\AdsPhotos\AdsPhotoRepositoryInterface;
use Classifieds\AdsVideos\AdsVideoRepositoryInterface;
use Classifieds\BodyStyles\BodyStyleRepositoryInterface;
use Classifieds\Conditions\ConditionRepositoryInterface;
use Classifieds\Makes\MakeRepositoryInterface;
use Classifieds\Models\ModelRepositoryInterface;
use Classifieds\Colors\ColorRepositoryInterface;
use Classifieds\Transmissions\TransmissionRepositoryInterface;
use Classifieds\DriveTypes\DriveTypeRepositoryInterface;
use Classifieds\FuelTypes\FuelTypeRepositoryInterface;
use Classifieds\Users\UserRepositoryInterface;

class ResultsControllerService
{
    private $makeRepo;

    private $modelRepo;

    private $colorRepo;

    private $transmissionRepo;

    private $driveTypeRepo;

    private $fuelTypeRepo;

    public $adRepo;

    private $condRepo;

    private $adPhotoRepo;

    private $adVideoRepo;

    private $bodyStyle;

    private $userRepo;

    function __construct(AdRepositoryInterface $adRepo,
                         ConditionRepositoryInterface $condRepo,
                         MakeRepositoryInterface $makeRepo,
                         ModelRepositoryInterface $modelRepo,
                         ColorRepositoryInterface $colorRepo,
                         TransmissionRepositoryInterface $transmissionRepo,
                         DriveTypeRepositoryInterface $driveTypeRepo,
                         FuelTypeRepositoryInterface $fuelTypeRepo,
                         AdsPhotoRepositoryInterface $adPhotoRepo,
                         AdsVideoRepositoryInterface $adVideoRepo,
                         BodyStyleRepositoryInterface $bodyStyle,
                         UserRepositoryInterface $userRepo)
    {
        $this->makeRepo = $makeRepo;

        $this->modelRepo = $modelRepo;

        $this->colorRepo = $colorRepo;

        $this->transmissionRepo = $transmissionRepo;

        $this->driveTypeRepo = $driveTypeRepo;

        $this->fuelTypeRepo = $fuelTypeRepo;

        $this->adRepo = $adRepo;

        $this->condRepo = $condRepo;

        $this->adPhotoRepo = $adPhotoRepo;

        $this->adVideoRepo = $adVideoRepo;

        $this->bodyStyle = $bodyStyle;

        $this->userRepo = $userRepo;
    }

    /**
     * @param $params
     * @return array
     */
    public function filter($params)
    {
        $ads = $this->adRepo->filter($params);

        foreach ($ads as $key => $ad)
        {
            $ads[$key]->owner = $this->userRepo->getById($ad->user_id)[0];

            $ads[$key]->photos = $this->adPhotoRepo->getByAdId($ad->ad_id);

            $ads[$key]->videos = $this->adVideoRepo->getByAdId($ad->ad_id);
        }

        return $ads;
    }

    /**
     * @param $input
     * @return array
     */
    public function parseFiltersValues($input)
    {
        $parsed = [
            'where' => ['disabled' => 0],
            'whereBetween' => [],
            'whereLike' => [],
            'order' => ['created_at' => 'desc'],
            'paginate' => 7
        ];

        foreach ($input as $filterType => $filters)
        {
            $suffix = '';

            switch ($filterType)
            {
                case 'text':
                    $key = 'whereLike'; break;

                case 'select':
                    $key = 'where'; $suffix = '_id';
                    break;

                case 'active':
                    $key = 'where'; $suffix = '_id';
                    break;

                case 'range':
                    $key = 'whereBetween'; break;

                default: continue;
            }

            foreach ($filters as $filter)
            {
                $parsed[$key][$filter['name'] . $suffix] = $filter['value'];
            }
        }

        return $parsed;
    }

    /**
     * @return mixed
     */
    public function getFiltersLists()
    {
        $filters = [
            'select' => $this->getSelectFilters(),
            'range' => $this->getRangeFilters(),
            'text' => $this->getTextFilters()
        ];

        return $filters;
    }

    /**
     * @return mixed
     */
    public function getSelectFilters()
    {
        $selectFiltersData = [
            'cond' => formatList('cond_id', 'name', $this->condRepo->getAll()),
            'make' => formatList('make_id', 'name', $this->makeRepo->getAll()),
            'model' => formatList('model_id', 'name', $this->modelRepo->getAll()),
            'body_style' => formatList('body_style_id', 'name', $this->bodyStyle->getAll()),
            'ext_color' => formatList('color_id', 'name', $this->colorRepo->getAll()),
            'int_color' => formatList('color_id', 'name', $this->colorRepo->getAll()),
            'transm' => formatList('transm_id', 'name', $this->transmissionRepo->getAll()),
            'drive_type' => formatList('drive_type_id', 'name', $this->driveTypeRepo->getAll()),
            'fuel_type' => formatList('fuel_type_id', 'name', $this->fuelTypeRepo->getAll()),
        ];

        return $this->setDefaultForFilters($selectFiltersData);
    }

    /**
     * @return array
     */
    public function getRangeFilters()
    {
        return [
            'year' => $this->adRepo->getYearsRange(),
            'price' => $this->adRepo->getPriceRange(),
            'mileage' => $this->adRepo->getMilesRange()
        ];
    }

    /**
     * @return array
     */
    public function getTextFilters()
    {
        return ['desc' => 'Words'];
    }

    /**
     * @param $filtersList
     * @return mixed
     */
    public function setDefaultForFilters($filtersList)
    {
        foreach ($filtersList as $key => $list)
        {
            $filtersList[$key] = [0 => 'Any'] + $filtersList[$key];
        }

        return $filtersList;
    }
}