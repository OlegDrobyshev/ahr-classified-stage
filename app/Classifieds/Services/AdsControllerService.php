<?php  namespace Classifieds\Services; 

use Classifieds\Ads\AdRepositoryInterface;
use Classifieds\AdsPhotos\AdsPhotoRepositoryInterface;
use Classifieds\AdsVideos\AdsVideoRepositoryInterface;
use Classifieds\BodyStyles\BodyStyleRepositoryInterface;
use Classifieds\Colors\ColorRepositoryInterface;
use Classifieds\Conditions\ConditionRepositoryInterface;
use Classifieds\DriveTypes\DriveTypeRepositoryInterface;
use Classifieds\FuelTypes\FuelTypeRepositoryInterface;
use Classifieds\Makes\MakeRepositoryInterface;
use Classifieds\Models\ModelRepositoryInterface;
use Classifieds\Transmissions\TransmissionRepositoryInterface;
use Classifieds\Users\UserRepositoryInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Mail;
use Session;
use Validator;

/**
 * Class AdsControllerService
 * @package Classifieds\Services
 */
class AdsControllerService {

    public $user;

    public $adRepo;

    private $makeRepo;

    private $modelRepo;

    private $colorRepo;

    private $transmissionRepo;

    private $driveTypeRepo;

    private $fuelTypeRepo;

    private $condRepo;

    private $adPhotoRepo;

    private $adVideoRepo;

    private $userRepo;

    private $bodyStyle;

    function __construct(AdRepositoryInterface $adRepo,
                         ConditionRepositoryInterface $condRepo,
                         MakeRepositoryInterface $makeRepo,
                         ModelRepositoryInterface $modelRepo,
                         ColorRepositoryInterface $colorRepo,
                         TransmissionRepositoryInterface $transmissionRepo,
                         DriveTypeRepositoryInterface $driveTypeRepo,
                         FuelTypeRepositoryInterface $fuelTypeRepo,
                         AdsPhotoRepositoryInterface $adPhotoRepo,
                         AdsVideoRepositoryInterface $adVideoRepo,
                         UserRepositoryInterface $userRepo,
                         BodyStyleRepositoryInterface $bodyStyle)
    {
        $this->user = Auth::user();

        $this->adRepo = $adRepo;

        $this->makeRepo = $makeRepo;

        $this->modelRepo = $modelRepo;

        $this->colorRepo = $colorRepo;

        $this->transmissionRepo = $transmissionRepo;

        $this->driveTypeRepo = $driveTypeRepo;

        $this->fuelTypeRepo = $fuelTypeRepo;

        $this->condRepo = $condRepo;

        $this->adPhotoRepo = $adPhotoRepo;

        $this->adVideoRepo = $adVideoRepo;

        $this->userRepo = $userRepo;

        $this->bodyStyle = $bodyStyle;
    }

    /**
     * @param $userId
     * @return mixed
     */
    public function getUserById($userId)
    {
        return $this->userRepo->getById($userId)[0];
    }

    /**
     * @param $makeId
     * @return array
     */
    public function getModelsByMakeId($makeId)
    {
        $models = $this->modelRepo->filter([
            'where' => ['make_id' => $makeId]
        ]);

        return ['' => 'Select Model'] + formatList('model_id', 'name', $models);
    }

    /**
     * @param $adId
     * @return mixed
     */
    public function viewedByAdmin($adId)
    {
        return $this->adRepo->update($adId, ['viewed_by_admin' => 1]);
    }

    /**
     * @param $data
     * @return array
     */
    public function emailSeller($data)
    {
        $result = ['errors' => [], 'status' => false];

        // Validate
        $validation = Validator::make($data, [
            'adId' => 'required|numeric',
            'name' => 'required',
            'email' => 'required|email',
            'text' => 'required',
            'g-recaptcha-response' => 'required|captcha'
        ]);

        if ($validation->fails())
        {
            $result['errors'] = $validation->messages();

            return $result;
        }

        $ad = $this->adRepo->getById($data['adId']);

        $adOwner = $this->userRepo->getById($ad->user_id);

        if ( ! $ad && ! $adOwner)
        {
            return $result;
        }

        // Send
        $data = [
            'text' => 'Name: ' . $data['name']
                . '<br />Email: ' . $data['email']
                . '<br />Message: ' . $data['text']
        ];

        Mail::queue('emails.contact-seller', $data, function ($message) use ($ad, $adOwner)
        {
            $message->subject('From potential customer about ' . adTitle($ad));

            $message->to($adOwner[0]->email);
        });

        $result['status'] = true;

        // Return status
        return $result;
    }

    /**
     * @param $adId
     * @return array
     */
    public function getAddById($adId)
    {
        $ad = $this->adRepo->getById($adId);

        if ( ! $ad->exists) abort(404);

        $ad->photos = $this->adPhotoRepo->getByAdId($adId);

        $ad->videos = $this->adVideoRepo->getByAdId($adId);

        return $ad;
    }

    /**
     * @param $adsIds
     * @return array
     */
    public function getByAdsIds($adsIds)
    {
        $result = [];

        $ads = $this->adRepo->getByIds($adsIds);

        foreach ($ads as $ad)
        {
            $ad->photos = $this->adPhotoRepo->getByAdId($ad->ad_id);

            $ad->videos = $this->adVideoRepo->getByAdId($ad->ad_id);

            $result[] = $ad;
        }

        return $result;
    }

    /**
     * @return array
     */
    public function getOldMediaInput()
    {
        $photos = [];

        $videos = [];

        if (Session::has('_old_input'))
        {
            // Get photos
            $photosIds = Session::get('_old_input.ad_photo_id');

            if ($photosIds)
            {
                $photos = $this->adPhotoRepo->getById($photosIds);

                foreach ($photos as $key => $photo)
                {
                    $photos[$key]->thumbnail = getThumbnail($photo->uri, 'small');
                }
            }

            // Get videos
            $videosIds = Session::get('_old_input.ad_video_id');

            if ($videosIds)
            {
                $videos = $this->adVideoRepo->getById($videosIds);

                foreach ($videos as $key => $video)
                {
                    $videos[$key]->thumbnail = getThumbnail($video->image, 'small');
                }
            }
        }

        return compact('videos', 'photos');
    }

    /**
     * @return array
     */
    public function getAllUserAdsFilterParams()
    {
        $params = [
            'where'  => ['user_id' => $this->user->user_id],
            'paginate' => 5
        ];

        $disabled = \Request::get('status');

        if (is_numeric($disabled))
        {
            $params['where']['disabled'] = $disabled;
        }

        return [$disabled, $params];
    }

    /**
     * @param $params
     * @param $disabled
     * @return mixed
     */
    public function getAllUserAds($params, $disabled)
    {
        $ads = $this->adRepo->filter($params);

        foreach ($ads as $key => $ad)
        {
            $ads[$key]->photos = $this->adPhotoRepo->getByAdId($ad->ad_id);

            $ads[$key]->videos = $this->adVideoRepo->getByAdId($ad->ad_id);
        }

        if (is_numeric($disabled))
        {
            $ads->appends(['status' => $disabled]);
        }

        return $ads;
    }

    /**
     * @param $ads
     */
    public function getUserAdsCountsByStatus($ads)
    {
        $ads->disabledAdsTotal = $this->adRepo->getUserAdsCount($this->user->user_id, 1);

        $ads->enabledAdsTotal = $this->adRepo->getUserAdsCount($this->user->user_id, 0);

        return $ads;
    }

    /**
     * @return mixed
     */
    public function getSelectElements()
    {
        $selectElements = [
            'cond' => [
                'title' => 'Condition *',
                'values' => formatList('cond_id', 'name', $this->condRepo->getAll())
            ],
            'make' => [
                'title' => 'Make *',
                'values' => formatList('make_id', 'name', $this->makeRepo->getAll())
            ],
            'model' => [
                'title' => 'Model *',
                'values' => formatList('model_id', 'name', $this->modelRepo->getAll())
            ],
            'year' => [
                'title' => 'Year *',
                'values' => getSequence(1900, date('Y'))
            ],
            'body_style' => [
                'title' => 'Body Style *',
                'values' => formatList('body_style_id', 'name', $this->bodyStyle->getAll())
            ],
            'ext_color' => [
                'title' => 'Exterior Color *',
                'values' => formatList('color_id', 'name', $this->colorRepo->getAll()),
            ],
            'int_color' => [
                'title' => 'Interior Color *',
                'values' => formatList('color_id', 'name', $this->colorRepo->getAll()),
            ],
            'transm' => [
                'title' => 'Transmission *',
                'values' => formatList('transm_id', 'name', $this->transmissionRepo->getAll())
            ],
            'drive_type' => [
                'title' => 'Drive Type *',
                'values' => formatList('drive_type_id', 'name', $this->driveTypeRepo->getAll())
            ],
            'fuel_type' => [
                'title' => 'Fuel Type *',
                'values' => formatList('fuel_type_id', 'name', $this->fuelTypeRepo->getAll())
            ],
            'doors' => [
                'title' => 'Doors *',
                'values' => getSequence(1, 10)
            ],
        ];

        return $this->setDefaultForFilters($selectElements);
    }

    /**
     * @param $selectElements
     * @return mixed
     */
    public function setDefaultForFilters($selectElements)
    {
        foreach ($selectElements as $key => $item)
        {
            $selectElements[$key]['values'] = [0 => 'Select ' . $item['title']] + $selectElements[$key]['values'];
        }

        return $selectElements;
    }

}