<?php  namespace Classifieds\Services; 

use App\Jobs\PayForSubscription;
use Classifieds\Plans\PlanRepositoryInterface;
use Classifieds\Users\UserRepositoryInterface;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

/**
 * Class UsersControllerService
 * @package Classifieds\Services
 */
class UsersControllerService
{
    use DispatchesJobs;

    /**
     * @var UserRepositoryInterface
     */
    private $userRepo;

    /**
     * @var PlanRepositoryInterface
     */
    private $planRepo;

    /**
     * @param UserRepositoryInterface $userRepo
     * @param PlanRepositoryInterface $planRepo
     */
    function __construct(UserRepositoryInterface $userRepo,
                         PlanRepositoryInterface $planRepo)
    {
        $this->userRepo = $userRepo;

        $this->planRepo = $planRepo;
    }

    /**
     * @return mixed
     */
    public function getPaidPlan()
    {
        return $this->planRepo->getBySKU('PAID');
    }

    /**
     * @param $input
     * @param $user
     */
    public function doSubscriptionUpdate($input, $user)
    {
        $cardData = [
            'plan_id' => 2,
            'gateway_id' => $input['gateway_id'],
            'card_expiry' => $input['card_expiry'],
            'card_number' => $input['card_number'],
        ];

        $payment = $this->dispatchNow(new PayForSubscription($cardData, $user));

        if ($payment['approved'])
        {
            Session::flash('messages', [[
                'type' => 'success',
                'text' => 'Subscription successfully updated'
            ]]);

            return Redirect::to('/');
        }
        else
        {
            Session::flash('messages', [[
                'type' => 'danger',
                'text' => 'We were unable to charge to your credit card'
            ]]);

            return Redirect::back();
        }
    }
}