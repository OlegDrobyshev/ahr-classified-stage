<?php  namespace Classifieds\Users;

/**
 * Interface UserRepositoryInterface
 * @package Classifieds\Users
 */
interface UserRepositoryInterface
{
    public function filter($params, $onlyCount = false);
    public function getById($userId);
    public function update($userId, $data);
    public function delete($userId);
}