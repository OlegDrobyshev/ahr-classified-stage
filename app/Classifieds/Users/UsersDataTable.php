<?php  namespace Classifieds\Users;

use Classifieds\DataTables\DataTableInterface;
use Classifieds\DataTables\DataTableFilters;

/**
 * Class UsersDataTable
 * @package Classifieds\Users
 */
class UsersDataTable implements DataTableInterface {

    use DataTableFilters;

    /**
     * @var UserRepositoryInterface
     */
    private $userRepo;

    function __construct(UserRepositoryInterface $userRepo)
    {
        $this->userRepo = $userRepo;
    }

    /**
     * @param array $input
     * @return mixed
     */
    public function getResponse(array $input)
    {
        $columns = [
            ['db' => 'user_id', 'dt' => 0],
            ['db' => 'surname', 'dt' => 1],
            ['db' => 'email', 'dt' => 2],
            ['db' => 'plan_id', 'dt' => 3],
            ['db' => 'status', 'dt' => 4],
            ['db' => 'expires', 'dt' => 5],
            ['db' => 'created_at', 'dt' => 6]
        ];

        $filters = $this->getFilters($columns, $input);

        $filters['where']['type'] = 'seller';

        $filters['relations'] = ['plan'];

        $users = $this->userRepo->filter($filters);

        $total = $this->_total($filters);

        return [
            'draw' => $input['draw'],
            'recordsTotal' => $total,
            'recordsFiltered' => $total,
            'data' => $this->format($users)
        ];
    }

    /**
     * @param $users
     * @return array
     */
    private function format($users)
    {
        $result = [];

        foreach ($users as $user)
        {
            $result[] = [
                $user->user_id,
                fullUserName($user),
                $this->_getUserEmailLink($user),
                $user->plan->sku,
                $this->_getUserStatus($user),
                $user->expires,
                $user->created_at->format('Y-m-d'),
                $this->_getActionButtons($user)
            ];
        }

        return $result;
    }

    /**
     * @param $filters
     * @return mixed
     */
    private function _total($filters)
    {
        unset($filters['total'], $filters['offset']);

        return $this->userRepo->filter($filters, true);
    }

    /**
     * @param $user
     * @return string
     */
    private function _getUserEmailLink($user)
    {
        return '<a href="mailto:' . $user->email . '">' . $user->email . '</a>';
    }

    /**
     * @param $user
     * @return string
     */
    private function _getUserStatus($user)
    {
        $status = '<span class="label label-success">Active</span>';

        if ($user->status == 0)
        {
            $status = '<span class="label label-danger">Blocked</span>';
        }
        else if ($user->status == 2)
        {
            $status = '<span class="label label-warning">Expired</span>';
        }

        return $status;
    }

    /**
     * @param $user
     * @return string
     */
    private function _getActionButtons($user)
    {
        $html = '<a href="/admin/user/' . $user->user_id . '" class="btn btn-info">'
                . '<span class="glyphicon glyphicon-search"></span>'
            . '</a>';

        $html .= '&nbsp;';

        $html .= '<a href="#" data-user-id="' . $user->user_id . '" class="btn btn-danger user-delete">'
                . '<span class="glyphicon glyphicon-trash"></span>'
            . '</a>';

        return $html;
    }
}