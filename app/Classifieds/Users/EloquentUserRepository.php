<?php  namespace Classifieds\Users; 

/**
 * Class EloquentUserRepository
 * @package Classifieds\Users
 */
class EloquentUserRepository implements UserRepositoryInterface
{
    /**
     * @var User
     */
    private $model;

    /**
     * @param User $model
     */
    function __construct(User $model)
    {
        $this->model = $model;
    }

    /**
     * @param $userId
     * @return mixed
     */
    public function getById($userId)
    {
        return $this->model->whereUserId($userId)->get();
    }

    /**
     * @param $userId
     * @param $data
     * @return mixed
     */
    public function update($userId, $data)
    {
        User::$rules = [];

        if (isset($data['password']))
        {
            User::$rules = array_merge(User::$rules, [
                'password' => 'required|alpha_num|min:6|confirmed'
            ]);
        }

        $user = $this->model->find($userId);

        foreach ($data as $key => $value)
        {
            $user->{$key} = $value;
        }

        $user->save();

        return $user;
    }

    /**
     * @param $userId
     * @return mixed
     */
    public function delete($userId)
    {
        return $this->model->whereUserId($userId)->delete();
    }

    /**
     * @param $params
     * @param bool $onlyCount
     * @return mixed
     */
    public function filter($params, $onlyCount = false)
    {
        $query = $this->model;

        // Relations
        if (isset($params['relations']))
        {
            $query = $query->with($params['relations']);
        }

        // Where statement
        if (isset($params['where']))
        {
            foreach ($params['where'] as $column => $value)
            {
                $method = is_array($value) ? 'whereIn' : 'where';

                $query = $query->{$method}($column, $value);
            }
        }

        // Where between statement
        if (isset($params['whereBetween']))
        {
            foreach ($params['whereBetween'] as $column => $range)
            {
                $query = $query->whereBetween($column, $range);
            }
        }

        // Where Like statement
        if (isset($params['whereLike']))
        {
            foreach ($params['whereLike'] as $column => $value)
            {
                $query = $query->where($column, 'LIKE', '%' . $value . '%');
            }
        }

        // Search by key word
        if (isset($params['key_word']))
        {
            $query = $query->where('surname', 'LIKE', '%' . $params['key_word'] . '%');
        }

        // Order statement
        if (isset($params['order']))
        {
            foreach ($params['order'] as $column => $dir)
            {
                $column = explode('.', $column);

                if (count($column) == 3)
                {
                    $query->join($column[0], $column[0] . '.' . $column[1], '=', 'ads.' . $column[1])
                        ->orderBy($column[0] . '.' . $column[2], $dir);
                }
                else
                {
                    $query = $query->orderBy($column[0], $dir);
                }
            }
        }

        // Enable Laravel's simple paginate
        if (isset($params['simplePaginate']))
        {
            return $query->simplePaginate($params['simplePaginate']);
        }

        // Enable Laravel's paginate
        if (isset($params['paginate']))
        {
            return $query->paginate($params['paginate']);
        }

        // Limit statement
        if (isset($params['total']))
        {
            $query = $query->limit($params['total']);
        }

        // Offset
        if (isset($params['offset']))
        {
            $query = $query->skip($params['offset']);
        }

        if ($onlyCount) return $query->count();

        return $query->get();
    }
}