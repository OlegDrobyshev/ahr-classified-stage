<?php

namespace Classifieds\Users;

use Classifieds\BaseModel;

/**
 * Class User
 * @package Classifieds\Users
 */
class User extends BaseModel
{
    protected $primaryKey = 'user_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'surname', 'email', 'password', 'address', 'type',
        'city', 'state', 'zip', 'phone', 'plan_id', 'status', 'expires'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function plan()
    {
        return $this->belongsTo('Classifieds\Plans\Plan', 'plan_id');
    }

    /**
     * @return bool
     */
    public function isAdmin()
    {
        return ($this->attributes['type'] == 'admin');
    }
}
