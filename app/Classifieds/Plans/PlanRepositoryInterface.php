<?php  namespace Classifieds\Plans;

/**
 * Interface PlanRepositoryInterface
 * @package Classifieds\Plans
 */
interface PlanRepositoryInterface
{
    public function getAll();
    public function getById($planId);
    public function getBySKU($sku);
    public function update($planId, $data);
}