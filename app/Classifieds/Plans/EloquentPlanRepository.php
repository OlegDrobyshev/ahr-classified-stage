<?php  namespace Classifieds\Plans;

/**
 * Class EloquentPlanRepository
 * @package Classifieds\Plans
 */
class EloquentPlanRepository implements PlanRepositoryInterface
{
    private $model;

    function __construct(Plan $model)
    {
        $this->model = $model;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAll()
    {
        return $this->model->all();
    }

    /**
     * @param $planId
     * @return bool
     */
    public function getById($planId)
    {
        $plan = $this->model->wherePlanId($planId)->get();

        if (isset($plan[0]))
        {
            return $plan[0];
        }

        return false;
    }

    /**
     * @param $sku
     * @return bool
     */
    public function getBySKU($sku)
    {
        $plan = $this->model->whereSku($sku)->get();

        if (isset($plan[0]))
        {
            return $plan[0];
        }

        return false;
    }

    /**
     * @param $planId
     * @param $data
     * @return mixed
     */
    public function update($planId, $data)
    {
        return $this->model->wherePlanId($planId)->update($data);
    }
}