<?php

namespace Classifieds\Plans;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Plan
 * @package Classifieds\Plans
 */
class Plan extends Model
{
    /**
     * @var string
     */
    protected $primaryKey = 'plan_id';

    /**
     * @var array
     */
    protected $fillable = ['sku', 'price'];

    /**
     * @var bool
     */
    public $timestamps = false;
}
