<?php

namespace Classifieds\Makes;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Make
 * @package App\Classifieds\Makes
 */
class Make extends Model
{
    /**
     * @var string
     */
    protected $primaryKey = 'make_id';

    /**
     * @var array
     */
    protected $fillable = ['name'];

    public $timestamps = false;
}
