<?php  namespace Classifieds\Makes;

/**
 * Interface MakeRepositoryInterface
 * @package Classifieds\Makes
 */
interface MakeRepositoryInterface
{
    public function getAll();
}