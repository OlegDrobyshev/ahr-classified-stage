<?php  namespace Classifieds\Makes;

/**
 * Class EloquentMakeRepository
 * @package Classifieds\Makes
 */
class EloquentMakeRepository implements MakeRepositoryInterface
{
    private $model;

    function __construct(Make $model)
    {
        $this->model = $model;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAll()
    {
        return $this->model->all();
    }
}