<?php  namespace Classifieds\BodyStyles;

/**
 * Class BodyStyleRepositoryInterface
 * @package Classifieds\BodyStyles
 */
interface BodyStyleRepositoryInterface {
    public function getAll();
}