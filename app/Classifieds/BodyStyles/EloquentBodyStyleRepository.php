<?php  namespace Classifieds\BodyStyles;

/**
 * Class EloquentBodyStyleRepository
 * @package Classifieds\BodyStyles
 */
class EloquentBodyStyleRepository implements BodyStyleRepositoryInterface
{
    /**
     * @var BodyStyle
     */
    private $model;

    /**
     * @param BodyStyle $model
     */
    function __construct(BodyStyle $model)
    {
        $this->model = $model;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAll()
    {
        return $this->model->all();
    }
}