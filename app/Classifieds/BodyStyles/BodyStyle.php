<?php  namespace Classifieds\BodyStyles; 

use Classifieds\BaseModel;

/**
 * Class BodyStyle
 * @package Classifieds\BodyStyles
 */
class BodyStyle extends BaseModel
{
    /**
     * @var string
     */
    protected $primaryKey = 'body_style_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @var array
     */
    public static $rules = [
        'name' => 'required'
    ];
}