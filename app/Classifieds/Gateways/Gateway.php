<?php  namespace Classifieds\Gateways; 

use Classifieds\BaseModel;

/**
 * Class Gateway
 * @package Classifieds\Gateways
 */
class Gateway extends BaseModel
{
    /**
     * @var string
     */
    protected $primaryKey = 'gateway_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * @var array
     */
    public static $rules = [

    ];
}