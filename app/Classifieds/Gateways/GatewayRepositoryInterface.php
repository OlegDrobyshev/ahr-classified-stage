<?php  namespace Classifieds\Gateways;

/**
 * Interface GatewayRepositoryInterface
 * @package Classifieds\Gateways
 */
interface GatewayRepositoryInterface {
    public function filter($params);
    public function getById($gatewayId);
}