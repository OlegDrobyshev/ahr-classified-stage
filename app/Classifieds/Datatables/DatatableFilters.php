<?php

namespace Classifieds\DataTables;

/**
 * Class DataTableFilters
 */
trait DataTableFilters {

    /**
     * @param array $input
     * @param $columns
     * @return array
     */
    public function getFilters($columns, array $input)
    {
        $filters = [];

        $filters['offset'] = $input['start'];

        $filters['total'] = $input['length'];

        $filters['key_word'] = $input['search']['value'];

        foreach ($input['order'] as $item)
        {
            foreach ($columns as $column)
            {
                if ($column['dt'] == $item['column'])
                {
                    if (isset($column['relation']))
                    {
                        $relation = explode('.', $column['relation']);

                        $column['db'] = $relation[0] . '.' . $relation[1] . '.' . $column['db'];
                    }

                    $filters['order'] = [$column['db'] => $item['dir']];
                }
            }
        }

        return $filters;
    }
}