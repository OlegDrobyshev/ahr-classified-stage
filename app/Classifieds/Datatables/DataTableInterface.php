<?php

namespace Classifieds\DataTables;

/**
 * Interface DataTableInterface
 */
interface DataTableInterface
{
    /**
     * @param array $input
     * @return mixed
     */
    public function getResponse(array $input);
}