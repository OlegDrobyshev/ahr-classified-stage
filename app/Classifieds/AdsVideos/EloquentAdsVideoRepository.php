<?php  namespace Classifieds\AdsVideos;

/**
 * Class EloquentAdsVideosRepository
 * @package Classifieds\AdsVideos
 */
class EloquentAdsVideoRepository implements AdsVideoRepositoryInterface
{
    private $model;

    function __construct(AdVideo $model)
    {
        $this->model = $model;
    }

    /**
     * @param $adId
     * @return mixed
     */
    public function getByAdId($adId)
    {
        return $this->model->whereAdId($adId)->orderBy('created_at', 'asc')->get();
    }

    /**
     * @param $adVideoId
     * @return mixed
     */
    public function getById($adVideoId)
    {
        $method = is_array($adVideoId) ? 'whereIn' : 'where';

        return $this->model->{$method}('video_id', $adVideoId)->get();
    }

    /**
     * @param $data
     * @return static
     */
    public function create($data)
    {
        return $this->model->create($data);
    }

    /**
     * @param $adVideoId
     * @param $data
     * @return mixed
     */
    public function update($adVideoId, $data)
    {
        return $this->model->whereVideoId($adVideoId)->update($data);
    }

    /**
     * @param $adVideoId
     */
    public function delete($adVideoId)
    {
        return $this->model->whereVideoId($adVideoId)->delete();
    }
}