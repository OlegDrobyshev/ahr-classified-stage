<?php  namespace Classifieds\AdsVideos;

/**
 * Class AdsVideoRepositoryInterface
 * @package Classifieds\AdsVideos
 */
interface AdsVideoRepositoryInterface {
    public function getByAdId($adId);
    public function getById($adVideoId);
    public function create($data);
    public function update($adVideoId, $data);
    public function delete($adVideoId);
}