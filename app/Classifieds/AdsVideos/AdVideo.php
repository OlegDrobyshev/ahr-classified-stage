<?php

namespace Classifieds\AdsVideos;

use Illuminate\Database\Eloquent\Model;

/**
 * Class AdVideo
 * @package Classifieds\AdsVideos
 */
class AdVideo extends Model
{
    /**
     * @var string
     */
    protected $table = 'ads_videos';

    /**
     * @var string
     */
    protected $primaryKey = 'video_id';

    /**
     * @var array
     */
    protected $fillable = ['ad_id', 'provider', 'vid', 'url', 'image'];
}
