<?php

namespace Classifieds\Ads;

/**
 * Interface AdRepositoryInterface
 */
interface AdRepositoryInterface
{
    public function getById($adId);
    public function getByIds($adsIds);
    public function checkExistenceByIds($adsIds);
    public function getUserAds($userId, $disabled = null);
    public function getUserAdsCount($userId, $disabled = null);
    public function getYearsRange();
    public function getPriceRange();
    public function getMilesRange();
    public function filter($params, $onlyCount = false);
    public function getAll();
    public function create($data);
    public function update($adId, $data);
    public function delete($adId);
}