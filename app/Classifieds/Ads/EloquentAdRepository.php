<?php  namespace Classifieds\Ads;

/**
 * Class EloquentAdRepository
 * @package Classifieds\Ads
 */
class EloquentAdRepository implements AdRepositoryInterface {

    /**
     * @var Ad
     */
    private $model;

    /**
     * @param Ad $model
     */
    function __construct(Ad $model)
    {
        $this->model = $model;
    }

    /**
     * @param $data
     * @return static
     */
    public function create($data)
    {
        return $this->model->create($data);
    }

    /**
     * @param $adId
     * @param $data
     * @return mixed
     */
    public function update($adId, $data)
    {
        $ad = $this->model->find($adId);

        foreach ($data as $key => $value)
        {
            if ($value)
            {
                $ad->{$key} = $value;
            }
            else
            {
                unset($ad->{$key});
            }
        }

        $ad->save();

        $errors = $ad->getErrors();

        $ad = $this->model->find($adId);

        $ad->setErrors($errors);

        return $ad;
    }

    /**
     * @param $adId
     * @return mixed
     */
    public function delete($adId)
    {
        $method = is_array($adId) ? 'whereIn' : 'where';

        return $this->model->{$method}('ad_id', $adId)->delete();
    }

    /**
     * @param $adId
     * @return mixed
     */
    public function getById($adId)
    {
        $ad = $this->filter([
            'where' => ['ad_id' => $adId]
        ]);

        return isset($ad[0]) ? $ad[0] : $this->model;
    }

    /**
     * @param $adsIds
     * @return mixed
     */
    public function getByIds($adsIds)
    {
        return $this->filter([
            'where' => ['ad_id' => $adsIds]
        ]);
    }

    /**
     * @param $adsIds
     * @return mixed
     */
    public function checkExistenceByIds($adsIds)
    {
        foreach ($adsIds as $key => $adId)
        {
            if ( ! $this->model->whereAdId($adId)->exists())
            {
                unset($adsIds[$key]);
            }
        }

        return $adsIds;
    }

    /**
     * @param $params
     * @param bool $onlyCount
     * @return mixed
     */
    public function filter($params, $onlyCount = false)
    {
        $params = $this->_setFilterDefaults($params);

        $query = $this->model;

        // Relations
        if (isset($params['relations']))
        {
            $query = $query->with($params['relations']);
        }

        // Where statement
        if (isset($params['where']))
        {
            foreach ($params['where'] as $column => $value)
            {
                $method = is_array($value) ? 'whereIn' : 'where';

                $query = $query->{$method}('ads.' . $column, $value);
            }
        }

        // Where between statement
        if (isset($params['whereBetween']))
        {
            foreach ($params['whereBetween'] as $column => $range)
            {
                $query = $query->whereBetween($column, $range);
            }
        }

        // Where Like statement
        if (isset($params['whereLike']))
        {
            foreach ($params['whereLike'] as $column => $value)
            {
                $query = $query->where($column, 'LIKE', '%' . $value . '%');
            }
        }

        // Search by key word
        if (isset($params['key_word']))
        {
            $query = $query->whereHas('make', function($q) use ($params)
            {
                $q->where('name', 'LIKE', '%' . $params['key_word'] . '%');
            });
        }

        // Order statement
        if (isset($params['order']))
        {
            foreach ($params['order'] as $column => $dir)
            {
                $column = explode('.', $column);

                if (count($column) == 3)
                {
                    $query->join($column[0], $column[0] . '.' . $column[1], '=', 'ads.' . $column[1])
                        ->orderBy($column[0] . '.' . $column[2], $dir);
                }
                else
                {
                    $query = $query->orderBy($column[0], $dir);
                }
            }
        }

        // Enable Laravel's simple paginate
        if (isset($params['simplePaginate']))
        {
            return $query->simplePaginate($params['simplePaginate']);
        }

        // Enable Laravel's paginate
        if (isset($params['paginate']))
        {
            return $query->paginate($params['paginate']);
        }

        // Limit statement
        if (isset($params['total']))
        {
            $query = $query->limit($params['total']);
        }

        // Offset
        if (isset($params['offset']))
        {
            $query = $query->skip($params['offset']);
        }

        if ($onlyCount) return $query->count();

        return $query->get();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAll()
    {
        return $this->model->all();
    }

    /**
     * @return mixed
     */
    public function getYearsRange()
    {
        return [
            'min' => $this->model->min('year'),
            'max' => $this->model->max('year'),
            'step' => 1,
            'init-min' => 1990
        ];
    }

    /**
     * @return mixed
     */
    public function getPriceRange()
    {
        return [
            'min' => $this->model->min('price'),
            'max' => $this->model->max('price'),
            'step' => 100,
            'init-min' => 5000
        ];
    }

    /**
     * @return mixed
     */
    public function getMilesRange()
    {
        return [
            'min' => $this->model->min('mileage'),
            'max' => $this->model->max('mileage'),
            'step' => 100,
            'init-min' => 1000
        ];
    }

    /**
     * @param $userId
     * @param null $disabled
     * @return mixed
     */
    public function getUserAds($userId, $disabled = null)
    {
        $query = $this->model->whereUserId($userId);

        if ($disabled !== null)
            $query->whereDisabled($disabled);

        return $query->get();
    }

    /**
     * @param $userId
     * @param null $disabled
     * @return mixed
     */
    public function getUserAdsCount($userId, $disabled = null)
    {
        return $this->getUserAds($userId, $disabled)->count();
    }

    /**
     * @param $params
     * @return array
     */
    private function _setFilterDefaults($params)
    {
        $defaults = [
            'relations' => [
                'user', 'condition', 'make', 'model', 'bodyStyle', 'intColor', 'extColor', 'transmission', 'driveType', 'fuelType'
            ]
        ];

        return array_merge_recursive($params, $defaults);
    }
}