<?php  namespace Classifieds\Ads; 

use Classifieds\DataTables\DataTableFilters;

class AdsDataTable {

    use DataTableFilters;

    private $adRepo;

    function __construct(AdRepositoryInterface $adRepo)
    {
        $this->adRepo = $adRepo;
    }

    /**
     * @param array $input
     * @return mixed
     */
    public function getResponse(array $input)
    {
        $columns = [
            ['db' => 'ad_id', 'dt' => 0],
            ['db' => 'name', 'dt' => 1, 'relation' => 'makes.make_id'],
            ['db' => 'name', 'dt' => 2, 'relation' => 'models.model_id'],
            ['db' => 'year', 'dt' => 3],
            ['db' => 'surname', 'dt' => 4, 'relation' => 'users.user_id'],
            ['db' => 'disabled', 'dt' => 5],
            ['db' => 'created_at', 'dt' => 6]
        ];

        $filters = $this->getFilters($columns, $input);

        $filters = $this->_getCustomFilters($input, $filters);

        $ads = $this->adRepo->filter($filters);

        $total = $this->_total($filters);

        return [
            'draw' => $input['draw'],
            'recordsTotal' => $total,
            'recordsFiltered' => $total,
            'data' => $this->format($ads)
        ];
    }

    /**
     * @param $ads
     * @return array
     */
    private function format($ads)
    {
        $result = [];

        foreach ($ads as $ad)
        {
            $result[] = [
                $ad->ad_id,
                $ad->make->name,
                $ad->model->name,
                $ad->year,
                $this->_getUserDetails($ad->user),
                $this->_getAdStatus($ad),
                $ad->created_at->format('Y-m-d'),
                $this->_getActionButtons($ad)
            ];
        }

        return $result;
    }

    /**
     * @param $filters
     * @return mixed
     */
    private function _total($filters)
    {
        unset($filters['total'], $filters['offset']);

        return $this->adRepo->filter($filters, true);
    }

    /**
     * @param $user
     * @return string
     */
    private function _getUserDetails($user)
    {
        $html = fullUserName($user) . '<br />';

        $html .= '<a href="mailto:' . $user->email . '">' . $user->email . '</a>';

        return $html;
    }

    /**
     * @param $ad
     * @return string
     */
    private function _getAdStatus($ad)
    {
        $status = '<span class="label label-success">Approved</span>';

        if ($ad->disabled)
        {
            $status = '<span class="label label-danger">Blocked</span>';
        }

        if ( ! $ad->viewed_by_admin)
        {
            $status .= '&nbsp;<span class="label label-primary">New</span>';
        }

        return $status;
    }

    /**
     * @param $ad
     * @return string
     */
    private function _getActionButtons($ad)
    {
        $html = '<a href="' . adLink($ad) . '" target="_blank" class="btn btn-info">'
            . '<span class="glyphicon glyphicon-search"></span>'
            . '</a>';

        $html .= '&nbsp;';

        if ($ad->disabled)
        {
            $html .= '<a href="#" data-ad-id="' . $ad->ad_id . '" data-disabled="' . $ad->disabled . '" class="btn btn-success ad-approval" role="button">'
                . '<span class="glyphicon glyphicon-thumbs-up"></span>'
            . '</a>';
        }
        else
        {
            $html .= '<a href="#" data-ad-id="' . $ad->ad_id . '" data-disabled="' . $ad->disabled . '" class="btn btn-danger ad-approval" role="button">'
                . '<span class="glyphicon glyphicon-thumbs-down"></span>'
                . '</a>';
        }

        return $html;
    }

    /**
     * @param $input
     * @param $filters
     * @return mixed
     */
    private function _getCustomFilters($input, $filters)
    {
        if ($input['makeId'])
        {
            $filters['where']['make_id'] = $input['makeId'];
        }

        if ($input['modelId'])
        {
            $filters['where']['model_id'] = $input['modelId'];
        }

        if ($input['status'])
        {
            if ($input['status'] == 'approved')
            {
                $filters['where']['disabled'] = 0;
            }

            if ($input['status'] == 'blocked')
            {
                $filters['where']['disabled'] = 1;
            }

            if ($input['status'] == 'new')
            {
                $filters['where']['viewed_by_admin'] = 0;
            }
        }

        if ($input['userId'])
        {
            $filters['where']['user_id'] = $input['userId'];
        }

        return $filters;
    }
}