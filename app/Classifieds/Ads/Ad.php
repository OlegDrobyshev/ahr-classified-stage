<?php

namespace Classifieds\Ads;

use Classifieds\BaseModel;

/**
 * Class Ad
 * @package Classifieds\Ads
 */
class Ad extends BaseModel
{
    /**
     * @var string
     */
    protected $primaryKey = 'ad_id';

    /**
     * @var array
     */
    protected $fillable = [
        'user_id', 'cond_id', 'make_id', 'model_id', 'year', 'price', 'body_style_id', 'mileage',
        'ext_color_id', 'int_color_id', 'transm_id', 'drive_type_id', 'doors', 'disabled',
        'fuel_type_id', 'vin', 'address', 'city', 'state', 'zip', 'phone', 'desc', 'viewed_by_admin'
    ];

    /**
     * @var array
     */
    protected $appends = ['slug'];

    public static $rules = [
        'user_id' => 'required',
        'cond_id' => 'required|not_in:0',
        'make_id' => 'required|not_in:0',
        'model_id' => 'required|not_in:0|required_with:make_id',
        'year' => 'required|numeric|not_in:0',
        'body_style_id' => 'required|numeric',
        'mileage' => 'required|numeric',
        'ext_color_id' => 'required|not_in:0',
        'int_color_id' => 'required|not_in:0',
        'transm_id' => 'required|not_in:0',
        'drive_type_id' => 'required|not_in:0',
        'fuel_type_id' => 'required|not_in:0',
        'doors' => 'required|numeric',
        'price' => 'required|numeric',
        'vin' => 'alpha_num',
        'address' => 'required',
        'city' => 'required',
        'state' => '',
        'zip' => 'required',
        'phone' => ''
    ];

    /**
     * @return string
     */
    public function getSlugAttribute()
    {
        return adSlug($this);
    }

    /**
     * @param $value
     * @return string
     */
    public function getPriceAttribute($value)
    {
        return number_format($value);
    }

    /**
     * @param $value
     * @return string
     */
    public function setPriceAttribute($value)
    {
        $this->attributes['price'] = (int) str_replace(',', '', $value);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('Classifieds\Users\User', 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function condition()
    {
        return $this->belongsTo('Classifieds\Conditions\Condition', 'cond_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function make()
    {
        return $this->belongsTo('Classifieds\Makes\Make', 'make_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function model()
    {
        return $this->belongsTo('Classifieds\Models\Model', 'model_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function bodyStyle()
    {
        return $this->belongsTo('Classifieds\BodyStyles\BodyStyle', 'body_style_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function transmission()
    {
        return $this->belongsTo('Classifieds\Transmissions\Transmission', 'transm_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function driveType()
    {
        return $this->belongsTo('Classifieds\DriveTypes\DriveType', 'drive_type_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function fuelType()
    {
        return $this->belongsTo('Classifieds\FuelTypes\FuelType', 'fuel_type_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function intColor()
    {
        return $this->belongsTo('Classifieds\Colors\Color', 'int_color_id', 'color_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function extColor()
    {
        return $this->belongsTo('Classifieds\Colors\Color', 'ext_color_id', 'color_id');
    }
}
