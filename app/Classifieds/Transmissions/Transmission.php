<?php

namespace Classifieds\Transmissions;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Transmission
 * @package Classifieds\Transmissions
 */
class Transmission extends Model
{
    /**
     * @var string
     */
    protected $primaryKey = 'transm_id';

    /**
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * @var bool
     */
    public $timestamps = false;
}
