<?php  namespace Classifieds\Transmissions;

/**
 * Class EloquentTransmissionRepository
 * @package Classifieds\Transmissions
 */
class EloquentTransmissionRepository implements TransmissionRepositoryInterface
{
    private $model;

    function __construct(Transmission $model)
    {
        $this->model = $model;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAll()
    {
        return $this->model->all();
    }
}