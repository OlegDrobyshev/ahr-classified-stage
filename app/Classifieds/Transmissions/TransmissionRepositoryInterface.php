<?php  namespace Classifieds\Transmissions;

/**
 * Interface TransmissionRepositoryInterface
 * @package Classifieds\Transmissions
 */
interface TransmissionRepositoryInterface
{
    public function getAll();
}