<?php  namespace Classifieds\FuelTypes;

/**
 * Interface FuelTypeRepositoryInterface
 * @package Classifieds\FuelTypes
 */
interface FuelTypeRepositoryInterface
{
    public function getAll();
}