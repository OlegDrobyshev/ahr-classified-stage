<?php

namespace Classifieds\FuelTypes;

use Illuminate\Database\Eloquent\Model;

/**
 * Class FuelType
 * @package Classifieds\FuelTypes
 */
class FuelType extends Model
{
    /**
     * @var string
     */
    protected $primaryKey = 'fuel_type_id';

    /**
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * @var bool
     */
    public $timestamps = false;
}
