<?php  namespace Classifieds\FuelTypes;

/**
 * Class EloquentFuelTypeRepository
 * @package Classifieds\FuelTypes
 */
class EloquentFuelTypeRepository implements FuelTypeRepositoryInterface
{
    private $model;

    function __construct(FuelType $model)
    {
        $this->model = $model;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAll()
    {
        return $this->model->all();
    }
}