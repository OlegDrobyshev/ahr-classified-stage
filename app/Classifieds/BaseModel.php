<?php  namespace Classifieds;

use Hash;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Validator;

/**
 * Class BaseModel
 * @package Classifieds
 */
class BaseModel extends Authenticatable {

    /**
     * Stores errors
     *
     * @var
     */
    protected $errors;

    public static $rules = [];

    public static $errorMessages = [];

    public static function boot()
    {
        parent::boot();

        static::saving(function($model)
        {
            return $model->validate();
        });
    }

    /**
     * @return bool
     */
    public function validate()
    {
        $validation = Validator::make($this->getAttributes(), static::$rules, static::$errorMessages);

        foreach ($this->getAttributes() as $name => $value)
        {
            if ( strpos($name, '_confirmation') !== false)
            {
                unset($this->{$name});
            }

            if ($name == 'password')
            {
                if (Hash::needsRehash($value))
                {
                    $this->{$name} = Hash::make($value);
                }
            }
        }

        // Remove _confirmation attributes
        if ($validation->fails())
        {
            $this->errors = $validation->messages();

            return false;
        }

        return true;
    }

    /**
     * @return mixed
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * @param $errors
     */
    public function setErrors($errors)
    {
        $this->errors = $errors;
    }
}