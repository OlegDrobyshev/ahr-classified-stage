<?php  namespace Classifieds\Conditions;

/**
 * Class EloquentConditionRepository
 * @package Classifieds\Conditions
 */
class EloquentConditionRepository implements ConditionRepositoryInterface
{
    private $model;

    function __construct(Condition $model)
    {
        $this->model = $model;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getAll()
    {
        return $this->model->all();
    }
}