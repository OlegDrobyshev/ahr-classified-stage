<?php

namespace Classifieds\Conditions;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Condition
 * @package App\Classifieds\Conditions
 */
class Condition extends Model
{
    /**
     * @var string
     */
    protected $primaryKey = 'cond_id';

    /**
     * @var array
     */
    protected $fillable = ['name'];

    /**
     * @var bool
     */
    public $timestamps = false;
}
