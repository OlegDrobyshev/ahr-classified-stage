<?php  namespace Classifieds\Conditions;

/**
 * Class ConditionRepositoryInterface
 * @package Classifieds\Conditions
 */
interface ConditionRepositoryInterface
{
    public function getAll();
}