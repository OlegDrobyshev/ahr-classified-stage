<?php  namespace Classifieds\Payments;

/**
 * Class EloquentPaymentRepository
 * @package Classifieds\Payments
 */
class EloquentPaymentRepository implements PaymentRepositoryInterface
{
    private $model;

    function __construct(Payment $model)
    {
        $this->model = $model;
    }

    /**
     * @param $data
     * @return static
     */
    public function create($data)
    {
        return $this->model->create($data);
    }

    /**
     * @param array $dateRange
     * @return mixed
     */
    public function totalProfit(array $dateRange = [])
    {
        $query = $this->model;

        if ($dateRange)
        {
            $query = $query->whereBetween('created_at', $dateRange);
        }

        return $query->sum('amount');
    }

    /**
     * @param $params
     * @param bool $onlyCount
     * @return mixed
     */
    public function filter($params, $onlyCount = false)
    {
        $params = $this->_setFilterDefaults($params);

        $query = $this->model;

        // Relations
        if (isset($params['relations']))
        {
            $query = $query->with($params['relations']);
        }

        // Where statement
        if (isset($params['where']))
        {
            foreach ($params['where'] as $column => $value)
            {
                $method = is_array($value) ? 'whereIn' : 'where';

                $query = $query->{$method}($column, $value);
            }
        }

        // Where between statement
        if (isset($params['whereBetween']))
        {
            foreach ($params['whereBetween'] as $column => $range)
            {
                $query = $query->whereBetween('payments.' . $column, $range);
            }
        }

        // Where Like statement
        if (isset($params['whereLike']))
        {
            foreach ($params['whereLike'] as $column => $value)
            {
                $query = $query->where($column, 'LIKE', '%' . $value . '%');
            }
        }

        // Search by key word
//        if (isset($params['key_word']))
//        {
//            $query = $query->whereHas('make', function($q) use ($params)
//            {
//                $q->where('name', 'LIKE', '%' . $params['key_word'] . '%');
//            });
//        }

        // Order statement
        if (isset($params['order']))
        {
            foreach ($params['order'] as $column => $dir)
            {
                $column = explode('.', $column);

                if (count($column) == 3)
                {
                    $query->join($column[0], $column[0] . '.' . $column[1], '=', 'payments.' . $column[1])
                        ->orderBy($column[0] . '.' . $column[2], $dir);
                }
                else
                {
                    $query = $query->orderBy($column[0], $dir);
                }
            }
        }

        // Enable Laravel's simple paginate
        if (isset($params['simplePaginate']))
        {
            return $query->simplePaginate($params['simplePaginate']);
        }

        // Enable Laravel's paginate
        if (isset($params['paginate']))
        {
            return $query->paginate($params['paginate']);
        }

        // Limit statement
        if (isset($params['total']))
        {
            $query = $query->limit($params['total']);
        }

        // Offset
        if (isset($params['offset']))
        {
            $query = $query->skip($params['offset']);
        }

        if ($onlyCount) return $query->count();

        return $query->get();
    }

    /**
     * @param $params
     * @return array
     */
    private function _setFilterDefaults($params)
    {
        $defaults = [
            'relations' => ['user', 'plan', 'gateway']
        ];

        return array_merge_recursive($params, $defaults);
    }
}