<?php  namespace Classifieds\Payments; 

use Classifieds\BaseModel;

/**
 * Class Payment
 * @package Classifieds\Payments
 */
class Payment extends BaseModel
{

    /**
     * @var string
     */
    protected $primaryKey = 'payment_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'plan_id', 'amount', 'status', 'gateway_id'
    ];

    /**
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('Classifieds\Users\User', 'user_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function plan()
    {
        return $this->belongsTo('Classifieds\Plans\Plan', 'plan_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function gateway()
    {
        return $this->belongsTo('Classifieds\Gateways\Gateway', 'gateway_id');
    }

}