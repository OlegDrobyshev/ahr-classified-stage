<?php  namespace Classifieds\Payments;

/**
 * Interface GatewayServiceInterface
 * @package Classifieds\Payments
 */
interface GatewayServiceInterface
{
    public function pay(array $data);
}