<?php  namespace Classifieds\Payments;

/**
 * Interface PaymentRepositoryInterface
 * @package Classifieds\Payments
 */
interface PaymentRepositoryInterface {
    public function filter($params);
    public function totalProfit(array $dateRange = []);
    public function create($data);
}