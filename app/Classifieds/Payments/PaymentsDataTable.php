<?php  namespace Classifieds\Payments; 

use Classifieds\DataTables\DataTableFilters;
use Classifieds\DataTables\DataTableInterface;

/**
 * Class PaymentsDataTable
 * @package Classifieds\Payments
 */
class PaymentsDataTable implements DataTableInterface
{
    use DataTableFilters;

    /**
     * @var PaymentRepositoryInterface
     */
    private $paymentRepo;

    /**
     * @param PaymentRepositoryInterface $paymentRepo
     */
    function __construct(PaymentRepositoryInterface $paymentRepo)
    {
        $this->paymentRepo = $paymentRepo;
    }

    /**
     * @param array $input
     * @return mixed
     */
    public function getResponse(array $input)
    {
        $columns = [
            ['db' => 'payment_id', 'dt' => 0],
            ['db' => 'email', 'dt' => 1, 'relation' => 'users.user_id'],
            ['db' => 'amount', 'dt' => 2],
            ['db' => 'status', 'dt' => 3],
            ['db' => 'gateway_id', 'dt' => 4, 'relation' => 'gateways.gateway_id'],
            ['db' => 'created_at', 'dt' => 5]
        ];

        $filters = $this->getFilters($columns, $input);

        $filters = $this->_getCustomFilters($filters, $input);

        $payments = $this->paymentRepo->filter($filters);

        $total = $this->_total($filters);

        return [
            'draw' => $input['draw'],
            'recordsTotal' => $total,
            'recordsFiltered' => $total,
            'data' => $this->format($payments)
        ];
    }

    /**
     * @param $payments
     * @return array
     */
    public function format($payments)
    {
        $result = [];

        foreach ($payments as $payment)
        {
            $result[] = [
                $payment->payment_id,
                $this->_getUserInfo($payment),
                $this->_getPaymentAmount($payment),
                $this->getPaymentStatus($payment),
                $this->_getPaymentGateway($payment),
                $this->_getPaymentDate($payment)
            ];
        }

        return $result;
    }

    /**
     * @param $filters
     * @return mixed
     */
    private function _total($filters)
    {
        unset($filters['total'], $filters['offset']);

        return $this->paymentRepo->filter($filters, true);
    }

    /**
     * @param $payment
     * @return string
     */
    private function _getUserInfo($payment)
    {
        return '<b>' . fullUserName($payment->user) . '</b><br />'
            . '<a href="mailto:' . $payment->user->email . '" >' . $payment->user->email . '</a>';
    }

    /**
     * @param $payment
     * @return mixed
     */
    private function _getPaymentGateway($payment)
    {
        return $payment->gateway->name;
    }

    /**
     * @param $payment
     * @return mixed
     */
    private function _getPaymentDate($payment)
    {
        return $payment->created_at->format('Y-m-d');
    }

    /**
     * @param $payment
     * @return string
     */
    private function _getPaymentAmount($payment)
    {
        return '$' . $payment->amount;
    }

    /**
     * @param $payment
     * @return string
     */
    private function getPaymentStatus($payment)
    {
        $status = '<span class="label label-success">Approved</span>';

        if ( ! $payment->status)
        {
            $status = '<span class="label label-danger">Declined</span>';
        }

        return $status;
    }

    /**
     * @param $filters
     * @param $input
     * @return mixed
     */
    private function _getCustomFilters($filters, $input)
    {
        if ($input['gatewayId'])
        {
            $filters['where']['gateway_id'] = $input['gatewayId'];
        }

        if ($input['dateRange'])
        {
            $date = explode('-', $input['dateRange']);

            $dateRange = [
                date('Y-m-d H:i:s', strtotime($date[0])),
                date('Y-m-d H:i:s', strtotime($date[1]))
            ];

            $filters['whereBetween']['created_at'] = $dateRange;
        }

        if (($input['status'] !== '') && ($input['status'] != 'any'))
        {
            $filters['where']['status'] = $input['status'];
        }

        return $filters;
    }
}