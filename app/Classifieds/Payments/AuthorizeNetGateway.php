<?php  namespace Classifieds\Payments; 

use Payum\Core\Reply\ReplyInterface;
use Payum\Core\Request\Capture;
use Payum\Core\Request\GetHumanStatus;
use Payum\LaravelPackage\Controller\PayumController;

/**
 * Class AuthorizeNetGateway
 * @package Classifieds\Payments
 */
class AuthorizeNetGateway extends PayumController implements GatewayServiceInterface
{
    /**
     * @var string
     */
    private $gateway = 'authorize_net';

    /**
     * @var string
     */
    private $afterRoute = 'payment-fake-route';

    /**
     * @param array $data
     * @return array|\Symfony\Component\HttpFoundation\Response
     */
    public function pay(array $data)
    {
        $details = $this->saveInStorage($data);

        $captureToken = $this->getCaptureToken($details);

        $gateway = $this->getGateway($captureToken);

        try
        {
            $gateway->execute(new Capture($captureToken));
        }
        catch (ReplyInterface $reply)
        {
            return $this->convertReply($reply);
        }

        $paymentDetails = $this->getTransactionDetails($gateway, $captureToken);

        return $this->parsePaymentDetails($paymentDetails);
    }

    /**
     * @param $data
     * @return mixed
     */
    private function saveInStorage($data)
    {
        $storage = $this->getStorage();

        $details = $storage->create();

        foreach ($data as $key => $value)
        {
            $details[$key] = $value;
        }

        $storage->update($details);

        return $details;
    }

    /**
     * @param $details
     * @return mixed
     */
    private function getCaptureToken($details)
    {
        return $this->getPayum()
            ->getTokenFactory()
            ->createCaptureToken($this->gateway, $details, $this->afterRoute);
    }

    /**
     * @param $captureToken
     * @return mixed
     */
    private function getGateway($captureToken)
    {
        return $this->getPayum()
            ->getGateway($captureToken->getGatewayName());
    }

    /**
     * @param $gateway
     * @param $captureToken
     * @return GetHumanStatus
     */
    private function getTransactionDetails($gateway, $captureToken)
    {
        $gateway->execute($status = new GetHumanStatus($captureToken));

        return $status;
    }

    /**
     * @return \Payum\Core\Storage\StorageInterface
     */
    private function getStorage()
    {
        return $this->getPayum()->getStorage('Payum\Core\Model\ArrayObject');
    }

    /**
     * @param $payment
     * @return array
     */
    private function parsePaymentDetails($payment)
    {
        return iterator_to_array($payment->getFirstModel());
    }
}