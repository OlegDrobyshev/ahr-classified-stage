<?php

namespace Classifieds\Models;

use Illuminate\Database\Eloquent\Model as EloquentModel;

/**
 * Class Model
 * @package Classifieds\Models
 */
class Model extends EloquentModel
{
    /**
     * @var string
     */
    protected $primaryKey = 'model_id';

    /**
     * @var array
     */
    protected $fillable = ['make_id', 'name'];

    /**
     * @var bool
     */
    public $timestamps = false;
}
