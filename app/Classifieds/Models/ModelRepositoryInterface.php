<?php  namespace Classifieds\Models;

/**
 * Interface ModelsRepositoryInterface
 * @package Classifieds\Models
 */
interface ModelRepositoryInterface
{
    public function getAll();
    public function getByMakeId($makeId);
}