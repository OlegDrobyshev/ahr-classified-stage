<?php  namespace App\Console\Commands;

use App\Jobs\SendAccountExpirationNotification;
use App\Jobs\SendAccountExpiredNotification;
use Classifieds\Users\UserRepositoryInterface;
use Illuminate\Console\Command;
use Illuminate\Foundation\Bus\DispatchesJobs;

/**
 * Class BlockAccounts
 * @package App\Console\Commands
 */
class BlockAccounts extends Command
{
    use DispatchesJobs;

    /**
     * @var string
     */
    protected $signature = 'block-accounts';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Block expired accounts';

    /**
     * Execute the console command.
     *
     * @param UserRepositoryInterface $userRepo
     * @return mixed
     */
    public function handle(UserRepositoryInterface $userRepo)
    {
        $this->blockExpiredAccounts($userRepo);

        $this->sendExpireTomorrowNotification($userRepo);
    }

    /**
     * @param UserRepositoryInterface $userRepo
     */
    public function blockExpiredAccounts(UserRepositoryInterface $userRepo)
    {
        $date = date('Y-m-d');

        $users = $userRepo->filter([
            'where' => [
                'expires' => $date
            ]
        ]);

        foreach ($users as $user)
        {
            $user->expires = '0000-00-00';

            $user->status = 2;

            $user->save();

            $this->dispatchNow(new SendAccountExpiredNotification($user));
        }
    }

    /**
     * @param UserRepositoryInterface $userRepo
     */
    private function sendExpireTomorrowNotification(UserRepositoryInterface $userRepo)
    {
        $notificationDate = date('Y-m-d', strtotime('-1 day', time()));

        $users = $userRepo->filter([
            'where' => [
                'expires' => $notificationDate
            ]
        ]);

        foreach ($users as $user)
        {
            $this->dispatchNow(new SendAccountExpirationNotification($user));
        }
    }
}