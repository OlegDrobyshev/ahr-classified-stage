<?php

namespace App\Jobs;

use App\Jobs\Job;
use Classifieds\Ads\AdRepositoryInterface;
use Classifieds\AdsPhotos\AdsPhotoRepositoryInterface;
use Classifieds\AdsVideos\AdsVideoRepositoryInterface;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Session;

/**
 * Class UpdateAd
 * @package App\Jobs
 */
class UpdateAd extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    private $adId;

    private $adPhotosIds;

    private $adVideosIds;

    private $adData;

    public function __construct($data)
    {
        $this->adId = $data['ad_id'];

        $this->adPhotosIds = isset($data['ad_photo_id']) ? $data['ad_photo_id'] : [];

        $this->adVideosIds = isset($data['ad_video_id']) ? $data['ad_video_id'] : [];

        unset($data['ad_id'], $data['ad_photo_id'], $data['ad_video_id']);

        $this->adData = $data;
    }

    /**
     * @param AdRepositoryInterface $adRepo
     * @param AdsPhotoRepositoryInterface $adPhotoRepo
     * @param AdsVideoRepositoryInterface $adVideoRepo
     * @return mixed
     */
    public function handle(AdRepositoryInterface $adRepo,
                           AdsPhotoRepositoryInterface $adPhotoRepo,
                           AdsVideoRepositoryInterface $adVideoRepo)
    {
        $ad = $adRepo->update($this->adId, $this->adData);

        if ($ad->status)
        {
            $this->bindPhotos($adPhotoRepo);

            $this->bindVideos($adVideoRepo);
        }

        $this->flashMessage($ad);

        return $ad;
    }

    /**
     * @param AdsPhotoRepositoryInterface $adPhotoRepo
     */
    private function bindPhotos(AdsPhotoRepositoryInterface $adPhotoRepo)
    {
        foreach ($this->adPhotosIds as $photoId)
        {
            $adPhotoRepo->update($photoId, [
                'ad_id' => $this->adId
            ]);
        }
    }

    /**
     * @param AdsVideoRepositoryInterface $adVideoRepo
     */
    private function bindVideos(AdsVideoRepositoryInterface $adVideoRepo)
    {
        foreach ($this->adVideosIds as $videoId)
        {
            $adVideoRepo->update($videoId, [
                'ad_id' => $this->adId
            ]);
        }
    }

    /**
     * @param $ad
     */
    private function flashMessage($ad)
    {
        $type = 'success';

        $adTitle = adTitle($ad);

        $message = '<strong>' . $adTitle . '</strong> was successfully updated';

        if ($ad->getErrors())
        {
            $type = 'warning';

            $message = 'Oops, <strong>' . $adTitle . '</strong> was not updated';
        }

        Session::flash('message', [
            'type' => $type,
            'text' => $message
        ]);
    }
}
