<?php

namespace App\Jobs;

use App\Events\SubscriptionPaymentAttempt;
use App\Jobs\Job;
use Classifieds\Gateways\GatewayRepositoryInterface;
use Classifieds\Payments\GatewayServiceInterface;
use Classifieds\Plans\PlanRepositoryInterface;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Class PayForSubscription
 * @package App\Jobs
 */
class PayForSubscription extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    private $data;

    private $user;

    public function __construct($data, $user)
    {
        $this->data = $data;

        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @param GatewayServiceInterface $gateway
     * @return mixed
     */
    public function handle(GatewayServiceInterface $gateway)
    {
        $paymentDetails = $this->buildPaymentDetails();

        $payment = $gateway->pay($paymentDetails);

        $payment = $this->extendPaymentDetails($payment);

        event(new SubscriptionPaymentAttempt($payment, $this->user));

        return $payment;
    }

    /**
     * @return mixed
     */
    private function buildPaymentDetails()
    {
        return [
            'amount' => $this->getPlanPriceById(),
            'exp_date' => $this->data['card_expiry'],
            'card_num' => $this->data['card_number'],
            'first_name' => $this->user['first_name'],
            'last_name' => $this->user['surname'],
            'address' => $this->user['address'],
            'city' => $this->user['city'],
            'stat' => $this->user['state'],
            'zip_code' => $this->user['zip'],
            'phone' => $this->user['phone'],
            'email_address' => $this->user['email']
        ];
    }

    /**
     * @return mixed
     */
    public function getPlanPriceById()
    {
        $planRepo = app()->make(PlanRepositoryInterface::class);

        return $planRepo->getById($this->data['plan_id'])->price;
    }

    /**
     * @param $payment
     * @return mixed
     */
    private function extendPaymentDetails($payment)
    {
        $payment['gateway_id'] = $this->data['gateway_id'];

        $payment['plan_id'] = $this->data['plan_id'];

        return $payment;
    }
}
