<?php

namespace App\Jobs;

use App\Jobs\Job;
use Classifieds\AdsVideos\AdsVideoRepositoryInterface;
use Config;
use File;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Class DeleteAdVideo
 * @package App\Jobs
 */
class DeleteAdVideo extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * @var
     */
    private $adVideoId;

    /**
     * @param $adVideoId
     */
    public function __construct($adVideoId)
    {
        $this->adVideoId = $adVideoId;
    }

    /**
     * Execute the job.
     *
     * @param AdsVideoRepositoryInterface $adVideoRepo
     */
    public function handle(AdsVideoRepositoryInterface $adVideoRepo)
    {
        $adVideo = $adVideoRepo->getById($this->adVideoId)[0];

        $thumbnailsSizes = Config::get('image.thumbnails_sizes');

        File::delete(public_path($adVideo->image));

        foreach ($thumbnailsSizes as $thumbName => $size)
        {
            $file = public_path(getThumbnail($adVideo->image, $thumbName));

            File::delete($file);
        }

        return $adVideoRepo->delete($this->adVideoId);
    }
}
