<?php

namespace App\Jobs;

use App\Jobs\Job;
use Classifieds\AdsVideos\AdsVideoRepositoryInterface;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AddAdVideo extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * @var
     */
    private $data;

    /**
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @param AdsVideoRepositoryInterface $adVideoRepo
     */
    public function handle(AdsVideoRepositoryInterface $adVideoRepo)
    {
        return $adVideoRepo->create($this->data);
    }
}
