<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Mail;

/**
 * Class SendAccountExpirationNotification
 * @package App\Jobs
 */
class SendAccountExpirationNotification extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * @var
     */
    private $user;

    /**
     * @param $user
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $user = $this->user;

        $subject = 'Your account will expire tomorrow';

        $viewData = [
            'text' => 'Hello ' . $user->first_name . '!<br />'
                . '<br />Your account will expire tomorrow. All your ads will be available in catalog but potential customers will not be able to contact you.'
                . '<br /><br />You can always update your subscription from your <a href="http://americanhotrod.com/login">account</a>'
        ];

        Mail::queue('emails.account-expiration', $viewData, function ($message) use ($user, $subject)
        {
            $message->subject($subject);

            $message->to($user->email);
        });
    }
}
