<?php

namespace App\Jobs;

use App\Jobs\Job;
use Classifieds\Ads\AdRepositoryInterface;
use Classifieds\AdsPhotos\AdsPhotoRepositoryInterface;
use Classifieds\AdsVideos\AdsVideoRepositoryInterface;
use Classifieds\Users\User;
use File;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class DeleteUserAds extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @param AdRepositoryInterface $adRepo
     */
    public function handle(AdRepositoryInterface $adRepo)
    {
        $ads = $adRepo->getUserAds($this->user->user_id);

        $adsIds = [];

        foreach ($ads as $ad)
        {
            $adsIds[] = $ad->ad_id;

            $this->_deleteAdsFiles($ad->ad_id);
        }

        return $adRepo->delete($adsIds);
    }

    /**
     * @param $adId
     */
    private function _deleteAdsFiles($adId)
    {
        $adPhotoRepo = \App::make(AdsPhotoRepositoryInterface::class);

        $photos = $adPhotoRepo->getByAdId($adId);

        foreach ($photos as $photo)
        {
            $this->_deletePhotoFileAdThumbnails($photo->uri);
        }

        $adVideoRepo = \App::make(AdsVideoRepositoryInterface::class);

        $videos = $adVideoRepo->getByAdId($adId);

        foreach ($videos as $video)
        {
            $this->_deletePhotoFileAdThumbnails($video->image);
        }
    }

    /**
     * @param $file
     */
    private function _deletePhotoFileAdThumbnails($file)
    {
        $thumbnails = ['small', 'medium', 'large'];

        File::delete(public_path($file));

        foreach ($thumbnails as $thumbnail)
        {
            File::delete(public_path(getThumbnail($file, $thumbnail)));
        }
    }
}
