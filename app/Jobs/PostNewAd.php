<?php

namespace App\Jobs;

use App\Events\NewAdWasPosted;
use App\Jobs\Job;
use Classifieds\Ads\AdRepositoryInterface;
use Classifieds\AdsPhotos\AdsPhotoRepositoryInterface;
use Classifieds\AdsVideos\AdsVideoRepositoryInterface;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Class PostNewAd
 * @package App\Jobs
 */
class PostNewAd extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $newAd;

    /**
     * Create a new job instance.
     * @param $input
     */
    public function __construct($input)
    {
        $this->newAd = array_filter($input);

        $this->newAd['disabled'] = 1;
    }

    /**
     * Execute the job.
     *
     * @param AdRepositoryInterface $adRepo
     * @param AdsPhotoRepositoryInterface $adPhotoRepo
     * @param AdsVideoRepositoryInterface $adVideoRepo
     * @return
     */
    public function handle(AdRepositoryInterface $adRepo,
                           AdsPhotoRepositoryInterface $adPhotoRepo,
                           AdsVideoRepositoryInterface $adVideoRepo)
    {
        $ad = $adRepo->create($this->newAd);

        if (isset($this->newAd['ad_photo_id']))
        {
            foreach ($this->newAd['ad_photo_id'] as $adPhotoId)
            {
                $adPhotoRepo->update($adPhotoId, ['ad_id' => $ad->ad_id]);
            }
        }

        if (isset($this->newAd['ad_video_id']))
        {
            foreach ($this->newAd['ad_video_id'] as $adVideoId)
            {
                $adVideoRepo->update($adVideoId, ['ad_id' => $ad->ad_id]);
            }
        }

        event(new NewAdWasPosted());

        return $ad;
    }
}
