<?php

namespace App\Jobs;

use App\Jobs\Job;
use Classifieds\AdsPhotos\AdsPhotoRepositoryInterface;
use Config;
use File;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class DeleteAdPhoto extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * @var
     */
    private $adPhotoId;

    /**
     * @param $adPhotoId
     */
    public function __construct($adPhotoId)
    {
        $this->adPhotoId = $adPhotoId;
    }

    /**
     * Execute the job.
     *
     * @param AdsPhotoRepositoryInterface $adPhotoRepo
     */
    public function handle(AdsPhotoRepositoryInterface $adPhotoRepo)
    {
        $adPhoto = $adPhotoRepo->getById($this->adPhotoId)[0];

        $thumbnailsSizes = Config::get('image.thumbnails_sizes');

        File::delete(public_path($adPhoto->uri));

        foreach ($thumbnailsSizes as $thumbName => $size)
        {
            $file = public_path(getThumbnail($adPhoto->uri, $thumbName));

            File::delete($file);
        }

        return $adPhotoRepo->delete($this->adPhotoId);
    }

}
