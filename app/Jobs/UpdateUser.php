<?php

namespace App\Jobs;

use Classifieds\Users\UserRepositoryInterface;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Redirect;
use Session;

/**
 * Class UpdateUser
 * @package App\Jobs
 */
class UpdateUser extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $userId;

    protected $data;

    public function __construct($userId, $data)
    {
        $this->userId = $userId;

        $this->data = $data;
    }

    /**
     * @param UserRepositoryInterface $userRepo
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function handle(UserRepositoryInterface $userRepo)
    {
        if (isset($this->data['status']) && ($this->data['status'] == 2)) // expired status
        {
            $this->data['expires'] = null;
        }

        $user = $userRepo->update($this->userId, $this->data);

        if ($user->getErrors())
        {
            $backUri = 'admin/user/' . $this->userId . '#password';

            $errors = $user->getErrors();

            return Redirect::to($backUri)->withErrors($errors)->withInput();
        }

        Session::flash('popUpMessages', [
            [
                'type' => 'success',
                'text' => 'User was successfully updated'
            ]
        ]);

        return Redirect::back();
    }
}
