<?php

namespace App\Jobs;

use App\Jobs\Job;
use Classifieds\AdsPhotos\AdsPhotoRepositoryInterface;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Class UpdateAdPhoto
 * @package App\Jobs
 */
class UpdateAdPhoto extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * @var
     */
    private $data;

    /**
     * @var
     */
    private $adPhotoId;

    /**
     * @param $data
     */
    public function __construct($data)
    {
        $this->adPhotoId = $data['adPhotoId'];

        unset($data['adPhotoId']);

        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @param AdsPhotoRepositoryInterface $adPhotoRepo
     */
    public function handle(AdsPhotoRepositoryInterface $adPhotoRepo)
    {
        return $adPhotoRepo->update($this->adPhotoId, $this->data);
    }
}
