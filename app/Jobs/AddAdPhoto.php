<?php

namespace App\Jobs;

use App\Jobs\Job;
use Classifieds\AdsPhotos\AdsPhotoRepositoryInterface;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AddAdPhoto extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $adPhoto;

    /**
     * Create a new job instance.
     * @param $data
     */
    public function __construct($data)
    {
        $this->adPhoto = $data;
    }

    /**
     * Execute the job.
     *
     * @param AdsPhotoRepositoryInterface $adPhotoRepo
     */
    public function handle(AdsPhotoRepositoryInterface $adPhotoRepo)
    {
        $adPhoto = $adPhotoRepo->create($this->adPhoto);

        return $adPhoto;
    }
}
