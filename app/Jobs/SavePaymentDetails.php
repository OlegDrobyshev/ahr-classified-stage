<?php

namespace App\Jobs;

use App\Jobs\Job;
use Classifieds\Payments\PaymentRepositoryInterface;
use Classifieds\Users\UserRepositoryInterface;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\Session;

/**
 * Class SavePaymentDetails
 * @package App\Jobs
 */
class SavePaymentDetails extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels, DispatchesJobs;

    /**
     * @var
     */
    private $data;

    /**
     * @param $data
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @param PaymentRepositoryInterface $paymentRepo
     * @param UserRepositoryInterface $userRepo
     * @return mixed
     */
    public function handle(PaymentRepositoryInterface $paymentRepo,
                           UserRepositoryInterface $userRepo)
    {
        $status = $paymentRepo->create($this->data);

        if ($this->data['status'])
        {
            $this->updateUserSubscription($userRepo, 2);

            $message = [
                'type' => 'success',
                'text' => 'payment was approved'
            ];
        }
        else
        {
            $this->updateUserSubscription($userRepo, 1);

            $message = [
                'type' => 'warning',
                'text' => 'payment was declined. Your current plan - <b>TRIAL</b>'
            ];
        }

        Session::flash('messages', [$message]);

        return $status;
    }

    /**
     * @param UserRepositoryInterface $userRepo
     * @param $planId
     */
    private function updateUserSubscription(UserRepositoryInterface $userRepo, $planId)
    {
        $userRepo->update($this->data['user_id'], [
            'plan_id' => $planId,
            'status' => 1,
            'expires'  => date('Y-m-d', strtotime('+1 month', time()))
        ]);
    }
}
