<?php

namespace App\Jobs;

use App\Events\UserWasDeleted;
use Classifieds\Users\UserRepositoryInterface;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Class DeleteUser
 * @package App\Jobs
 */
class DeleteUser extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    private $userId;

    public function __construct($userId)
    {
        $this->userId = $userId;
    }

    /**
     * Execute the job.
     *
     * @param UserRepositoryInterface $userRepo
     * @return array
     */
    public function handle(UserRepositoryInterface $userRepo)
    {
        $user = $userRepo->getById($this->userId)[0];

        event(new UserWasDeleted($user));

        $message = [
            'type' => 'success',
            'text' => 'User was successfully deleted'
        ];

        if ( ! $userRepo->delete($this->userId))
        {
            $message = [
                'type' => 'danger',
                'text' => 'Oops, something went wrong, please try again later'
            ];
        }

        return [
            'status' => true,
            'messages' => [$message]
        ];
    }
}
