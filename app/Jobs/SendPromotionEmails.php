<?php

namespace App\Jobs;

use Classifieds\Users\UserRepositoryInterface;
use Illuminate\Mail\Mailer;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

/**
 * Class SendPromotionEmails
 * @package App\Jobs
 */
class SendPromotionEmails extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    private $subject;

    private $text;

    public function __construct($subject, $text)
    {
        $this->subject = $subject;

        $this->text = $text;
    }

    /**
     * Execute the job.
     *
     * @param Mailer $mailer
     * @param UserRepositoryInterface $userRepo
     */
    public function handle(Mailer $mailer,
                           UserRepositoryInterface $userRepo)
    {
        $users = $userRepo->filter([
            'where' => [
                'type' => 'seller',
                'disabled' => 0
            ]
        ]);

        $subject = $this->subject;

        $viewData = ['text' => $this->text];

        foreach ($users as $user)
        {
            $mailer->queue('emails.promotion', $viewData, function ($message) use ($user, $subject)
            {
                $message->subject($subject);

                $message->to($user->email);
            });
        }
    }
}
