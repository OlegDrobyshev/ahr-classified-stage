<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

/**
 * Class AppServiceProvider
 * @package App\Providers
 */
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->setRepositories();

        $this->setPayumServiceProvider();
    }

    private function setRepositories()
    {
        $this->app->bind('Classifieds\Users\UserRepositoryInterface', 'Classifieds\Users\EloquentUserRepository');
        $this->app->bind('Classifieds\Plans\PlanRepositoryInterface', 'Classifieds\Plans\EloquentPlanRepository');
        $this->app->bind('Classifieds\Ads\AdRepositoryInterface', 'Classifieds\Ads\EloquentAdRepository');
        $this->app->bind('Classifieds\AdsPhotos\AdsPhotoRepositoryInterface', 'Classifieds\AdsPhotos\EloquentAdsPhotoRepository');
        $this->app->bind('Classifieds\AdsVideos\AdsVideoRepositoryInterface', 'Classifieds\AdsVideos\EloquentAdsVideoRepository');
        $this->app->bind('Classifieds\Makes\MakeRepositoryInterface', 'Classifieds\Makes\EloquentMakeRepository');
        $this->app->bind('Classifieds\Conditions\ConditionRepositoryInterface', 'Classifieds\Conditions\EloquentConditionRepository');
        $this->app->bind('Classifieds\Models\ModelRepositoryInterface', 'Classifieds\Models\EloquentModelRepository');
        $this->app->bind('Classifieds\Colors\ColorRepositoryInterface', 'Classifieds\Colors\EloquentColorRepository');
        $this->app->bind('Classifieds\Transmissions\TransmissionRepositoryInterface', 'Classifieds\Transmissions\EloquentTransmissionRepository');
        $this->app->bind('Classifieds\DriveTypes\DriveTypeRepositoryInterface', 'Classifieds\DriveTypes\EloquentDriveTypeRepository');
        $this->app->bind('Classifieds\FuelTypes\FuelTypeRepositoryInterface', 'Classifieds\FuelTypes\EloquentFuelTypeRepository');
        $this->app->bind('Classifieds\BodyStyles\BodyStyleRepositoryInterface', 'Classifieds\BodyStyles\EloquentBodyStyleRepository');
        $this->app->bind('Classifieds\Payments\PaymentRepositoryInterface', 'Classifieds\Payments\EloquentPaymentRepository');
        $this->app->bind('Classifieds\Gateways\GatewayRepositoryInterface', 'Classifieds\Gateways\EloquentGatewayRepository');
        $this->app->bind('Classifieds\Payments\GatewayServiceInterface', 'Classifieds\Payments\AuthorizeNetGateway');
    }

    private function setPayumServiceProvider()
    {
        $this->app->resolving('payum.builder', function (\Payum\Core\PayumBuilder $payumBuilder)
        {
            $payumBuilder
                // this method registers filesystem storages, consider to change them to something more
                // sophisticated, like eloquent storage
                ->addDefaultStorages()
                ->addGateway('authorize_net', [
                    'factory'         => 'authorize_net_aim',
                    'login_id'        => '6BtPy72K',
                    'transaction_key' => '9SD8gr6Bn486yX9t',
//                    'sandbox' => true,
                ])
                ->getPayum();
        });
    }
}
