<?php

namespace App\Http\Middleware;

use Closure;

/**
 * Class ActiveAccount
 * @package App\Http\Middleware
 */
class ActiveAccount
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();

        if ($user->status == 2) {
            return redirect()->to('/update-subscription');
//            return response('Unauthorized.', 401);
        }

        return $next($request);
    }
}
