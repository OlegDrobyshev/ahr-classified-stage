<?php

namespace App\Http\Middleware;

use Classifieds\Ads\Ad;
use Closure;

class CheckIfUserOwnsAd
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();

        $adId = $request->route('adId');

        if ( ! $user && $adId) {
            return response('Unauthorized.', 401);
        }

        $ad = Ad::find($adId);

        if ($ad)
        {
            if ($ad->user_id != $user->user_id)
            {
                return response('Unauthorized.', 401);

            }
        }

        return $next($request);
    }
}
