<?php

Route::get('/', 'HomeController@index');

Route::get('home', function()
{
    return Redirect::to('/');
});

/** Authorization routes */
Route::auth();

/** Search */
Route::get('results/{condition?}/{make?}/{model?}', 'ResultsController@index')
    ->where('condition', '[\w\d-]+')
    ->where('make', '[\w\d-]+')
    ->where('model', '[\w\d-]+');

Route::post('results-ajax/{page?}', 'ResultsController@getResultsAjax')
    ->where('page', '\d+');

/** Single ad */
Route::get('ad/{ad_id}/{slug}', 'AdsController@show')
    ->where('ad_id', '\d+');

Route::post('contact-seller-ajax', 'AdsController@contactSellerAjax');

/** Print ad version */
//Route::get('print-ad/{ad_id}/{slug}', 'AdsController@printAd')
//    ->where('ad_id', '\d+');

/** Compare page */
Route::get('compare', 'AdsController@compare');

/** Payments */
Route::get('payment-fake-route', ['as' => 'payment-fake-route', 'uses' => function()
{
    return true;
}]);

/** User Ads */
Route::group(['middleware' => ['auth', 'activeAccount']], function()
{
    Route::get('ads', 'AdsController@index');

    Route::get('new-ad', 'AdsController@create');
    Route::post('ad/store', ['as' => 'ad-store', 'uses' => 'AdsController@store']);

    Route::get('ad/edit/{adId}', 'AdsController@edit')
        ->where('adId', '\d+');
    Route::post('ad/update', ['as' => 'ad-update', 'uses' => 'AdsController@update']);

    Route::post('upload-ad-photo-ajax', 'AdsController@uploadAdPhotoAjax');
    Route::post('update-ad-photo-ajax', 'AdsController@updateAdPhotoAjax');
    Route::post('delete-ad-photo-ajax', 'AdsController@deleteAdPhotoAjax');

    Route::post('add-ad-video-ajax', 'AdsController@addAdVideoAjax');
    Route::post('delete-ad-video-ajax', 'AdsController@deleteAdVideoAjax');

    Route::post('get-models-by-make-id-ajax', 'AdsController@getModelByMakeIdAjax');
});

Route::group(['middleware' => ['auth']], function()
{
    Route::get('update-subscription', 'UsersController@updateSubscription');

    Route::post('subscription-do-update', 'UsersController@doSubscriptionUpdate');
});

/** Admin routes */
Route::group(['middleware' => ['auth', 'admin']], function()
{
    Route::get('admin/dashboard', 'Admin\DashboardController@index');

    Route::get('admin/', function ()
    {
        return Redirect::to('admin/dashboard');
    });

    Route::post('admin/update-subscription-price-ajax', 'Admin\DashboardController@updateSubscriptionPriceAjax');

    /** Admin: users */
    Route::get('admin/users', 'Admin\UsersController@index');

    Route::get('admin/user/{userId}', 'Admin\UsersController@show');

    Route::post('admin/user-delete/', 'Admin\UsersController@deleteAjax');

    Route::post('admin/update-user-password', 'Admin\UsersController@updateUserPassword');

    Route::post('admin/users/data-table-ajax-source', 'Admin\UsersController@dataTableAjaxSource');

    Route::post('admin/update-user-status-ajax', 'Admin\UsersController@updateUserStatusAjax');

    /** Admin: ads */
    Route::get('admin/ads', 'Admin\AdsController@index');

    Route::post('admin/ads/data-table-ajax-source', 'Admin\AdsController@dataTableAjaxSource');

    Route::post('admin/get-models-by-make-id-ajax', 'Admin\AdsController@getModelsByMakeIdAjax');

    Route::post('admin/search-seller-email-ajax', 'Admin\AdsController@searchSellerEmailAjax');

    Route::post('admin/approve-ad-ajax', 'Admin\AdsController@approveAdAjax');

    /** Admin: payments */
    Route::get('admin/payments', 'Admin\PaymentsController@index');

    Route::post('admin/payments/data-table-ajax-source', 'Admin\PaymentsController@dataTableAjaxSource');

    /** Admin: send mass emails */
    Route::get('admin/send-emails', 'Admin\EmailsController@index');

    Route::post('admin/initialize-email-sending', 'Admin\EmailsController@send');
});

/** Temp routes */
//Route::get('import', 'ImportController@index');
//
//Route::post('create-thumbs', 'ImportController@createThumbs');
//Route::get('make-thumbs', 'ImportController@makeThumbs');
//
//
//Route::get('remove-photos', 'ImportController@removePhotos');
//Route::post('make-remove-photos', 'ImportController@makeRemovePhotos');

/** Helper routes */
Route::get('info', function() {
    phpinfo();
});