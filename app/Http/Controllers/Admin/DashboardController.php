<?php

namespace App\Http\Controllers\Admin;

use Classifieds\Services\Admin\AdsControllerService;
use Classifieds\Services\Admin\DashboardControllerService;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Request;
use Response;

class DashboardController extends BaseAdminController
{
    /**
     * @var AdsControllerService
     */
    private $service;

    function __construct(DashboardControllerService $service)
    {
        $this->service = $service;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $newAds = $this->getNewAds();

        $subPrice = $this->service->getSubscriptionPrice();

        $stat = json_encode([
            'users' => $this->service->getRegisteredUsersStat(),
            'ads' => $this->service->getAdsStat(),
            'profit' => $this->service->getProfitStat(),
            'subscription_price' => $subPrice
        ]);

        $chartsData = json_encode([
            'ads_users' => $this->service->getAdsUsersChartData(),
            'profit' => $this->service->getProfitChartData()
        ]);

        $recentAds = $this->service->getAds([
            'order' => ['created_at' => 'desc'],
            'total' => 5,
        ]);

        $recentUsers = $this->service->getUsers([
            'order' => ['created_at' => 'desc'],
            'total' => 6
        ]);

        return view('admin.dashboard', compact(
            'newAds', 'stat', 'chartsData', 'recentAds', 'recentUsers', 'subPrice'
        ));
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateSubscriptionPriceAjax()
    {
        if (Request::ajax())
        {
            $input = Input::only('value');

            $message = $this->service->updateSubscriptionPrice($input['value']);

            return Response::json([$message]);
        }
    }
}
