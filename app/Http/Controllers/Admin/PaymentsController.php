<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use Classifieds\Payments\PaymentsDataTable;
use Classifieds\Services\Admin\PaymentsControllerService;
use Illuminate\Support\Facades\Input;
use Request;

/**
 * Class PaymentsController
 * @package App\Http\Controllers\Admin
 */
class PaymentsController extends BaseAdminController
{
    private $service;

    private $paymentsDataTable;

    function __construct(PaymentsControllerService $service,
                         PaymentsDataTable $paymentsDataTable)
    {
        $this->service = $service;

        $this->paymentsDataTable = $paymentsDataTable;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $gateways = $this->service->getGateways();

        $statuses = $this->service->getPaymentStatuses();

        $newAds = $this->getNewAds();

        return view('admin.payments', compact('newAds', 'gateways', 'statuses'));
    }

    /**
     * @return mixed
     */
    public function dataTableAjaxSource()
    {
        if (Request::ajax())
        {
            return $this->paymentsDataTable->getResponse(Input::all());
        }
    }
}
