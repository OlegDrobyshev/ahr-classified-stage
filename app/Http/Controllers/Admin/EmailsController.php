<?php namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use Classifieds\Services\EmailsControllerService;
use Illuminate\Support\Facades\Input;

/**
 * Class EmailsController
 * @package App\Http\Controllers\Admin
 */
class EmailsController extends BaseAdminController
{
    /**
     * @var EmailsControllerService
     */
    private $service;

    function __construct(EmailsControllerService $service)
    {
        $this->service = $service;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $totalUsers = $this->service->getTotalUsers();

        $newAds = $this->getNewAds();

        return view('admin.send-emails', compact('newAds', 'totalUsers'));
    }

    /**
     * @return mixed
     */
    public function send()
    {
        $input = Input::only('subject', 'text');

        return $this->service->sendEmailToUsers($input);
    }
}
