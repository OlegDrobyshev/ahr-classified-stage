<?php namespace App\Http\Controllers\Admin;

use Classifieds\Ads\AdsDataTable;
use App\Jobs\DeleteUser;
use App\Jobs\UpdateUser;
use Classifieds\Services\Admin\AdsControllerService;
use Classifieds\Services\Admin\UsersControllerService;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Redirect;
use Request;
use Response;


class AdsController extends BaseAdminController
{
    private $adsDataTable;

    private $service;

    function __construct(AdsControllerService $service,
                         AdsDataTable $adsDataTable)
    {
        $this->adsDataTable = $adsDataTable;

        $this->service = $service;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $filters = $this->service->getAdsFiltersValues();

        $newAds = $this->getNewAds();

        return view('admin.ads', compact('newAds', 'filters'));
    }

    /**
     * @return mixed
     */
    public function dataTableAjaxSource()
    {
        if (Request::ajax())
        {
            return $this->adsDataTable->getResponse(Input::all());
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getModelsByMakeIdAjax()
    {
        if (Request::ajax())
        {
            extract(Input::only('makeId'));

            $models = $this->service->getModelsByMakeId($makeId);

            return Response::json($models);
        }
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function searchSellerEmailAjax()
    {
        if (Request::ajax())
        {
            extract(Input::only('q'));

            $users = $this->service->searchSellerByEmail($q);

            return Response::json(['items' => $users]);
        }
    }

    /**
     * Update ad approval status
     */
    public function approveAdAjax()
    {
        if (Request::ajax())
        {
            $input = Input::only('disabled', 'adId');

            $response = $this->service->updateApprovalStatus($input['adId'], $input['disabled']);

            return Response::json($response);
        }
    }
}
