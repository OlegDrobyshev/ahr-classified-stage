<?php

namespace App\Http\Controllers\Admin;

use App\Jobs\DeleteUser;
use App\Jobs\UpdateUser;
use Classifieds\Services\Admin\UsersControllerService;
use Classifieds\Users\UsersDataTable;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Request;
use Response;

/**
 * Class UsersController
 * @package App\Http\Controllers\Admin
 */
class UsersController extends BaseAdminController
{
    /**
     * @var UsersDataTable
     */
    private $usersDataTable;

    /**
     * @var UsersControllerService
     */
    private $service;

    function __construct(UsersDataTable $usersDataTable,
                        UsersControllerService $service)
    {
        $this->usersDataTable = $usersDataTable;
        $this->service = $service;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $newAds = $this->getNewAds();

        return view('admin.users', compact('newAds'));
    }

    /**
     * @param $userId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($userId)
    {
        $user = $this->service->getUserById($userId);

        $newAds = $this->getNewAds();

        return view('admin.user', compact('newAds', 'user'));
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function deleteAjax()
    {
        if ( Request::ajax())
        {
            $input = Input::only('userId');

            $response = $this->dispatchNow(new DeleteUser($input['userId']));

            return Response::json($response);
        }
    }

    /**
     * Update user status ajax
     */
    public function updateUserStatusAjax()
    {
        if (Request::ajax())
        {
            extract(Input::all());

            $this->dispatchNow(new UpdateUser($userId, [
                'status' => $value
            ]));
        }
    }

    /**
     * @return mixed
     */
    public function dataTableAjaxSource()
    {
        if (Request::ajax())
        {
            return $this->usersDataTable->getResponse(Input::all());
        }
    }

    /**
     * @return $this|\Illuminate\Http\RedirectResponse
     */
    public function updateUserPassword()
    {
        $input = Input::only('userId', 'password', 'password_confirmation');

        $userId = $input['userId'];

        $data = [
            'password' => $input['password'],
            'password_confirmation' => $input['password_confirmation']
        ];

        return $this->dispatchNow(new UpdateUser($userId, $data));
    }
}
