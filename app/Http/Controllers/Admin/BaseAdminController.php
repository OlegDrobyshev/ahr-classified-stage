<?php

namespace App\Http\Controllers\Admin;

use App;
use Classifieds\Ads\AdRepositoryInterface;
use Classifieds\AdsPhotos\AdsPhotoRepositoryInterface;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

/**
 * Class BaseAdminController
 * @package App\Http\Controllers\Admin
 */
class BaseAdminController extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    /**
     * @return mixed
     */
    protected function getNewAds()
    {
        $adRepo = \App::make(AdRepositoryInterface::class);

        $adPhotoRepo = \App::make(AdsPhotoRepositoryInterface::class);

        $ads = $adRepo->filter([
            'where' => ['viewed_by_admin' => 0],
            'order' => ['created_at' => 'desc'],
            'total' => 7
        ]);

        $result = [];

        foreach ($ads as $ad)
        {
            $ad->photos = $adPhotoRepo->getByAdId($ad->ad_id);

            $result[] = $ad;
        }

        return $result;
    }
}
