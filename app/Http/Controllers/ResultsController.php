<?php  namespace App\Http\Controllers;

use Classifieds\Services\ResultsControllerService;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Request;

/**
 * Class ResultsController
 * @package Http\Controllers
 */
class ResultsController extends Controller
{
    private $service;

    function __construct(ResultsControllerService $service)
    {
        $this->service = $service;

        $this->checkCompareList();
    }

    /**
     * @param null $condition
     * @param null $make
     * @param null $model
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($condition = null, $make = null, $model = null)
    {
        $filters = [
            'active' => [],
            'list' => $this->service->getFiltersLists()
        ];

        $ads = $this->service->filter([
            'where' => ['disabled' => 0],
            'order' => ['created_at' => 'desc'],
            'paginate' => 7
        ]);

        $recentAds = $this->getRecentAds(3);

        return view('results.index', compact('filters', 'ads', 'recentAds'));
    }

    /**
     * Returns search results via ajax in JSON format
     */
    public function getResultsAjax()
    {
        if ( ! Request::ajax()) return redirect('404');

        $input = Input::except('page');

        $filters = $this->service->parseFiltersValues($input);

        $ads = $this->service->filter($filters);

        return response()->json($ads);
    }

}