<?php

namespace App\Http\Controllers;

use Classifieds\Ads\AdRepositoryInterface;
use Classifieds\AdsPhotos\AdsPhotoRepositoryInterface;
use Classifieds\AdsVideos\AdsVideoRepositoryInterface;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;
use Illuminate\Support\Facades\Gate;

class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    /**
     * Check if compare items still exist and not disabled
     */
    public function checkCompareList()
    {
        if ( ! isset($_COOKIE['compare'])) return false;

        $adRepo = \App::make(AdRepositoryInterface::class);

        $compareList = (array) json_decode($_COOKIE['compare']);

        $compareList = $adRepo->checkExistenceByIds($compareList);

        setcookie('compare', json_encode($compareList), time() + 60 * 60 * 24 * 365, '/');
    }

    /**
     * @param $collection
     * @param $params
     * @return int
     */
    public function getCountFromCollection($collection, $params)
    {
        foreach ($params as $key => $value)
        {
            $collection = $collection->where($key, $value);
        }

        return $collection->count();
    }

    /**
     * @param $number
     * @return mixed
     */
    public function getRecentAds($number)
    {
        $adRepo = app()->make(AdRepositoryInterface::class);

        $adPhotoRepo = app()->make(AdsPhotoRepositoryInterface::class);

        $adVideoRepo = app()->make(AdsVideoRepositoryInterface::class);

        $ads = $adRepo->filter([
            'where' => [
                'disabled' => 0,
            ],
            'order' => [
                'created_at' => 'desc'
            ],
            'total' => $number
        ]);

        foreach ($ads as $key => $ad)
        {
            $ads[$key]->photos = $adPhotoRepo->getByAdId($ad->ad_id);

            $ads[$key]->videos = $adVideoRepo->getByAdId($ad->ad_id);
        }

        return $ads;
    }

}
