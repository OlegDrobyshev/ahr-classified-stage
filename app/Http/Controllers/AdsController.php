<?php

namespace App\Http\Controllers;

use App\Jobs\AddAdPhoto;
use App\Jobs\AddAdVideo;
use App\Jobs\DeleteAdPhoto;
use App\Jobs\DeleteAdVideo;
use App\Jobs\PostNewAd;
use App\Jobs\UpdateAd;
use App\Jobs\UpdateAdPhoto;
use Classifieds\Services\AdMediaService;
use Classifieds\Services\AdsControllerService;
use Classifieds\Services\ResultsControllerService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Input;
use Redirect;
use Request;
use Response;

/**
 * Class AdsController
 * @package App\Http\Controllers
 */
class AdsController extends Controller
{
    /**
     * @var AdsControllerService
     */
    private $service;

    /**
     * @var ResultsControllerService
     */
    private $resultsService;

    /**
     * @var AdMediaService
     */
    private $mediaService;

    /**
     * @var
     */
    protected $user;

    /**
     * @param AdsControllerService $service
     * @param ResultsControllerService $resultsService
     * @param AdMediaService $mediaService
     */
    function __construct(AdsControllerService $service,
                         ResultsControllerService $resultsService,
                         AdMediaService $mediaService)
    {
        $this->middleware('ownsAd', ['only' => [
            'edit', 'update'
        ]]);

        $this->service = $service;

        $this->resultsService = $resultsService;

        $this->mediaService = $mediaService;

        $this->user = Auth::user();

        $this->checkCompareList();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        list($disabled, $params) = $this->service->getAllUserAdsFilterParams();

        $ads = $this->service->getAllUserAds($params, $disabled);

        $ads = $this->service->getUserAdsCountsByStatus($ads);

        $recentAds = $this->getRecentAds(3);

        return view('ads.public.all', compact('ads', 'recentAds'))
            ->withUser($this->user);
    }

    /**
     * @return mixed
     */
    public function create()
    {
        $oldInput = $this->service->getOldMediaInput();

        $selects = $this->service->getSelectElements();

        $recentAds = $this->getRecentAds(3);

        return view('ads.public.create', compact('selects', 'oldInput', 'recentAds'))
            ->withUser($this->user);
    }

    /**
     * @param $adId
     * @return mixed
     */
    public function edit($adId)
    {
        $ad = $this->service->getAddById($adId);

        $this->authorize('edit-ad', $ad);

        $selects = $this->service->getSelectElements();

        $recentAds = $this->getRecentAds(3);

        return view('ads.public.edit', compact('selects', 'ad', 'recentAds'))
            ->withUser($this->user);
    }

    /**
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update()
    {
        $input = Input::all();

        unset($input['image_alt'], $input['photos'], $input['_token'], $input['add-video-input']);

        $ad = $this->dispatchNow(new UpdateAd($input));

        if ($ad->getErrors())
        {
            $oldMakeId = Request::only('make_id')['make_id'];

            $models = $this->service->getModelsByMakeId($oldMakeId);

            return redirect()->back()
                ->withErrors($ad->getErrors())
                ->withInput()
                ->with('models', $models);
        }

        return Redirect::to('ads');
    }

    /**
     * @param $adId
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show($adId)
    {
        if ($this->user && ($this->user->type == 'admin'))
        {
            $this->service->viewedByAdmin($adId);
        }

        $ad = $this->service->getAddById($adId);

        $adOwner = $this->service->getUserById($ad->user_id);

        $recentAds = $this->getRecentAds(3);

        return view('ads.public.single', compact('ad', 'adOwner', 'recentAds'))
            ->withUser($this->user);
    }

    /**
     * @param $adId
     * @param $slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
//    public function printAd($adId, $slug)
//    {
//        @todo not sure that customer needs separate print page

//        $ad = $this->service->getAddById($adId);
//
//        return view('ads.public.print', compact('ad'));
//    }

    /**
     * @return mixed
     */
    public function compare()
    {
        $adsIds = isset($_COOKIE['compare']) ? json_decode($_COOKIE['compare']) : [];

        $ads = $this->service->getByAdsIds($adsIds);

        $recentAds = $this->getRecentAds(3);

        return view('ads.public.compare', compact('ads', 'recentAds'))
            ->withUser($this->user);
    }

    /**
     * Create new ad
     */
    public function store()
    {
        $input = Input::except('photos');

        $ad = $this->dispatchNow(new PostNewAd($input));

        if ( ! $ad->exists)
        {
            $oldMakeId = Request::only('make_id')['make_id'];

            $models = $this->service->getModelsByMakeId($oldMakeId);

            return redirect()->back()
                ->withErrors($ad->getErrors())
                ->withInput()
                ->with('models', $models);
        }

        return Redirect::to('/ads');
    }

    /**
     * @return bool
     */
    public function contactSellerAjax()
    {
        if ( ! Request::ajax()) return false;

        $input = Input::only('name', 'email', 'text', 'adId', 'g-recaptcha-response');

        $data = $this->service->emailSeller($input);

        return Response::json($data);
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function addAdVideoAjax()
    {
        extract(Input::only('provider', 'id'));

        $directory = "ads-photos/{$this->user->user_id}/";

        // Get video thumbnail
        $data = $this->mediaService->saveVideoThumbnail($provider, $id, $directory);

        // Save video
        $video = $this->dispatchNow(new AddAdVideo($data));

        $video->thumbnail = getThumbnail($video->image, 'small');

        // Return video id and thumbnail
        return Response::json($video);
    }

    /**
     * Delete Ad video
     */
    public function deleteAdVideoAjax()
    {
        extract(Input::only('adVideoId'));

        $this->dispatchNow(new DeleteAdVideo($adVideoId));
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function uploadAdPhotoAjax()
    {
        $input = Input::only('photos');

        $directory = "ads-photos/{$this->user->user_id}/";

        $result = $this->mediaService->uploadPhotos($input, 'photos', $directory);

        $photos = [];

        foreach ($result['uploaded'] as $photo)
        {
            unset($photo['thumbnail']);

            $photo = $this->dispatchNow(new AddAdPhoto($photo));

            $photo->thumbnail = getThumbnail($photo->uri, 'small');

            $photos[] = $photo;
        }

        $result['uploaded'] = $photos;

        return Response::json($result);
    }

    /**
     * Update ad photo ajax
     */
    public function updateAdPhotoAjax()
    {
        $input = Input::all();

        $this->dispatchNow(new UpdateAdPhoto($input));
    }

    /**
     * Delete ad photo ajax
     */
    public function deleteAdPhotoAjax()
    {
        $input = Input::only('adPhotoId');

        $this->dispatchNow(new DeleteAdPhoto($input['adPhotoId']));
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function getModelByMakeIdAjax()
    {
        if (Request::ajax())
        {
            $input = Input::only('makeId');

            $models = $this->service->getModelsByMakeId($input['makeId']);

            return Response::json($models);
        }
    }

}