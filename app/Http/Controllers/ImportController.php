<?php

namespace App\Http\Controllers;

use Classifieds\Services\AdMediaService;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;

/**
 * Class ImportController
 * @package App\Http\Controllers
 */
class ImportController extends Controller
{
    public $forImport;

    public $db;

    private $media;

    function __construct(AdMediaService $media)
    {
//        Artisan::call('migrate:refresh');
//
//        Artisan::call('db:seed');
//
//        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
//
//        $this->forImport = DB::connection('for-import');
//
//        $this->db = new DB();
//
        $this->media = $media;
    }

    public function removePhotos()
    {
        $totalPhotos = DB::table('ads_photos')->count();

        return view('temp2', compact('totalPhotos'));
    }

    public function makeRemovePhotos()
    {
        $input = Input::all();

        $baseDirectory = '/storage/ads-photos/';

        $allFiles = [];

        $deletedFiles = 0;

        $leavedFiles = 0;

        $cantDelete = 0;

        $directory = $baseDirectory . $input['dir'];

        if (File::exists(public_path($directory)))
        {
            $files = File::allFiles(public_path($directory));

            $directory = explode('/', $directory);

            $directory = end($directory);

            foreach ($files as $file)
            {
                $file = $baseDirectory . $directory . '/' . $file->getBaseName();

                if (DB::table('ads_photos')->where('uri', $file)->exists())
                {
                    $leavedFiles++;
                } else
                {
                    if (File::delete(public_path($file)))
                    {
                        $deletedFiles++;
                    } else
                    {
                        $cantDelete++;
                    }
                }

                $allFiles[] = $file;
            }
        }

        die(json_encode([
            'deleted' => $deletedFiles,
            'leaved' => $leavedFiles,
            'cant' => $cantDelete,
            'total' => count($allFiles)
        ]));
    }

    public function createThumbs()
    {
        $input = Input::all();

        $ads = DB::table('ads');

        if ($input['offset'])
        {
            $ads = $ads->skip($input['offset']);
        }

        $ads = $ads->limit($input['limit'])->get();

        $affectedAds = 0;

        $affectedPhotos = 0;

        $line = '';

        foreach ($ads as $ad)
        {
            $photos = DB::table('ads_photos')->where('ad_id', $ad->ad_id)->get();

            foreach ($photos as $photo)
            {
                $dir = 'ads-photos/' . $ad->user_id . '/';

                $fullName = explode('/', $photo->uri)[4];

                $fullName = explode('.', $fullName);

                $name = $fullName[0];

                $ext = $fullName[1];

                $this->media->createThumbnails($dir, $name, $ext);

                $affectedPhotos++;

                $line .= $ad->user_id . ' ';
            }

            $affectedAds++;
        }

        die(json_encode([
            'photos' => $affectedPhotos,
            'ads' => $affectedAds,
            'line' => $line
        ]));
    }

    public function makeThumbs()
    {
        $totalAds = $ads = DB::table('ads')->count();

        return view('temp', compact('totalAds'));
    }

    public function index()
    {
        $tables = $this->getTablesList();

        foreach ($tables as $table)
        {
            $method = 'import' . ucfirst($table);

            echo $table . '<br />';

            DB::table($table)->truncate();

            if (method_exists($this, $method))
            {
                $this->{$method}($table);
            }
            else
            {
                $items = $this->forImport->table($table)->get();

                foreach ($items as $item)
                {
                    $item = (array) $item;

                    DB::table($table)->insert($item);
                }
            }
        }
    }

    public function importUsers($table)
    {
        $users = $this->forImport->table($table)->get();

        foreach ($users as $user)
        {
            $user = (array) $user;

            $user['type'] = 'seller';

            $user['status'] = 1;

            unset($user['disabled']);

            $user['expires'] = date('Y-m-d', strtotime('+3 month', time()));

            DB::table($table)->insert($user);
        }
    }

    public function importAds($table)
    {
        $ads = $this->forImport->table($table)->get();

        foreach ($ads as $ad)
        {
            $ad = (array) $ad;

            $ad['body_style_id'] = $ad['body_style'];

            unset($ad['body_style']);

            $ad['viewed_by_admin'] = 1;

            Db::table($table)->insert($ad);
        }
    }

    /**
     * @return array
     */
    private function getTablesList()
    {
        return [
            "body_styles",
            "colors",
            "drive_types",
            "fuel_types",
            "makes",
            "transmissions",
            "users",
            "models",
            "ads",
            "ads_photos",
        ];

        $tables = [];

        $tablesTmp = DB::select('SHOW TABLES');

        foreach ($tablesTmp as $table)
        {
            $table = (array)$table;

            $tables[] = $table['Tables_in_ahr-classified'];
        }

        unset($tables[12], $tables[14], $tables[10], $tables[9], $tables[15], $tables[2], $tables[5], $tables[7], $tables[16]);

        return $tables;
    }
}
