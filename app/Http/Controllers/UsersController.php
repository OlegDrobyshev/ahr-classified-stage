<?php

namespace App\Http\Controllers;

use Auth;
use Classifieds\Services\UsersControllerService;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;

/**
 * Class UsersController
 * @package App\Http\Controllers
 */
class UsersController extends Controller
{
    public $user;

    private $service;

    function __construct(UsersControllerService $service)
    {
        $this->user = Auth::user();

        $this->service = $service;
    }

    /**
     * @return mixed
     */
    public function updateSubscription()
    {
        $plan = $this->service->getPaidPlan();

        return view('users.update-subscription', compact('plan'))
            ->withUser($this->user);
    }

    /**
     * @return mixed
     */
    public function doSubscriptionUpdate()
    {
        $input = Input::only('gateway_id', 'card_number', 'card_expiry');

        return $this->service->doSubscriptionUpdate($input, $this->user);
    }
}
