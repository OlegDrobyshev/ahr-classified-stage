<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Image Driver
    |--------------------------------------------------------------------------
    |
    | Intervention Image supports "GD Library" and "Imagick" to process images
    | internally. You may choose one of them according to your PHP
    | configuration. By default PHP's "GD Library" implementation is used.
    |
    | Supported: "gd", "imagick"
    |
    */

    'driver' => 'gd',

    'thumbnails_sizes' => [
        'small' => [
            'width' => 170,
            'height' => 160
        ],
        'medium' => [
            'width' => 360,
            'height' => 320
        ],
        'large' => [
            'width' => 720,
            'height' => 640
        ]
    ],

    'watermark' => 'assets/public/images/ahr-logo-main.png',

    'canvas-bg' => 'assets/public/images/canvas.png'
);
