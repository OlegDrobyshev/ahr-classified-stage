@extends('layouts.public.app')

@section('header')
    {!! Html::style('/assets/admin/vendors/Buttons/css/buttons.css') !!}
    {!! Html::style('/assets/admin/css/pages/advbuttons.css') !!}
    {!! Html::style('/assets/admin/vendors/card/css/card.css') !!}
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-no-border">
                <div class="panel-heading text-center warning"><h3>Sign Up</h3></div>
                <div class="panel-body">

                    {!! Form::open(['url' => '/register', 'id' => 'registration-form', 'class' => 'form-horizontal']) !!}
                        {!! Form::hidden('gateway_id', 1) !!}
                        {!! Form::hidden('plan_id', old('plan_id', 1)) !!}

                        <div class="col-md-12 text-center">
                            <div class="ui-group-buttons plans">
                                {!! Html::link('#', 'Free Month Trial', ['class' => (old('plan_id', 1) == 1) ? 'btn btn-success' : 'btn btn-default', 'data-plan-id' => 1]) !!}
                                <div class="or"></div>
                                {!! Html::link('#', 'Paid ($' . $plans[1]->price . ')', ['class' => (old('plan_id', 1) == 2) ? 'btn btn-success' : 'btn btn-default', 'data-plan-id' => 2]) !!}
                            </div>
                        </div>

                        <!-- Left column -->
                        <div class="col-md-3 col-md-offset-3">
                            <div class="form-group {{ $errors->has('first_name') ? 'form-group-error' : '' }}">
                                {!! Form::text('first_name', null, ['class' => 'form-control width-90 align-center', 'placeholder' => 'First name *']) !!}
                                @if ($errors->has('first_name'))
                                    <span class="help-block">
                                        {{ $errors->first('first_name') }}
                                    </span>
                                @endif
                            </div>

                            <div class="form-group {{ $errors->has('surname') ? 'form-group-error' : '' }}">
                                {!! Form::text('surname', null, ['class' => 'form-control width-90 align-center', 'placeholder' => 'Surname *']) !!}
                                @if ($errors->has('surname'))
                                    <span class="help-block">
                                        {{ $errors->first('surname') }}
                                    </span>
                                @endif
                            </div>

                            <div class="form-group {{ $errors->has('email') ? 'form-group-error' : '' }}">
                                {!! Form::email('email', null, ['class' => 'form-control width-90 align-center', 'placeholder' => 'Email *']) !!}
                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        {{ $errors->first('email') }}
                                    </span>
                                @endif
                            </div>

                            <div class="form-group {{ $errors->has('password') ? 'form-group-error' : '' }}">
                                {!! Form::password('password', ['class' => 'form-control width-90 align-center', 'placeholder' => 'Password *']) !!}
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        {{ $errors->first('password') }}
                                    </span>
                                @endif
                            </div>

                            <div class="form-group {{ $errors->has('password_confirmation') ? 'form-group-error' : '' }}">
                                {!! Form::password('password_confirmation', ['class' => 'form-control width-90 align-center', 'placeholder' => 'Confirmation *']) !!}
                                @if ($errors->has('password_confirmation'))
                                    <span class="help-block">
                                        {{ $errors->first('password_confirmation') }}
                                    </span>
                                @endif
                            </div>
                        </div><!-- end Left column -->

                        <!-- Right column -->
                        <div class="col-md-3">
                            <div class="form-group {{ $errors->has('address') ? 'form-group-error' : '' }}">
                                {!! Form::text('address', null, ['id' => 'address', 'class' => 'form-control width-90 align-center', 'placeholder' => 'Address *']) !!}
                                @if ($errors->has('address'))
                                    <span class="help-block">
                                        {{ $errors->first('address') }}
                                    </span>
                                @endif
                            </div>

                            <div class="form-group {{ $errors->has('city') ? 'form-group-error' : '' }}">
                                {!! Form::text('city', null, ['id' => 'city', 'class' => 'form-control width-90 align-center', 'placeholder' => 'City *']) !!}
                                @if ($errors->has('city'))
                                    <span class="help-block">
                                        {{ $errors->first('city') }}
                                    </span>
                                @endif
                            </div>

                            <div class="form-group {{ $errors->has('state') ? 'form-group-error' : '' }}">
                                {!! Form::text('state', null, ['id' => 'state', 'class' => 'form-control  width-90 align-center', 'placeholder' => 'State']) !!}
                                @if ($errors->has('state'))
                                    <span class="help-block">
                                        {{ $errors->first('state') }}
                                    </span>
                                @endif
                            </div>

                            <div class="form-group {{ $errors->has('zip') ? 'form-group-error' : '' }}">
                                {!! Form::text('zip', null, ['id' => 'zip', 'class' => 'form-control width-90 align-center', 'placeholder' => 'ZIP']) !!}
                                @if ($errors->has('zip'))
                                    <span class="help-block">
                                        {{ $errors->first('zip') }}
                                    </span>
                                @endif
                            </div>

                            <div class="form-group {{ $errors->has('phone') ? 'form-group-error' : '' }}">
                                {!! Form::text('phone', null, ['class' => 'form-control width-90 align-center', 'placeholder' => 'Phone']) !!}
                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        {{ $errors->first('phone') }}
                                    </span>
                                @endif
                            </div>

                        </div><!-- end Right column -->

                        <!-- Credit card -->
                        <div class="col-md-6 col-md-offset-3 card-block" @if (old('plan_id', 1) == 1) style="display: none;" @endif>
                            <div class="card-wrapper"></div>
                            <br />
                            <div id="card">
                                <div class="form-group">
                                    {!! Form::text('card.number', null, [
                                        'class' => 'form-control width-95 align-center',
                                        'placeholder' => 'Card Number',
                                        'novalidate' => ''
                                    ]) !!}
                                </div>
                                <div class="form-group">
                                    {!! Form::text('card.name', null, [
                                        'class' => 'form-control width-95 align-center',
                                        'placeholder' => 'Name on Card',
                                        'novalidate' => '',
                                        'maxlength' => 40
                                    ]) !!}
                                </div>
                                <div  class="form-group">
                                    {!! Form::text('card.cvc', null, [
                                        'class' => 'form-control width-95 align-center',
                                        'placeholder' => 'CVV',
                                        'novalidate' => ''
                                    ]) !!}
                                </div>
                                <div  class="form-group">
                                    {!! Form::text('card.expiry', null, [
                                        'class' => 'form-control width-95 align-center',
                                        'placeholder' => 'Expiry Date',
                                        'novalidate' => ''
                                    ]) !!}
                                </div>
                            </div>
                        </div><!-- end Credit card -->

                        <div class="col-md-12">
                            <div class="form-group text-center">
                                {!! Form::submit('Register', ['class' => 'btn btn-primary']) !!}
                            </div>
                        </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
    {!! Html::script('//cdn.jsdelivr.net/places.js/1/places.min.js') !!}
    {!! Html::script('/assets/admin/vendors/card/js/card.js') !!}
    {!! Html::script('/assets/public/js/pages/registration.js') !!}
    <script>
        $( function() {
            Registration.init();
            adCreateAddress.init();
        } );
    </script>
@endsection