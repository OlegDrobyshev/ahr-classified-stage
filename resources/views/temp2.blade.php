<html>
<head>
    {!! Html::script('/assets/admin/js/jquery-1.11.3.min.js') !!}
</head>
<body>
<p>Total Ads Photos in DB {{ $totalPhotos }}</p>
Deleted: <span class="removed">0</span><br />
Leaved: <span class="leaved">0</span><br />
Can't delete: <span class="cant">0</span><br />
Total found on disk: <span class="found">0</span><br />

<p>Time: <b class="timer">0</b><b> sec</b></p>
<div></div>

<script>
    $( function() {
        var totalRemoved = $( ".removed" ),
            totalLeaved = $( '.leaved' ),
            totalCant = $( '.cant' ),
            totalFound = $( '.found' ),
            timer = $( '.timer'),
            tData = {
                dir: 2,
                limit: 1
            },
            total = 44;

        $.ajaxSetup({
            headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' }
        });

        var t = setInterval( function() {
            timer.html( parseInt(timer.html()) + 1 );
        }, 1000);

        function dos() {
            $.post( "/make-remove-photos", tData, function (data) {
                data = $.parseJSON( data );

                totalRemoved.html( parseInt(totalRemoved.html()) + data.deleted );
                totalLeaved.html( parseInt(totalLeaved.html()) + data.leaved );
                totalCant.html( parseInt(totalCant.html()) + data.cant );
                totalFound.html( parseInt(totalFound.html()) + data.total );

                tData.dir = tData.dir + tData.limit;

                if ( tData.dir <= total) {
                    dos();
                } else {
                    clearInterval(t);
                }
            } );
        }

        dos();
    } );
</script>
</body>
</html>