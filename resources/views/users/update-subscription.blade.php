@extends('layouts.public.app')

@section('header')
    {!! Html::style('/assets/admin/vendors/Buttons/css/buttons.css') !!}
    {!! Html::style('/assets/admin/css/pages/advbuttons.css') !!}
    {!! Html::style('/assets/admin/vendors/card/css/card.css') !!}
@endsection

@section('content')
    <div class="container">
        <div class="row">

            <!-- Messages -->
            @if (session()->has('messages'))
                <div class="col-md-10 col-md-offset-1">
                    @foreach(session('messages') as $message)
                        <div class="alert alert-{{ $message['type'] }} alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <strong>{{ ucfirst($message['type']) }}:</strong> {!! $message['text'] !!}
                        </div>
                    @endforeach
                </div>
            @endif<!-- end Messages -->

            <div class="col-md-12">
                <div class="panel panel-no-border">
                    <div class="panel-heading text-center warning"><h3>Update Subscription (${{ $plan->price }} / month)</h3></div>
                    <div class="panel-body">

                        {!! Form::open(['url' => '/subscription-do-update', 'id' => 'update-subscription-form', 'class' => 'form-horizontal']) !!}
                            {!! Form::hidden('gateway_id', 1) !!}

                            <!-- Credit card -->
                            <div class="col-md-6 col-md-offset-3 card-block">
                                <div class="card-wrapper"></div>
                                <br />
                                <div id="card">
                                    <div class="form-group">
                                        {!! Form::text('card.number', null, [
                                            'class' => 'form-control width-95 align-center',
                                            'placeholder' => 'Card Number',
                                            'novalidate' => ''
                                        ]) !!}
                                    </div>
                                    <div class="form-group">
                                        {!! Form::text('card.name', null, [
                                            'class' => 'form-control width-95 align-center',
                                            'placeholder' => 'Name on Card',
                                            'novalidate' => '',
                                            'maxlength' => 40
                                        ]) !!}
                                    </div>
                                    <div  class="form-group">
                                        {!! Form::text('card.cvc', null, [
                                            'class' => 'form-control width-95 align-center',
                                            'placeholder' => 'CVV',
                                            'novalidate' => ''
                                        ]) !!}
                                    </div>
                                    <div  class="form-group">
                                        {!! Form::text('card.expiry', null, [
                                            'class' => 'form-control width-95 align-center',
                                            'placeholder' => 'Expiry Date',
                                            'novalidate' => ''
                                        ]) !!}
                                    </div>
                                </div>
                            </div><!-- end Credit card -->

                            <div class="col-md-12">
                                <div class="form-group text-center">
                                    {!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}
                                </div>
                            </div>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    {!! Html::script('//cdn.jsdelivr.net/places.js/1/places.min.js') !!}
    {!! Html::script('/assets/admin/vendors/card/js/card.js') !!}
    {!! Html::script('/assets/public/js/pages/update-subscription.js') !!}
    <script>
        $( function() {
            UpdateSubscription.init();
        } );
    </script>
@endsection