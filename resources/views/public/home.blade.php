@extends('layouts.public.app')

@section('content')
    <div class="container">
        <div class="row">

            @if (session()->has('messages'))
                <div class="col-md-10 col-md-offset-1">
                    @foreach(session('messages') as $message)
                        <div class="alert alert-{{ $message['type'] }} alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <strong>{{ ucfirst($message['type']) }}:</strong> {!! $message['text'] !!}
                        </div>
                    @endforeach
                </div>
            @endif

            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-no-border">
                    <div class="panel-heading text-center warning"><h3>Dashboard</h3></div>
                    <div class="panel-body">
                        {{--You are logged in!--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
