@extends('layouts.admin.app')

@section('header')
    {!! Html::style('/assets/admin/vendors/modal/css/component.css') !!}
@endsection

@section('content')
    <aside class="right-side">

        <!-- Page title -->
        <section class="content-header">
            <h1>Users</h1>
            <ol class="breadcrumb">
                <li>
                    <a href="/admin/dashboard">
                        <span class="glyphicon glyphicon-home"></span>Dashboard
                    </a>
                </li>
                <li class="active">Users</li>
            </ol>
        </section><!-- end Page title -->

        <!-- Content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">

                    <!-- Datatable block -->
                    <div class="portlet box portlet-white">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="glyphicon glyphicon-user"></i>
                                All Users
                                <div class="pull-right loader-wrap" style="display: none;">
                                    <div class="loader">
                                        {!! Html::image(asset('assets/public/images/ajax-loader.gif')) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <table class="table table-striped table-bordered table-hover datatable" id="users">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Plan</th>
                                        <th>Status</th>
                                        <th>Expires At</th>
                                        <th>Registration Date</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div><!-- end Datatable block -->

                </div>
            </div>
        </section><!-- end Content -->
    </aside>
@endsection

@section('footer')
    {!! Html::script('/assets/admin/vendors/datatables/js/jquery.dataTables.min.js') !!}
    {!! Html::script('/assets/admin/vendors/datatables/js/dataTables.bootstrap.min.js') !!}
    {!! Html::script('/assets/admin/vendors/datatables/extensions/Responsive/js/dataTables.responsive.min.js') !!}
    {!! Html::script('/assets/admin/js/pages/users.js') !!}
    <script>
        $( function() {
            Users.init();
        } );
    </script>
@endsection