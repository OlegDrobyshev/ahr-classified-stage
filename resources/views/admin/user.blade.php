@extends('layouts.admin.app')

@section('header')
    {!! Html::style('/assets/admin/vendors/x-editable/css/bootstrap-editable.css') !!}
@endsection

@section('content')
    <aside class="right-side">

        <!-- Page title -->
        <section class="content-header">
            <h1>{{ fullUserName($user) }} details</h1>
            <ol class="breadcrumb">
                <li>
                    <a href="/admin/dashboard">
                        <span class="glyphicon glyphicon-home"></span>Dashboard
                    </a>
                </li>
                <li>
                    <a href="/admin/users">Users</a>
                </li>
                <li class="active">{{ fullUserName($user) }}</li>
            </ol>
        </section><!-- end Page title -->

        <!-- Content -->
        <section class="content">
            <div class="row">
                <div class="col-lg-12">
                    <!-- Tabs menu -->
                    <ul class="nav nav-tabs ">
                        <li class="active">
                            <a href="#profile" data-toggle="tab">
                                <i class="livicon" data-name="user" data-size="16" data-c="#000" data-hc="#000" data-loop="true"></i>
                                Profile</a>
                        </li>
                        <li>
                            <a href="#password" data-toggle="tab">
                                <i class="livicon" data-name="key" data-size="16" data-loop="true" data-c="#000" data-hc="#000"></i>
                                Change Password</a>
                        </li>
                    </ul><!-- end Tabs menu -->

                    <!-- Tabs content -->
                    <div  class="tab-content mar-top">

                        <!-- User Profile tab -->
                        <div id="profile" class="tab-pane fade active in">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="panel">
                                        <div class="panel-heading">

                                        </div>
                                        <div class="panel-body" style="padding: 0;">
                                            <div class="col-md-4">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail img-file">
                                                        <img class="img-thumbnail center-block full-width"
                                                             src="{{ asset('assets/public/images/no-photo.png') }}"
                                                             alt="{{ fullUserName($user) }}" />
                                                        </div>
                                                </div>
                                            </div>
                                            <div class="col-md-8">
                                                <div class="panel-body">
                                                    <div class="table-responsive">
                                                        <table class="table table-bordered table-striped" id="users">
                                                            <tr>
                                                                <td>User Name</td>
                                                                <td>{{ fullUserName($user) }}</td>
                                                            </tr>
                                                            <tr>
                                                                <td>E-mail</td>
                                                                <td><a href="mailto:{{ $user->email }}">{{ $user->email }}</a></td>
                                                            </tr>
                                                            <tr>
                                                                <td>Status</td>
                                                                <td>
                                                                    <a href="#" id="status" data-type="select" data-pk="1" data-value="{{ $user->status }}" data-title="Status"></a>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>Registration Date</td>
                                                                <td>{{ $user->created_at->format('Y-m-d') }}</td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div><!-- end User Profile tab -->

                        <!-- Reset password tab -->
                        <div id="password" class="tab-pane fade">
                            <div class="row">
                                <div class="col-md-12 pd-top">

                                    <!-- Change user password -->
                                    {!! Form::open(['url' => 'admin/update-user-password', 'class' => 'form-horizontal']) !!}
                                        {!! Form::hidden('userId', $user->user_id) !!}
                                        <div class="form-body">
                                            <div class="form-group">
                                                <label for="inputpassword" class="col-md-3 control-label">
                                                    Password
                                                    <span class='require'>*</span>
                                                </label>
                                                <div class="col-md-9">
                                                    <div class="input-group @if ($errors->first('password')) has-error @endif">
                                                        <span class="input-group-addon">
                                                            <i class="livicon" data-name="key" data-size="16" data-loop="true" data-c="#000" data-hc="#000"></i>
                                                        </span>
                                                        {!! Form::input('password', 'password', null, ['class' => 'form-control']) !!}
                                                    </div>
                                                    {!! $errors->first('password', '<p class="text-danger">:message</p>') !!}
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="inputnumber" class="col-md-3 control-label">
                                                    Confirm Password
                                                    <span class='require'>*</span>
                                                </label>
                                                <div class="col-md-9">
                                                    <div class="input-group @if ($errors->first('password_confirmation')) has-error @endif">
                                                        <span class="input-group-addon">
                                                            <i class="livicon" data-name="key" data-size="16" data-loop="true" data-c="#000" data-hc="#000"></i>
                                                        </span>
                                                        {!! Form::input('password', 'password_confirmation', null, ['class' => 'form-control']) !!}
                                                    </div>
                                                    {!! $errors->first('password_confirmation', '<p class="text-danger">:message</p>') !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions">
                                            <div class="col-md-offset-3 col-md-9">
                                                <button type="submit" class="btn btn-primary">Submit</button>
                                                &nbsp;
                                                <input type="reset" class="btn btn-default hidden-xs" value="Reset">
                                            </div>
                                        </div>
                                    </form><!-- end Change user password -->

                                </div>
                            </div>
                        </div><!-- end Reset password tab -->

                    </div><!-- endTabs content -->
                </div>
            </div>
        </section><!-- end Content -->
    </aside>
@endsection

@section('footer')
    {!! Html::script('/assets/admin/vendors/x-editable/bootstrap-editable.js') !!}
    {!! Html::script('/assets/admin/vendors/x-editable/jquery.mockjax.js') !!}
    {!! Html::script('/assets/admin/js/pages/user.js') !!}
    <script>
        $( function() {
            var userId = '{{ $user->user_id }}';
            User.init( {
                userId: userId,
                popUpMessages: $.parseJSON( '{!! json_encode(Session::get("popUpMessages")) !!}' )
            } );
        } );
    </script>
@endsection