@extends('layouts.admin.app')

@section('header')
    {!! Html::style('/assets/admin/vendors/modal/css/component.css') !!}
    {!! Html::style('/assets/admin/vendors/daterangepicker/css/daterangepicker-bs3.css') !!}
@endsection

@section('content')
    <aside class="right-side">

        <!-- Page title -->
        <section class="content-header">
            <h1>Payments</h1>
            <ol class="breadcrumb">
                <li>
                    <a href="/admin/dashboard">
                        <span class="glyphicon glyphicon-home"></span>Dashboard
                    </a>
                </li>
                <li class="active">Payments</li>
            </ol>
        </section><!-- end Page title -->

        <!-- Content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">

                    <!-- Datatable block -->
                    <div class="portlet box portlet-white">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="glyphicon glyphicon-credit-card"></i>
                                All Payments
                                <div class="pull-right loader-wrap" style="display: none;">
                                    <div class="loader">
                                        {!! Html::image(asset('assets/public/images/ajax-loader.gif')) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="portlet-body">

                            <!-- Ads filters -->
                            <div class="col-md-12 ads-filters">
                                <div class="col-md-3 no-padding">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            {!! Form::text('date_range', null, ['class' => 'form-control full-width date-range']) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        {!! Form::select('gateway_id', $gateways, 'any', [
                                            'class' => 'form-control select2 full-width gateway-id',
                                            'data-placeholder' => 'Gateway'
                                        ]) !!}
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        {!! Form::select('status', $statuses, 'any', [
                                            'class' => 'form-control select2 full-width status',
                                            'data-placeholder' => 'Status'
                                        ]) !!}
                                    </div>
                                </div>
                                <div class="col-md-2 no-padding">
                                    <a href="#" class="btn btn-sm btn-warning clear">Clear Filters</a>
                                </div>
                            </div><!-- end client filters -->

                            <table class="table table-striped table-bordered table-hover datatable" id="payments">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>User</th>
                                        <th>Amount</th>
                                        <th>Status</th>
                                        <th>Gateway</th>
                                        <th>Date</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div><!-- end Datatable block -->

                </div>
            </div>
        </section><!-- end Content -->
    </aside>
@endsection

@section('footer')
    {!! Html::script('/assets/admin/vendors/datatables/js/jquery.dataTables.min.js') !!}
    {!! Html::script('/assets/admin/vendors/datatables/js/dataTables.bootstrap.min.js') !!}
    {!! Html::script('/assets/admin/vendors/datatables/extensions/Responsive/js/dataTables.responsive.min.js') !!}
    {!! Html::script('/assets/admin/vendors/daterangepicker/daterangepicker.js') !!}
    {!! Html::script('/assets/admin/js/pages/payments.js') !!}
    <script>
        $( function() {
            Payments.init();
        } );
    </script>
@endsection