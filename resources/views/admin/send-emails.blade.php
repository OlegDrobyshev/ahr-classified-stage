@extends('layouts.admin.app')

@section('header')
@endsection

@section('content')
    <aside class="right-side">

        <!-- Page title -->
        <section class="content-header">
            <h1>Send emails</h1>
            <ol class="breadcrumb">
                <li>
                    <a href="/admin/dashboard">
                        <span class="glyphicon glyphicon-home"></span>Dashboard
                    </a>
                </li>
                <li class="active">Send emails</li>
            </ol>
        </section><!-- end Page title -->

        <!-- Content -->
        <section class="content">
            <div class="row">

                <div class="col-md-6">
                    <div class="panel">
                        <div class="panel-heading border-light">
                            <h4 class="panel-title">
                                <span class="glyphicon glyphicon-envelope"></span>
                                Email
                            </h4>
                        </div>
                        <div class="panel-body">
                            {!! Form::open(['url' => 'admin/initialize-email-sending']) !!}
                                <div class="form-group @if ($errors->first('subject')) has-error @endif">
                                    {!! Form::text('subject', null, ['class' => 'form-control', 'placeholder' => 'Email Title']) !!}
                                </div>
                                <div class="form-group" @if ($errors->first('text')) style="border: 1px solid #EF6F6C;" @endif>
                                    {!! Form::textarea('text', null, ['id' => 'body', 'class' => 'form-control', 'placeholder' => 'Email Text']) !!}
                                </div>
                                <div class="form-group text-right">
                                    {!! Form::submit('Send', ['class' => 'btn btn-primary']) !!}
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="panel">
                        <div class="panel-heading border-light">
                            <h4 class="panel-title">
                                <span class="glyphicon glyphicon-user"></span>
                                Total users
                            </h4>
                        </div>
                        <div class="panel-body">
                            <p class="text-center" style="font-size: 18px;">
                                Will be sent to <h2 class="text-center img-circle email-total-users" style="color: #444;">{{ $totalUsers }}</h2>
                                <p class="text-center" style="font-size: 18px;">users</p>
                            </p>
                        </div>
                    </div>
                </div>

            </div>
        </section><!-- end Content -->
    </aside>
@endsection

@section('footer')
    {!! Html::script('/assets/public/vendor/tinymce/js/tinymce/tinymce.min.js') !!}
    {!! Html::script('/assets/admin/js/pages/send-emails.js') !!}
    <script>
        $( function() {
            SendEmails.init( {
                popUpMessages: $.parseJSON( '{!! json_encode(Session::get("popUpMessages")) !!}' )
            } );
        } );
    </script>
@endsection