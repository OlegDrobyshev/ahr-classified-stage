@extends('layouts.admin.app')

@section('header')
    {!! Html::style('/assets/admin/vendors/modal/css/component.css') !!}
@endsection

@section('content')
    <aside class="right-side">

        <!-- Page title -->
        <section class="content-header">
            <h1>Ads</h1>
            <ol class="breadcrumb">
                <li>
                    <a href="/admin/dashboard">
                        <span class="glyphicon glyphicon-home"></span>Dashboard
                    </a>
                </li>
                <li class="active">Ads</li>
            </ol>
        </section><!-- end Page title -->

        <!-- Content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">

                    <!-- Datatable block -->
                    <div class="portlet box portlet-white">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="glyphicon glyphicon-list"></i>
                                All Ads
                                <div class="pull-right loader-wrap" style="display: none;">
                                    <div class="loader">
                                        {!! Html::image(asset('assets/public/images/ajax-loader.gif')) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="portlet-body">

                            <!-- Ads filters -->
                            <div class="col-md-12 ads-filters">
                                <div class="col-md-2 no-padding">
                                    <div class="form-group">
                                        {!! Form::select('make_id', $filters['makes'], 'any', [
                                            'class' => 'form-control select2 full-width',
                                            'data-placeholder' => 'Make'
                                        ]) !!}
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        {!! Form::select('model_id', $filters['models'], 'any', [
                                            'class' => 'form-control select2 full-width',
                                            'disabled' => 'true',
                                            'data-placeholder' => 'Model'
                                        ]) !!}
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        {!! Form::select('status', $filters['status'], 'any', [
                                            'class' => 'form-control select2 full-width',
                                            'data-placeholder' => 'Status'
                                        ]) !!}
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        {!! Form::select('email', $filters['email'], 'any', [
                                            'class' => 'form-control select2 full-width',
                                            'data-placeholder' => 'Seller Email'
                                        ]) !!}
                                    </div>
                                </div>
                                <div class="col-md-2 no-padding">
                                    <a href="#" class="btn btn-sm btn-warning clear">Clear Filters</a>
                                </div>
                            </div><!-- end client filters -->

                            <table class="table table-striped table-bordered table-hover datatable" id="ads">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Make</th>
                                    <th>Model</th>
                                    <th>Year</th>
                                    <th>Seller</th>
                                    <th>Status</th>
                                    <th>Posted Date</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                            </table>
                        </div>
                    </div><!-- end Datatable block -->

                </div>
            </div>
        </section><!-- end Content -->
    </aside>
@endsection

@section('footer')
    {!! Html::script('/assets/admin/vendors/datatables/js/jquery.dataTables.min.js') !!}
    {!! Html::script('/assets/admin/vendors/datatables/js/dataTables.bootstrap.min.js') !!}
    {!! Html::script('/assets/admin/vendors/datatables/extensions/Responsive/js/dataTables.responsive.min.js') !!}
    {!! Html::script('/assets/admin/js/pages/ads.js') !!}
    <script>
        $( function() {
            Ads.init();
        } );
    </script>
@endsection