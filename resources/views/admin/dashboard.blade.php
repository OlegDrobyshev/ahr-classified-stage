@extends('layouts.admin.app')

@section('header')
    {!! Html::style('/assets/admin/vendors/x-editable/css/bootstrap-editable.css') !!}
@endsection

@section('content')
    <aside class="right-side">

        <!-- Page title -->
        <section class="content-header">
            <h1>Dashboard</h1>
            <ol class="breadcrumb">
                <li>
                    <a href="/admin/dashboard">
                        <span class="glyphicon glyphicon-home"></span>Dashboard
                    </a>
                </li>
            </ol>
        </section><!-- end Page title -->

        <!-- Content -->
        <section class="content">
            <div class="row">

                <!-- Registered users -->
                <div class="col-lg-3 col-md-6 col-sm-6 margin_10 animated fadeInRightBig">
                    <!-- Trans label pie charts strats here-->
                    <div class="dashboard-box no-radius">
                        <div class="panel-body squarebox square_boxs">
                            <div class="col-xs-12 pull-left nopadmar">
                                <div class="row">
                                    <div class="square_box col-xs-7 pull-left">
                                        <span>Registered Users</span>
                                        <div class="number" id="myTargetElement1"></div>
                                    </div>
                                    <i class="livicon pull-right" data-name="users" data-l="true" data-c="#888" data-hc="#888" data-s="70"></i>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <small class="stat-label">Last Week</small>
                                        <h4 id="myTargetElement1.1"></h4>
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <small class="stat-label">Last Month</small>
                                        <h4 id="myTargetElement1.2"></h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- end Registered Users -->

                <!-- Total ads -->
                <div class="col-lg-3 col-md-6 col-sm-6 margin_10 animated fadeInLeftBig">
                    <!-- Trans label pie charts strats here-->
                    <div class="dashboard-box no-radius">
                        <div class="panel-body squarebox square_boxs">
                            <div class="col-xs-12 pull-left nopadmar">
                                <div class="row">
                                    <div class="square_box col-xs-7 text-right">
                                        <span>Total ads</span>
                                        <div class="number" id="myTargetElement2"></div>
                                    </div>
                                    <i class="livicon  pull-right" data-name="list-ul" data-l="true" data-c="#888" data-hc="#888" data-s="70"></i>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <small class="stat-label">Last Week</small>
                                        <h4 id="myTargetElement2.1"></h4>
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <small class="stat-label">Last Month</small>
                                        <h4 id="myTargetElement2.2"></h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- end Total ads -->

                <!-- Total Profit -->
                <div class="col-lg-3 col-md-6 col-sm-6 margin_10 animated fadeInRightBig">
                    <!-- Trans label pie charts strats here-->
                    <div class="dashboard-box no-radius">
                        <div class="panel-body squarebox square_boxs">
                            <div class="col-xs-12 pull-left nopadmar">
                                <div class="row">
                                    <div class="square_box col-xs-7 pull-left">
                                        <span>Total Profit</span>
                                        <div class="number" id="myTargetElement3"></div>
                                    </div>
                                    <i class="livicon pull-right" data-name="piggybank" data-l="true" data-c="#888" data-hc="#888" data-s="70"></i>
                                </div>
                                <div class="row">
                                    <div class="col-xs-6">
                                        <small class="stat-label">Last Week</small>
                                        <h4 id="myTargetElement31"></h4>
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <small class="stat-label">Last Month</small>
                                        <h4 id="myTargetElement32"></h4>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- end Total Profit -->

                <!-- Subscription price -->
                <div class="col-lg-3 col-md-6 col-sm-6 margin_10 animated fadeInRightBig">
                    <!-- Trans label pie charts strats here-->
                    <div class="dashboard-box no-radius">
                        <div class="panel-body squarebox square_boxs">
                            <div class="col-xs-12 pull-left nopadmar">
                                <div class="row">
                                    <div class="square_box col-xs-7 pull-left">
                                        <span>Subscription Price</span>
                                        <div class="number" id="subscription-price">{{ $subPrice }}</div>
                                    </div>
                                    <i class="livicon pull-right" data-name="money" data-l="true" data-c="#888" data-hc="#888" data-s="70"></i>
                                </div>
                                <div class="row">
                                    <div class="col-xs-12">
                                        <p class="stat-label" style="line-height: 25px;">
                                            <a href="/admin/subscription" class="text-warning edit-price" data-pk="1" data-url="/post" data-title="Enter price">
                                                <i class="glyphicon glyphicon glyphicon-cog"></i> Edit Price
                                            </a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- end Subscription Price -->
            </div>
<br />
            <div class="row">
                <!-- Users / Ads chart -->
                <div class="col-lg-6">
                    <div class="panel panel-white">
                        <div class="panel-heading border-light">
                            <h3 class="panel-title">
                                <i class="glyphicon glyphicon-list"></i>
                                Total Ads / Users
                            </h3>
                        <span class="pull-right">
                            <i class="glyphicon glyphicon-chevron-up showhide clickable"></i>
                        </span>
                        </div>
                        <div class="panel-body">
                            <div id="basicFlotLegend" class="flotLegend"></div>
                            <div id="line-chart" class="flotChart1" style="height: 300px;"></div>
                        </div>
                    </div>
                </div><!-- end Users / Ads chart -->

                <!-- Total Profit chart -->
                <div class="col-lg-6">
                    <div class="panel panel-white">
                        <div class="panel-heading border-light">
                            <h3 class="panel-title">
                                <i class="glyphicon glyphicon-usd"></i>
                                Total Profit
                            </h3>
                                <span class="pull-right">
                                    <i class="glyphicon glyphicon-chevron-up showhide clickable"></i>
                                </span>
                        </div>
                        <div class="panel-body">
                            <div id="basicFlotLegend2" class="flotLegend"></div>
                            <div id="bar-chart" class="flotChart" style="height: 332px;"></div>
                        </div>
                    </div>
                </div><!-- end Total Profit Chart -->
            </div>

            <div class="row">
                <!-- Recent Ads -->
                <div class="col-md-6">
                    <div class="panel panel-white">
                        <div class="panel-heading border-light">
                            <h4 class="panel-title">
                                <i class="glyphicon glyphicon-list"></i>
                                Recent Ads
                            </h4>
                            <span class="pull-right">
                                <i class="glyphicon glyphicon-chevron-up showhide clickable"></i>
                            </span>
                        </div>
                        <div class="panel-body">
                            <table class="ads-dashboard-table table table-striped table-hover table-v-middle" style="margin:0;">
                                <thead>
                                    <tr>
                                        <th class="text-center">Photo</th>
                                        <th class="text-center">Title</th>
                                        <th class="text-center">Actions</th>
                                    </tr>
                                </thead>
                                <tbody class="text-center">
                                    @if ( ! $recentAds) No ads found @endif
                                    @foreach ($recentAds as $ad)
                                        <tr class="@if ($ad->disabled) disabled-row @else enabled-row @endif">
                                            <td style="width:13.6%;">
                                                <a href="{{ adLink($ad) }}">
                                                    @if ($ad->photos->count())
                                                        <img src="{{ getThumbnail($ad->photos[0]->uri, 'small') }}"
                                                             class="img-thumbnail center-block"
                                                             alt="{{ $ad->photos[0]->desc ?: adTitle($ad) }}" />
                                                    @else
                                                        <img class="img-thumbnail center-block"
                                                             src="{{ asset('assets/public/images/no-photo.png') }}"
                                                             alt="{{ adTitle($ad) }}">
                                                    @endif
                                                </a>
                                            </td>
                                            <td class="text-left">
                                                @if ( ! $ad->viewed_by_admin)
                                                    <span class="label label-primary">New</span>
                                                @endif
                                                {!! Html::link(adLink($ad), adTitle($ad)) !!}
                                                    {{--{{ adTitle($ad) }}--}}
                                            </td>
                                            <td>
                                                <div class="ui-group-buttons">
                                                    <a href="{{ adLink($ad) }}" class="btn btn-default">
                                                        <i class="glyphicon glyphicon-link"></i>
                                                    </a>
                                                    @if ($ad->disabled)
                                                        <a href="#" data-ad-id="{{ $ad->ad_id }}" data-disabled="{{ $ad->disabled }}" class="btn btn-success ad-approval" role="button">
                                                            <span class="glyphicon glyphicon-thumbs-up"></span>
                                                        </a>
                                                    @else
                                                        <a href="#" data-ad-id="{{ $ad->ad_id }}" data-disabled="{{ $ad->disabled }}" class="btn btn-danger ad-approval" role="button">
                                                            <span class="glyphicon glyphicon-thumbs-down"></span>
                                                        </a>
                                                    @endif
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="col-md-12 text-center">
                                <hr style="margin-top:0;" />
                                <a href="/admin/ads">All Ads</a>
                            </div>
                        </div>
                    </div>
                </div><!-- end Recent Ads -->

                <!-- Recent Users -->
                <div class="col-md-6">
                    <div class="panel panel-white">
                        <div class="panel-heading border-light">
                            <h4 class="panel-title">
                                <i class="glyphicon glyphicon-user"></i>
                                Recent Users
                            </h4>
                                <span class="pull-right">
                                    <i class="glyphicon glyphicon-chevron-up showhide clickable"></i>
                                </span>
                        </div>
                        <div class="panel-body">
                            <table class="table table-striped table-hover table-v-middle" style="margin:0;">
                                <thead>
                                    <tr>
                                        <th class="text-center">Name</th>
                                        <th class="text-center">Email</th>
                                        <th class="text-center">Status</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody class="text-center">
                                    @if ( ! $recentUsers) No users found @endif
                                    @foreach ($recentUsers as $user)
                                        <tr>
                                            <td>
                                                <a href="#">{{ fullUserName($user) }}</a>
                                            </td>
                                            <td>{{ $user->email }}</td>
                                            <td>
                                                @if ($user->status == 0)
                                                    <span class="label label-danger">Disabled</span>
                                                @elseif ($user->status == 1)
                                                    <span class="label label-success">Active</span>
                                                @elseif ($user->status == 2)
                                                    <span class="label label-warning">Expired</span>
                                                @endif
                                            </td>
                                            <td>
                                                <a href="/admin/user/{{ $user->user_id }}" class="btn btn-default ">
                                                    <i class="glyphicon glyphicon-link"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="col-md-12 text-center">
                                <hr style="margin-top:0;" />
                                <a href="/admin/users">All Users</a>
                            </div>
                        </div>
                    </div>
                </div><!-- end Recent Users -->
            </div>
        </section><!-- end Content -->

    </aside>
@endsection

@section('footer')
    {!! Html::script('/assets/admin/vendors/x-editable/bootstrap-editable.js') !!}
    {!! Html::script('/assets/admin/vendors/countUp/countUp.js') !!}

    <!-- Charts -->
    {!! Html::script('/assets/admin/vendors/charts/jquery.flot.min.js') !!}
    {!! Html::script('/assets/admin/vendors/charts/jquery.flot.stack.js') !!}
    {!! Html::script('/assets/admin/vendors/charts/jquery.flot.crosshair.js') !!}
    {!! Html::script('/assets/admin/vendors/charts/jquery.flot.time.js') !!}
    {!! Html::script('/assets/admin/vendors/charts/jquery.flot.resize.js') !!}
    {!! Html::script('/assets/admin/vendors/charts/jquery.flot.tooltip.js') !!}

    <script>
        var stat = $.parseJSON( '{!! $stat !!}' ),
            chartsData = $.parseJSON( '{!! $chartsData !!}' ),
            xaxisMin = {{ strtotime(date('Y-01-01')) * 1000 }},
            xaxisMax = {{ strtotime(date('Y-12-01')) * 1000 }};
    </script>

    {!! Html::script('/assets/admin/js/dashboard.js') !!}
@endsection