@extends('layouts.public.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">

                <!-- Compare controls -->
                <div class="col-md-12" style="margin-bottom: 20px;">
                    <div class="box">
                        <div class="box-icon">
                            <i class="livicon icon" data-name="balance" data-size="55" data-loop="true" data-c="#01bc8c" data-hc="#01bc8c"></i>
                        </div>
                        <!-- Compare content -->
                        <div class="row info">
                            <a href="/results" class="col-md-4 text-center filter-status">
                                <h4>
                                    <i class="livicon" data-name="plus-alt" data-size="20" data-loop="true" data-c="#01bc8c" data-hc="#01bc8c"></i> Add
                                </h4>
                            </a>
                            <div class="col-md-4 text-center">
                                <a href="#" class="success"><h3 class="success text-center">Compare</h3></a>
                            </div>
                            <a href="#" class="col-md-4 text-center filter-status compare-clear">
                                <h4>
                                    <i class="livicon" data-name="minus-alt" data-size="20" data-loop="true" data-c="#F60" data-hc="#f60"></i> Clear List
                                </h4>
                            </a>
                        </div><!-- end Compare content -->
                    </div>
                </div><!--end  Compare controls -->

                <!-- Compare Ads -->
                <div class="row">
                    <div class="col-md-12">

                        @if ( ! $ads)
                            <p class="text-center" style="padding-bottom: 300px">No items for comparison, {!! Html::link('/results', 'browse') !!}</p>
                        @else
                            <!-- Compare table -->
                            <div class="table-responsive" style="border-top: 1px solid #eee;">
                                <table class="table table-hover table-striped compare-table">
                                    <thead>
                                        <tr>
                                            <th class="compare-title no-border" style="padding-left: 40px; border-right: 1px solid #eee!important;">
                                                <i class="livicon icon1" data-name="car" data-size="40" data-loop="true" data-c="#f60" data-hc="#f60"></i>
                                            </th>
                                            @foreach ($ads as $ad)
                                                <th class="no-border">
                                                    <h4 class="text-center">{{ adTitle($ad) }}<br />
                                                        <a href="#" class="compare-remove" data-ad-id="{{ $ad->ad_id }}">
                                                             <i class="livicon icon1" data-name="trash" data-size="20" data-loop="true" data-c="#f60" data-hc="#f60"></i>
                                                        </a>
                                                    </h4>
                                                </th>
                                            @endforeach
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="compare-title text-right"><b>Photo</b></td>
                                            @foreach ($ads as $ad)
                                                <td class="">
                                                    @if ($ad->photos->count())
                                                        <img src="{{ getThumbnail($ad->photos[0]->uri, 'large') }}"
                                                             class="img-thumbnail center-block"
                                                             alt="{{ $ad->photos[0]->desc ?: adTitle($ad) }}" />
                                                    @else
                                                        <img class="img-thumbnail center-block"
                                                             src="{{ asset('assets/public/images/no-photo.png') }}"
                                                             alt="{{ adTitle($ad) }}">
                                                    @endif
                                                </td>
                                            @endforeach
                                        </tr>
                                        <tr>
                                            <td class="compare-title text-right"><b>Condition</b></td>
                                            @foreach ($ads as $ad)
                                                <td class="">{{ $ad->condition->name }}</td>
                                            @endforeach
                                        </tr>

                                        <tr>
                                            <td class="compare-title text-right"><b>Make</b></td>
                                            @foreach ($ads as $ad)
                                                <td class="">{{ $ad->make->name }}</td>
                                            @endforeach
                                        </tr>

                                        <tr>
                                            <td class="compare-title text-right"><b>Model</b></td>
                                            @foreach ($ads as $ad)
                                                <td class="">{{ $ad->model->name }}</td>
                                            @endforeach
                                        </tr>

                                        <tr>
                                            <td class="compare-title text-right"><b>Year</b></td>
                                            @foreach ($ads as $ad)
                                                <td class="">{{ $ad->year }}</td>
                                            @endforeach
                                        </tr>

                                        <tr>
                                            <td class="compare-title text-right"><b>Mileage</b></td>
                                            @foreach ($ads as $ad)
                                                <td class="">{{ $ad->mileage }}</td>
                                            @endforeach
                                        </tr>

                                        <tr>
                                            <td class="compare-title text-right"><b>Body Style</b></td>
                                            @foreach ($ads as $ad)
                                                <td class="">{{ $ad->body_style }}</td>
                                            @endforeach
                                        </tr>

                                        <tr>
                                            <td class="compare-title text-right"><b>Exterior Color</b></td>
                                            @foreach ($ads as $ad)
                                                <td class="">{{ $ad->extColor->name }}</td>
                                            @endforeach
                                        </tr>

                                        <tr>
                                            <td class="compare-title text-right"><b>Interior Color</b></td>
                                            @foreach ($ads as $ad)
                                                <td class="">{{ $ad->intColor->name }}</td>
                                            @endforeach
                                        </tr>

                                        <tr>
                                            <td class="compare-title text-right"><b>Transmission</b></td>
                                            @foreach ($ads as $ad)
                                                <td class="">{{ $ad->transmission->name }}</td>
                                            @endforeach
                                        </tr>

                                        <tr>
                                            <td class="compare-title text-right"><b>Drive Type</b></td>
                                            @foreach ($ads as $ad)
                                                <td class="">{{ $ad->driveType->name }}</td>
                                            @endforeach
                                        </tr>

                                        <tr>
                                            <td class="compare-title text-right"><b>Fuel Type</b></td>
                                            @foreach ($ads as $ad)
                                                <td class="">{{ $ad->fuelType->name }}</td>
                                            @endforeach
                                        </tr>

                                        <tr>
                                            <td class="compare-title text-right"><b>Doors</b></td>
                                            @foreach ($ads as $ad)
                                                <td class="">{{ $ad->doors }}</td>
                                            @endforeach
                                        </tr>

                                        <tr>
                                            <td class="compare-title text-right"><b>Price</b></td>
                                            @foreach ($ads as $ad)
                                                <td class="text-success">$ {{ $ad->price }}</td>
                                            @endforeach
                                        </tr>

                                        <tr class="no-bg">
                                            <td class="text-right no-border" style="border-right: 1px solid #eee!important;"></td>
                                            @foreach ($ads as $ad)
                                                <td>
                                                    <br />
                                                    <a href="{{ adLink( $ad ) }}" target="_blank" class="btn btn-default">View</a>
                                                    <a href="#" class="btn btn-primary text-white compare-remove" data-ad-id="{{ $ad->ad_id }}">&times;</a>
                                                </td>
                                            @endforeach
                                        </tr>
                                    </tbody>
                                </table><br />
                            </div><!-- Compare table -->
                        @endif

                    </div>
                </div><!-- Compare Ads -->

            </div>
        </div>
    </div>
@endsection

@section('footer')
    {!! Html::script('/assets/public/js/compare-ads.js') !!}
@endsection