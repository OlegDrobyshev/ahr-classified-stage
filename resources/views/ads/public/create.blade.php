@extends('layouts.public.app')

@section('content')
    <div class="container">
        <div class="row">

            <!-- Title -->
            <div class="col-md-10 col-md-offset-1">
                <div class="col-md-12">
                    <h3 class="text-center success">New Ad</h3>
                </div>
            </div><!-- end Title -->

            {!! Form::open(['route' => 'ad-store', 'id' => 'create-ad-form']) !!}
                {!! Form::hidden('user_id', $user->user_id) !!}

                <!-- Car details -->
                <div class="col-md-5 col-md-offset-1">
                    <div class="box">
                        <div class="box-icon">
                            <i class="livicon icon1" data-name="gears" data-size="55" data-loop="true" data-c="#01bc8c" data-hc="#01bc8c"></i>
                        </div>
                        <div class="info row">
                            <h4 class="success text-center">Car Details</h4>

                            <div class="col-md-12 text-left">
                                <div class="form-group text-left @if ($errors->has('cond_id')) with-error @endif">
                                    {!! Form::select('cond_id', $selects['cond']['values'], null, ['class' => 'form-control select2']) !!}
                                </div>
                                <div class="form-group text-left @if ($errors->has('make_id')) with-error @endif">
                                    {!! Form::select('make_id', $selects['make']['values'], null, ['class' => 'form-control select2']) !!}
                                </div>
                                <div class="form-group text-left @if ($errors->has('model_id')) with-error @endif">
                                    @if (isset($models) && old('model_id'))
                                        {!! Form::select('model_id', $models, old('model_id'), ['class' => 'form-control select2']) !!}
                                    @else
                                        {!! Form::select('model_id', $selects['model']['values'], null, ['class' => 'form-control select2']) !!}
                                    @endif
                                </div>
                                <div class="form-group text-left @if ($errors->has('year')) with-error @endif">
                                    {!! Form::select('year', $selects['year']['values'], null, ['class' => 'form-control select2']) !!}
                                </div>
                                <div class="form-group text-left @if ($errors->has('mileage')) with-error @endif">
                                    {!! Form::text('mileage', null, ['class' => 'form-control', 'placeholder' => 'Mileage *']) !!}
                                </div>
                                <div class="form-group text-left @if ($errors->has('body_style_id')) with-error @endif">
                                    {!! Form::select('body_style_id', $selects['body_style']['values'], null, ['class' => 'form-control select2']) !!}
                                </div>
                                <div class="form-group text-left @if ($errors->has('ext_color_id')) with-error @endif">
                                    {!! Form::select('ext_color_id', $selects['ext_color']['values'], null, ['class' => 'form-control select2']) !!}
                                </div>
                                <div class="form-group text-left @if ($errors->has('int_color_id')) with-error @endif">
                                    {!! Form::select('int_color_id', $selects['int_color']['values'], null, ['class' => 'form-control select2']) !!}
                                </div>
                                <div class="form-group text-left @if ($errors->has('transm_id')) with-error @endif">
                                    {!! Form::select('transm_id', $selects['transm']['values'], null, ['class' => 'form-control select2']) !!}
                                </div>
                                <div class="form-group text-left @if ($errors->has('drive_type_id')) with-error @endif">
                                    {!! Form::select('drive_type_id', $selects['drive_type']['values'], null, ['class' => 'form-control select2']) !!}
                                </div>
                                <div class="form-group text-left @if ($errors->has('fuel_type_id')) with-error @endif">
                                    {!! Form::select('fuel_type_id', $selects['fuel_type']['values'], null, ['class' => 'form-control select2']) !!}
                                </div>
                                <div class="form-group text-left @if ($errors->has('doors')) with-error @endif">
                                    {!! Form::select('doors', $selects['doors']['values'], null, ['class' => 'form-control select2']) !!}
                                </div>
                                <div class="form-group text-left @if ($errors->has('vin')) with-error @endif">
                                    {!! Form::text('vin', null, ['class' => 'form-control', 'placeholder' => 'VIN']) !!}
                                </div>
                                <div class="form-group text-left @if ($errors->has('price')) with-error @endif">
                                    {!! Form::text('price', null, ['class' => 'form-control', 'placeholder' => 'Price *']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- end Car details -->

                <!-- Ad Address -->
                <div class="col-md-5">
                    <div class="box">
                        <div class="box-icon">
                            <i class="livicon icon1" data-name="location" data-size="55" data-loop="true" data-c="#01bc8c" data-hc="#01bc8c"></i>
                        </div>
                        <div class="info">
                            <h4 class="success text-center">Contacts</h4>

                            <div class="form-group text-left @if ($errors->has('address')) with-error @endif">
                                {!! Form::text('address', null, ['id' => 'address', 'class' => 'form-control', 'placeholder' => 'Address *']) !!}
                            </div>
                            <div class="form-group text-left @if ($errors->has('city')) with-error @endif">
                                {!! Form::text('city', null, ['id' => 'city', 'class' => 'form-control', 'placeholder' => 'City *']) !!}
                            </div>
                            <div class="form-group text-left @if ($errors->has('state')) with-error @endif">
                                {!! Form::text('state', null, ['id' => 'state', 'class' => 'form-control', 'placeholder' => 'State']) !!}
                            </div>
                            <div class="form-group text-left @if ($errors->has('zip')) with-error @endif">
                                {!! Form::text('zip', null, ['id' => 'zip', 'class' => 'form-control', 'placeholder' => 'ZIP *']) !!}
                            </div>
                            <div class="form-group text-left @if ($errors->has('phone')) with-error @endif">
                                {!! Form::text('phone', null, ['class' => 'form-control', 'placeholder' => 'Phone']) !!}
                            </div>
                        </div>
                    </div>
                </div><!-- end Ad Address -->

                <!-- Ad media -->
                <div class="col-md-5 media-wrap">
                    <div class="box">
                        <div class="box-icon">
                            <i class="livicon icon1" data-name="folder-open" data-size="55" data-loop="true" data-c="#01bc8c" data-hc="#01bc8c"></i>
                        </div>

                        <!-- Media head buttons -->
                        <div class="row info">
                            <a href="#" class="col-md-4 add-photo-btn success text-center">
                                <h4>
                                    <i class="livicon" data-name="plus-alt" data-size="14" data-loop="true" data-c="#01bc8c" data-hc="#01bc8c"></i> Photo
                                </h4>
                            </a>
                            <div class="col-md-4 text-center">
                                <h4 class="success text-center">Media</h4>
                            </div>
                            <a href="#" class="col-md-4 add-video-btn success text-center">
                                <h4>
                                    <i class="livicon" data-name="plus-alt" data-size="14" data-loop="true" data-c="#01bc8c" data-hc="#01bc8c"></i> Video
                                </h4>
                            </a>
                        </div><!-- end Media head buttons -->

                        <!-- Ad photo file input -->
                        {!! Form::file('photos[]', ['class' => 'add-photo-input hidden', 'multiple' => 'true']) !!}

                        <!-- Add video block -->
                        <div class="add-video-block">
                            {!! Form::text('add-video-input', '', ['class' => 'form-control', 'placeholder' => 'Paste link please']) !!}
                            <div class="btn-group-vertical" role="group">
                                {!! Html::link('#', 'Add', ['class' => 'btn btn-success ad-add-video']) !!}
                                {!! Html::link('#', 'Close', ['class' => 'btn btn-primary ad-add-video-close']) !!}
                            </div>
                            <div class="text-muted text-left">* Only YouTube and Video links allowed</div>
                        </div><!-- end Add video block -->

                        <!-- Message block -->
                        <div class="ad-media-message">No files</div>

                        <!-- Progress bar -->
                        <div class="progress">
                            <div class="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuemin="0" aria-valuemax="100">0%</div>
                        </div><!-- end Progress bar -->

                        <!-- Ad media items -->
                        <div class="info ad-media">
                            @foreach($oldInput['photos'] as $photo)
                                <?php echo $photo->desc; ?>
                                <div class="ad-media-item ad-media-closed" data-type="photo">
                                    <img src="{{ $photo->thumbnail }}" alt="slider-image" class="img-responsive img-rounded" />
                                    <div class="ad-media-item-form">
                                        <input type="hidden" name="ad_photo_id[]" value="{{ $photo->photo_id }}" />
                                        <input type="text" name="image_alt" value="{{ $photo->desc }}" class="form-control" placeholder="Photo description" />
                                        <div class="btn-group-vertical" role="group">
                                            {!! Html::link('#', 'Save', ['class' => 'btn btn-default ad-photo-update']) !!}
                                            {!! Html::link('#', 'Delete', ['class' => 'btn btn-default ad-photo-delete']) !!}
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                            @foreach($oldInput['videos'] as $video)
                                <div class="ad-media-item ad-media-closed" data-type="video">
                                    <img src="{{ $video->thumbnail }}" alt="slider-image" class="img-responsive img-rounded" />
                                    <div class="ad-media-item-form">
                                        <input type="hidden" name="ad_video_id[]" value="{{ $video->video_id }}" />
                                        <div class="btn-group-vertical" role="group">
                                            {!! Html::link('#', 'Remove', ['class' => 'btn btn-default ad-video-delete']) !!}
                                        </div>
                                    </div>
                                </div>
                            @endforeach

                            @for ($i = 1; $i <= 4; $i++)
                                {{--<div class="ad-media-item ad-media-closed" data-type="photo">--}}
                                    {{--<img src="/assets/public/images/15.jpg" alt="slider-image" class="img-responsive img-rounded" />--}}
                                    {{--<div class="ad-media-item-form">--}}
                                        {{--{!! Form::hidden('ad_photo_id[]', '22') !!}--}}
                                        {{--{!! Form::text('image_alt', '', ['class' => 'form-control', 'placeholder' => 'Photo description']) !!}--}}
                                        {{--<div class="btn-group-vertical" role="group">--}}
                                            {{--{!! Html::link('#', 'Save', ['class' => 'btn btn-default ad-photo-update']) !!}--}}
                                            {{--{!! Html::link('#', 'Delete', ['class' => 'btn btn-default ad-photo-delete']) !!}--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            @endfor

                            @for ($i = 1; $i <= 1; $i++)
                                {{--<div class="ad-media-item ad-media-closed" data-type="video">--}}
                                    {{--<img src="/assets/public/images/14.jpg" alt="slider-image" class="img-responsive img-rounded" />--}}
                                    {{--<div class="ad-media-item-form">--}}
                                        {{--<div class="btn-group-vertical" role="group">--}}
                                            {{--{!! Html::link('#', 'Remove', ['class' => 'btn btn-default ad-video-delete']) !!}--}}
                                            {{--{!! Html::link('#', 'Close', ['class' => 'btn btn-default ad-media-close']) !!}--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            @endfor
                        </div><!-- end Add media items -->

                    </div>
                </div><!-- end Ad media -->

                <!-- Ad description -->
                <div class="col-md-10 col-md-offset-1">
                    <div class="box">
                        <div class="box-icon">
                            <i class="livicon icon1" data-name="pencil" data-size="55" data-loop="true" data-c="#01bc8c" data-hc="#01bc8c"></i>
                        </div>
                        <textarea name="desc" id="ad_desc"></textarea>
                    </div>
                </div>

                <!-- Ad footer -->
                <div class="col-md-10 col-md-offset-1 ad-footer">
                    <div class="col-md-9 text-left">
                        <p>Note: your ad will be available after our approval</p>
                    </div>
                    <div class="col-md-3 text-right">
                        {!! Form::submit('Post Ad', ['class' => 'btn btn-primary']) !!}
                        {!! Html::link('/ads', 'Cancel', ['class' => 'btn btn-default']) !!}
                    </div>
                </div><!-- end Ad footer -->

            {!! Form::close() !!}

        </div>
    </div>
@endsection

@section('footer')
    {!! Html::script('//cdn.jsdelivr.net/places.js/1/places.min.js') !!}
    {!! Html::script('/assets/public/vendor/tinymce/js/tinymce/tinymce.min.js') !!}
    {!! Html::script('/assets/public/js/jsVideoUrlParser.min.js') !!}
    {!! Html::script('/assets/public/js/pages/ad_create.js') !!}
    <script>
        $( function() {
            adCreate.init();
            adCreateAddress.init();
        } );
    </script>
@endsection