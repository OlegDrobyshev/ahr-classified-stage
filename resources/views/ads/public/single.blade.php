@extends('layouts.public.app')

@section('header')
    {!! Html::style('/assets/public/vendor/utilcarousel/util.carousel.css') !!}
    {!! Html::style('/assets/public/vendor/utilcarousel/util.carousel.skins.css') !!}
    {!! Html::style('/assets/public/vendor/magnific-popup/magnific-popup.css') !!}
@endsection

@section('content')
    <div class="container">
        <div class="row ad-detailed">
            <!-- Ad -->
            <div class="col-md-12 col-sm-12">

                <!-- Ad info -->
                <div class="col-md-12">

                    <!-- Ad photo -->
                    <div class="col-lg-5 col-md-5 col-sm-12">
                        <a href="{{ adLink($ad) }}" title="{{ adTitle($ad) }}" class="ad-main-photo">
                            @if ($ad->photos->count())
                                <img src="{{ getThumbnail($ad->photos[0]->uri, 'large') }}"
                                     class="img-thumbnail center-block"
                                     alt="{{ $ad->photos[0]->desc ?: adTitle($ad) }}" />
                            @else
                                <img class="img-thumbnail center-block"
                                     src="{{ asset('assets/public/images/no-photo.png') }}"
                                     alt="{{ adTitle($ad) }}">
                            @endif
                        </a>
                    </div><!-- end ad photo -->

                    <!-- Ad details -->
                    <div class="col-lg-7 col-md-7 col-sm-12">

                        <div class="row">
                            <div class="col-md-9 text-left">
                                <h4>
                                    @can('approve-ad')
                                        @if ($ad->disabled)
                                            <a href="#" data-ad-id="{{ $ad->ad_id }}" data-disabled="{{ $ad->disabled }}" class="btn btn-success ad-approval" role="button">
                                                Approve
                                            </a>&nbsp;
                                        @else
                                            <a href="#" data-ad-id="{{ $ad->ad_id }}" data-disabled="{{ $ad->disabled }}" class="btn btn-danger text-white ad-approval" role="button">
                                                Block
                                            </a>&nbsp;
                                        @endif
                                    @endcan
                                    {!! Html::link(adLink($ad), adTitle($ad)) !!}
                                    @can('edit-ad', $ad)
                                        | {!! Html::link('/ad/edit/' . $ad->ad_id, 'Edit') !!}
                                    @endcan
                                </h4>
                            </div>
                            <div class="col-md-3">
                                <span class="pull-right priceText"><h4><b>${{ $ad->price }}</b></h4></span>
                            </div>
                        </div>

                        <div class="dotted-line"></div>

                        <!-- Buttons group -->
                        <div class="row ad-buttons">
                            <div class="col-sm-3">
                                <a href="/" class="btn btn-default btn-block btn-ad-media">Photos ({{ $ad->photos->count() }})</a>
                            </div>
                            <div class="col-sm-3">
                                <span>
                                    <a href="/" class="btn btn-default btn-block btn-ad-email @if ( ! shouldDisplayContacts($adOwner)) disabled @endif">Email Seller</a>
                                </span>
                            </div>
                            <div class="col-sm-3">
                                <span>
                                    <a href="/" data-ad-id="{{ $ad->ad_id }}" class="{{ ifAdForComparison($ad->ad_id) }} btn btn-default btn-block btn-ad-compare">Compare</a>
                                </span>
                            </div>
                            <div class="col-sm-3">
                                <span>
                                    <a href="/" class="btn btn-default btn-block btn-ad-save" disabled="disabled">Save</a>
                                </span>
                            </div>
                            {{--<div class="col-sm-2">--}}
                                {{--@todo not sure that customer needs separate print page--}}
                                {{--<span>--}}
                                    {{--<a href="{{ printAdLink($ad) }}" target="blank" class="btn btn-default btn-block btn-ad-save">Print</a>--}}
                                {{--</span>--}}
                            {{--</div>--}}
                        </div><!-- end Buttons group -->

                        <div class="dotted-line"></div>

                        <!-- Car info -->
                        <div class="row">
                            <div class="col-sm-5">
                                <p class="column-text"><strong>Stock #:</strong> {{ $ad->ad_id }}</p>
                                @if ($ad->extColor) <p class="column-text"><strong>Exterior Color:</strong> {{ $ad->extColor->name }}</p> @endif
                                @if ($ad->extColor) <p class="column-text"><strong>Interior color:</strong> {{ $ad->intColor->name }}</p> @endif
                                @if ($ad->mileage) <p class="column-text"><strong>Miles:</strong> {{ $ad->mileage }}</p> @endif
                                @if ($ad->transmission) <p class="column-text"><strong>Transmission:</strong> {{ $ad->transmission->name }}</p> @endif
                                @if ($ad->driveType) <p class="column-text"><strong>Drive Type:</strong> {{ $ad->driveType->name }}</p> @endif
                                @if ($ad->fuelType) <p class="column-text"><strong>Fuel Type:</strong> {{ $ad->fuelType->name }}</p> @endif
                                @if ($ad->bodyStyle) <p class="column-text"><strong>Body Style:</strong> {{ $ad->bodyStyle->name }}</p> @endif
                                @if ($ad->doors) <p class="column-text"><strong>Doors:</strong> {{ $ad->doors }}</p> @endif
                            </div>

                            <div class="col-sm-7">
                                @if ($ad->vin)
                                    <p class="column-text"><strong>Vin:</strong> {{ $ad->vin }}</p>
                                    <p class="column-text">&nbsp;</p>
                                @endif
                                <p class="column-text"><strong>{{ fullUserName($adOwner) }}</strong></p>
                                @if (shouldDisplayContacts($adOwner))
                                    @if ($ad->address) <p class="column-text">{{ $ad->address }}</p> @endif
                                    <p class="column-text">
                                        @if ($ad->city) {{ $ad->city }}, @endif
                                        @if($ad->state) {{ $ad->state }} @endif
                                        @if ($ad->zip) {{ $ad->zip }} @endif
                                    </p>
                                    <p class="column-text">
                                        @if ($ad->phone)
                                            <i class="livicon" data-name="phone" data-size="16" data-loop="true" data-c="#01bc8c" data-hc="#01bc8c"></i> {{ $ad->phone }}
                                        @endif
                                    </p>
                                @else
                                    <p class="column-text text-warning">You can't see contact info</p>
                                    <p class="column-text text-warning">because seller account expired</p>
                                @endif
                            </div>
                        </div><!-- end Car info -->

                    </div><!-- end Ad details -->
                </div><!-- end Ad info -->
            </div>

            <!-- Ad description and media -->
            <div id="ad-desc" class="col-md-12">

                <div class="tabbable-panel">
                    <div class="tabbable-line">

                        <!-- Tabs -->
                        <ul class="nav nav-tabs ">
                            <li class="active">
                                <a href="#ad_desc" data-toggle="tab">Description</a>
                            </li>
                            <li>
                                <a href="#ad_media" data-toggle="tab">Photos</a>
                            </li>
                            <li>
                                <a href="#ad_map" data-toggle="tab">Map</a>
                            </li>
                        </ul><!-- end Tabs -->

                        <!-- Tab-content -->
                        <div class="tab-content">

                            <!-- Ad Description -->
                            <div class="tab-pane active" id="ad_desc">
                                <p>{!! $ad->desc !!}</p>
                            </div><!-- end Ad Description -->

                            <!-- Ad Media -->
                            <div class="tab-pane" id="ad_media">
                                <div id="adMedia" class="util-carousel sample-img">
                                    @if ( ! $ad->photos->count() && ! $ad->videos->count())
                                        No photos available
                                    @else
                                        @foreach ($ad->photos as $photo)
                                            <div class="item">
                                                <div class="meida-holder">
                                                    <img src="{{ getThumbnail($photo->uri, 'large') }}" alt="{{ $photo->desc ?: adTitle($ad) }}" />
                                                </div>
                                                <div class="hover-content">
                                                    <div class="overlay"></div>
                                                    <div class="link-container">
                                                        <a href="{{ getThumbnail($photo->uri, 'large') }}" target="_blank"><i class="icon-link"></i></a>
                                                        <a href="{{ getThumbnail($photo->uri, 'large') }}" class="img-link" title="{{ $photo->desc ?: adTitle($ad) }}"><i class="icon-search"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach

                                        @foreach ($ad->videos as $video)
                                            <div class="item">
                                                <div class="meida-holder">
                                                    <img src="{{ getThumbnail($video->image, 'large') }}" alt="{{ adTitle($ad) }}" />
                                                </div>
                                                <div class="hover-content">
                                                    <div class="overlay"></div>
                                                    <div class="link-container">
                                                        <a href="{{ $video->url }}" target="_blank"><i class="icon-link"></i></a>
                                                        <a href="{{ $video->url }}" class="video-link"><i class="icon-play"></i></a>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                            </div><!-- end Ad Media -->

                            <!-- Ad on map -->
                            <div class="tab-pane" id="ad_map">
                                <div id="map" data-address="{{ $ad->address }}, {{ $ad->city }}, {{ $ad->state }}"></div>
                            </div><!-- end Ad on map -->

                        </div><!-- end Tab-content -->
                    </div>
                </div>
            </div><!-- end Ad description and media -->

            <div id="ad-contact" class="col-md-12" @if ( ! shouldDisplayContacts($adOwner)) style="display: none;" @endif>
                <!-- Contact form -->
                <div class="col-md-12 no-padding">
                    <h4 class="email-sent text-success text-center"></h4>

                    {!! Form::open(['class' => 'contact-seller']) !!}
                        {!! Form::hidden('adId', $ad->ad_id) !!}

                        <div class="col-md-5">
                            <p>Hi, I'm interested in your {!! Html::link(adLink($ad), adTitle($ad)) !!} (Stock #: {{ $ad->ad_id }})</p>
                            <div class="form-group text-left">
                                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Name']) !!}
                            </div>
                            <div class="form-group text-left">
                                {!! Form::email('email', null, ['class' => 'form-control', 'placeholder' => 'Email']) !!}
                            </div>
                            <div class="form-group text-left">
                                {!! app('captcha')->display(); !!}
                            </div>
                        </div>

                        <div class="col-md-7">
                            <div class="form-group text-right">
                                {!! Form::textarea('text', null, ['class' => 'form-control', 'placeholder' => 'You message', 'rows' => 10]) !!}
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group text-right">
                                {!! Form::submit('Send', ['class' => 'btn btn-primary']) !!}
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div><!-- end Contact form -->
            </div>
        </div><!-- end Ad -->
    </div>

@endsection

@section('footer')
    {!! Html::script('/assets/public/js/compare-ads.js') !!}
    {!! Html::script('https://maps.googleapis.com/maps/api/js?key=' . env('GOOGLE_MAP_KEY')) !!}
    {!! Html::script('/assets/public/vendor/utilcarousel/jquery.utilcarousel.min.js') !!}
    {!! Html::script('/assets/public/vendor/magnific-popup/jquery.magnific-popup.min.js') !!}

    {!! Html::script('/assets/public/js/pages/ad.js') !!}
    <script>
        $( function() {
            var interval = setInterval( init, 10 );

            function init() {
                if ( grecaptcha ) {
                    Ad.init( {
                        grecaptcha: grecaptcha
                    } );
                    clearInterval( interval );
                }
            }
        } );
    </script>
@endsection