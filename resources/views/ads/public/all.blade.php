@extends('layouts.public.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">

                <!-- Ads status -->
                <div class="col-md-12">
                    <div class="box">
                        <div class="box-icon">
                            <i class="livicon icon" data-name="dashboard" data-size="55" data-loop="true" data-c="#01bc8c" data-hc="#01bc8c"></i>
                        </div>
                        <!-- Ads status content -->
                        <div class="row info">
                            <a href="/ads?status=0" class="col-md-4 text-center filter-status @if (Request::get('status') == '0') filter-status-active @endif">
                                <h4>
                                    <i class="livicon" data-name="checked-on" data-size="20" data-loop="true" data-c="#01bc8c" data-hc="#01bc8c"></i> Approved - {{ $ads->enabledAdsTotal }}
                                </h4>
                            </a>
                            <div class="col-md-4 text-center">
                                <a href="/ads" class="success"><h3 class="success text-center">Ads status</h3></a>
                            </div>
                            <a href="/ads?status=1" class="col-md-4 text-center filter-status @if (Request::get('status') == '1') filter-status-active @endif">
                                <h4>
                                    <i class="livicon" data-name="checked-off" data-size="20" data-loop="true" data-c="#F60" data-hc="#f60"></i> Waiting - {{ $ads->disabledAdsTotal }}
                                </h4>
                            </a>
                        </div><!-- end Ads status content -->
                    </div>
                </div><!-- end Ads status -->

                @if (Session::has('message'))
                    <!-- Messages -->
                    <div class="col-md-12" style="padding-top: 10px">
                        <div class="alert alert-{{ Session::get('message')['type'] }} alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            {!! Session::get('message')['text'] !!}
                        </div>
                    </div><!-- end Messages -->
                @endif

                @foreach ($ads as $ad)
                    <div class="col-md-12 col-sm-12">

                        <!-- Ad top line -->
                        <div class="text-left">
                            @if ($ad->disabled)
                                <h4 class="border-warning">
                                    <i class="livicon" data-name="checked-off" data-size="20" data-loop="true" data-c="#F60" data-hc="#f60"></i> Waiting
                                </h4>
                            @else
                                <h4 class="border-success">
                                    <i class="livicon" data-name="checked-on" data-size="20" data-loop="true" data-c="#01bc8c" data-hc="#01bc8c"></i> Approved
                                </h4>
                            @endif
                        </div><!-- end Ad top line -->

                        <!-- Ad info -->
                        <div class="col-md-12">

                            <!-- Ad photo -->
                            <div class="col-lg-5 col-md-5 col-sm-12">
                                <a href="{{ adLink($ad) }}">
                                    @if ($ad->photos->count())
                                        <img src="{{ getThumbnail($ad->photos[0]->uri, 'large') }}"
                                             class="img-thumbnail center-block"
                                             alt="{{ $ad->photos[0]->desc ?: adTitle($ad) }}" />
                                    @else
                                        <img class="img-thumbnail center-block"
                                             src="{{ asset('assets/public/images/no-photo.png') }}"
                                             alt="{{ adTitle($ad) }}">
                                    @endif
                                </a>
                            </div><!-- end ad photo -->

                            <!-- Ad details -->
                            <div class="col-lg-7 col-md-7 col-sm-12">

                                <div class="row">
                                    <div class="col-md-9">
                                        <h4>
                                            {!! Html::link(adLink($ad), adTitle($ad)) !!}
                                        </h4>
                                    </div>
                                    <div class="col-md-3">
                                        <span class="pull-right priceText"><h4><b>${{ $ad->price }}</b></h4></span>
                                    </div>
                                </div>

                                <div class="dotted-line"></div>

                                <!-- Buttons group -->
                                <div class="row">
                                    <div class="col-sm-3">
                                        <a href="{{ adLink($ad) . '#media' }}" class="btn btn-default btn-block">Photos ({{ $ad->photos->count() }})</a>
                                    </div>
                                    <div class="col-sm-3">
                                        <span>
                                            <a href="{{ adLink($ad) . '#contact' }}" class="btn btn-default btn-block">Email Seller</a>
                                        </span>
                                    </div>
                                    <div class="col-sm-3">
                                        <span>
                                            <a href="/" data-ad-id="{{ $ad->ad_id }}" class="{{ ifAdForComparison($ad->ad_id) }} btn btn-default btn-block btn-ad-compare">Compare</a>
                                        </span>
                                    </div>
                                    <div class="col-sm-3">
                                        <span>
                                            <a href="/" class="btn btn-default btn-block" disabled="disabled">Save</a>
                                        </span>
                                    </div>
                                </div><!-- end Buttons group -->

                                <div class="dotted-line"></div>

                                <!-- Car info -->
                                <div class="row">
                                    <div class="col-sm-5">
                                        <p class="column-text"><strong>Stock #:</strong> {{ $ad->ad_id }}</p>
                                        <p class="column-text"><strong>Exterior Color:</strong> {{ $ad->extColor->name }}</p>
                                        <p class="column-text"><strong>Interior Color:</strong> {{ $ad->intColor->name }}</p>
                                        <p class="column-text"><strong>Miles:</strong> {{ $ad->mileage }}</p>
                                        <p class="column-text"><strong>Transmission:</strong> {{ $ad->transmission->name }}</p>
                                        <p class="column-text"><strong>Drive Type:</strong> {{ $ad->driveType->name }}</p>
                                        <p class="column-text"><strong>Fuel Type:</strong> {{ $ad->fuelType->name }}</p>
                                    </div>

                                    <div class="col-sm-7">
                                        <p class="column-text"><strong>{{ fullUserName($user) }}</strong></p>
                                        @if ($ad->address) <p class="column-text">{{ $ad->address }}</p> @endif
                                        <p class="column-text">
                                            @if ($ad->city) {{ $ad->city }}, @endif
                                            @if($ad->state) {{ $ad->state }} @endif
                                            @if ($ad->zip) {{ $ad->zip }} @endif
                                        </p>
                                        <p class="column-text">
                                            @if ($ad->phone)
                                                <i class="livicon" data-name="phone" data-size="16" data-loop="true" data-c="#01bc8c" data-hc="#01bc8c"></i> {{ $ad->phone }}
                                            @endif
                                        </p>
                                        <p class="column-text">
                                            <br /><a href="/ad/edit/{{ $ad->ad_id }}"><i class="livicon" data-name="pencil" data-size="16" data-loop="true" data-c="#f60" data-hc="#f60"></i> Edit Details</a>
                                        </p>
                                    </div>
                                </div><!-- end Car info -->

                            </div><!-- end Ad details -->
                        </div><!-- end Ad info -->
                    </div>
                @endforeach

                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 centered pagination">
                            <a href="{{ $ads->previousPageUrl() }}" class="btn btn-default @if ( ! $ads->previousPageUrl()) disabled @endif"><</a>
                                Page {{ $ads->currentPage() }} of {{ $ads->lastPage() }}
                            <a href="{{ $ads->nextPageUrl() }}" class="btn btn-default @if ( ! $ads->nextPageUrl()) disabled @endif">></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')
    {!! Html::script('/assets/public/js/compare-ads.js') !!}
@endsection