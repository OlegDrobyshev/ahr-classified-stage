<footer>
    <div class="container footer-text">
        <!-- About Us Section -->
        <div class="col-sm-4">
            <h4>About Us</h4>
            <p>
                Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.
            </p>
        </div><!-- end About Us Section -->

        <!-- Contacts section -->
        <div class="col-sm-4">
            <h4>Contact Us</h4>
            <p>Contact info here</p><br />
            {{--<ul class="list-unstyled">--}}
                {{--<li>35,Lorem Lis Street, Park Ave</li>--}}
                {{--<li>Lis Street, India.</li>--}}
                {{--<li><i class="livicon icon4 icon3" data-name="cellphone" data-size="18" data-loop="true" data-c="#ccc" data-hc="#ccc"></i>Phone:9140 123 4588</li>--}}
                {{--<li><i class="livicon icon4 icon3" data-name="printer" data-size="18" data-loop="true" data-c="#ccc" data-hc="#ccc"></i> Fax:400 423 1456</li>--}}
                {{--<li><i class="livicon icon3" data-name="mail-alt" data-size="20" data-loop="true" data-c="#ccc" data-hc="#ccc"></i> Email:<span class="success">--}}
                                {{--<a href="mailto:">info@americanhotrods.com</a></span>--}}
                {{--</li>--}}
            {{--</ul>--}}
            {{--<div class="news">--}}
                {{--<h4>News letter</h4>--}}
                {{--<p>subscribe to our newsletter and stay up to date with the latest news and deals</p>--}}
                {{--<div class="form-group">--}}
                    {{--<input type="text" class="form-control" placeholder="yourmail@mail.com" aria-describedby="basic-addon2">--}}
                    {{--<a href="#" class="btn btn-primary text-white" role="button">Subscribe</a>--}}
                {{--</div>--}}
            {{--</div>--}}
            <h4>Follow Us</h4>
            <ul class="list-inline">
                <li>
                    <a href="#"> <i class="livicon" data-name="facebook" data-size="18" data-loop="true" data-c="#ccc" data-hc="#ccc"></i>
                    </a>
                </li>
                <li>
                    <a href="#"> <i class="livicon" data-name="twitter" data-size="18" data-loop="true" data-c="#ccc" data-hc="#ccc"></i>
                    </a>
                </li>
                <li>
                    <a href="#"> <i class="livicon" data-name="google-plus" data-size="18" data-loop="true" data-c="#ccc" data-hc="#ccc"></i>
                    </a>
                </li>
                <li>
                    <a href="#"> <i class="livicon" data-name="linkedin" data-size="18" data-loop="true" data-c="#ccc" data-hc="#ccc"></i>
                    </a>
                </li>
            </ul>
        </div><!-- end Contacts section -->

        <!-- Recent Ads -->
        <div class="col-sm-4">
            @if (isset($recentAds))
                <h4>Recent Ads</h4>
                @foreach ($recentAds as $ad)
                    <div class="media">
                        <div class="media-left media-top">
                            <a href="{{ adLink($ad) }}">
                                @if ($ad->photos->count())
                                    <img src="{{ getThumbnail($ad->photos[0]->uri, 'small') }}"
                                         alt="{{ $ad->photos[0]->desc ?: adTitle($ad) }}" />
                                @else
                                    <img src="{{ asset('assets/public/images/no-photo.png') }}"
                                         alt="{{ adTitle($ad) }}">
                                @endif
                            </a>
                        </div>
                        <div class="media-body">
                            <p class="media-heading">{!! Html::link(adLink($ad), adTitle($ad)) !!}</p>
                            <p class="pull-right">
                            </p>
                        </div>
                    </div>
                @endforeach
            @endif
        </div><!-- end recent Ads -->

    </div>
</footer>