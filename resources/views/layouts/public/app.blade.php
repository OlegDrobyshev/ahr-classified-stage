<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>AHR Classified</title>


        <!-- Global styles -->
        {!! Html::style('/assets/public/css/bootstrap.min.css') !!}
        {!! Html::style('/assets/public/vendor/select2/select2.min.css') !!}
        {!! Html::style('/assets/public/vendor/slider/bootstrap-slider.min.css') !!}

        @yield('header')

        {!! Html::style('/assets/public/css/custom.css') !!}
    </head>

    <body>
        <header class="main-header">
            <!-- Icon Section Start -->
            @include('layouts.public.iconsection')

            <!-- Nav bar -->
            @include('layouts.public.navbar')
        </header>

        <!-- Breadcrumb Section Start -->
        {{--@include('layouts.public.breadcrumbs')--}}

        <!-- Container Section Start -->
        @yield('content')
        <!-- //Container Section End -->

        <!-- Footer Section Start -->
        @include('layouts.public.footer')

        <div class="copyright">
            <div class="container">
                <p>Copyright &copy; American Hot Rods, {{ date('Y')  }}</p>
            </div>
        </div>

        <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Return to top" data-toggle="tooltip" data-placement="left">
            <i class="livicon" data-name="plane-up" data-size="18" data-loop="true" data-c="#fff" data-hc="white"></i>
        </a>

        <!-- Global scripts -->
        {!! Html::script('/assets/public/js/jquery.min.js') !!}
        {!! Html::script('/assets/public/js/bootstrap.min.js') !!}
        {!! Html::script('/assets/public/js/raphael.js') !!}
        {!! Html::script('/assets/public/js/livicons-1.4.min.js') !!}
        {!! Html::script('/assets/public/js/jquery.cookie.js') !!}
        {!! Html::script('/assets/public/js/josh_frontend.js') !!}

        {!! Html::script('/assets/public/vendor/select2/select2.min.js') !!}
        {!! Html::script('/assets/public/vendor/slider/bootstrap-slider.min.js') !!}

        <!-- Footer scripts -->
        <script>
            $( function () {
                $( ".select2" ).select2( { placeholder: "Any" } );

                /**
                 * Set total items for comparison
                 */
                ( function() {
                    $.cookie.json = true;

                    var compareList = $.cookie( 'compare' ),
                        compareTotal = compareList ? compareList.length : 0;

                    $( "span.compare-total" ).html( compareTotal );
                } )();
            } );
        </script>

         @yield('footer')
    </body>
</html>