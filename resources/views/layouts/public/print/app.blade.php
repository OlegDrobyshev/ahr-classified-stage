<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>AHR Classified: {{ adTitle($ad) }}</title>

    <!-- Global styles -->
    {!! Html::style('/assets/public/css/bootstrap.min.css') !!}

    {!! Html::style('/assets/public/css/print.css') !!}
</head>
<body>

<header class="main-header">
    <nav class="navbar navbar-default container">
        <div class="navbar-header">
            <a class="navbar-brand" href="/">{!! Html::image('assets/public/images/ahr-logo.jpg', null, ['class' => 'logo_position']) !!}
            </a>
        </div>
    </nav>
</header>

<!-- Container Section Start -->
@yield('content')

</body>
</html>