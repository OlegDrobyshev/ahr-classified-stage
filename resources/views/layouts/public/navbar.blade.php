<nav class="navbar navbar-default container">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#collapse">
                    <span><a href="#"> <i class="livicon" data-name="responsive-menu" data-size="25" data-loop="true" data-c="#757b87" data-hc="#ccc"></i>
                        </a></span>
        </button>
        <a class="navbar-brand" href="/">{!! Html::image('assets/public/images/ahr-logo.jpg', null, ['class' => 'logo_position']) !!}
        </a>
    </div>
    <div class="collapse navbar-collapse" id="collapse">
        <ul class="nav navbar-nav navbar-right">
            <li><a href="/"> Home</a>
            <li><a href="/results">Browse</a>
            <li><a href="/compare">Compare (<span class="compare-total">0</span>)</a>
            </li>

            @if (Auth::guest())
                <li><a href="{{ url('/login') }}">Login</a></li>
                <li><a href="{{ url('/register') }}">Register</a></li>
            @else
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        {{ Auth::user()->first_name }} <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" role="menu">
                        @if (Auth::user()->type == 'admin')
                            <li>{!! Html::link(url('/admin'), 'Administrate') !!}</li>
                        @endif
                        <li><a href="{{ url('/new-ad') }}">New Ad</a></li>
                        <li><a href="{{ url('/ads') }}">My Ads</a></li>
                        <li><a href="{{ url('/logout') }}">Logout</a></li>
                    </ul>
                </li>
            @endif
        </ul>
    </div>
</nav>