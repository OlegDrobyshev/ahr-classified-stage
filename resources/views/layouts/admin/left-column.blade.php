<aside class="left-side sidebar-offcanvas">
    <section class="sidebar ">
        <div class="page-sidebar  sidebar-nav">
            <div class="nav_icons">
                <ul class="sidebar_threeicons">
                    <li>
                        <a href="/admin/ads">
                            <i class="livicon" data-name="list-ul" title="Ads" data-loop="true" data-color="#F89A14" data-hc="#F89A14" data-s="25"></i>
                        </a>
                    </li>
                    <li>
                        <a href="/admin/users">
                            <i class="livicon" data-name="users" title="Users" data-loop="true" data-color="#37bc9b" data-hc="#37bc9b" data-s="25"></i>
                        </a>
                    </li>
                    <li>
                        <a href="/admin/send-emails">
                            <i class="livicon" data-name="mail" title="Send Emails" data-loop="true" data-color="#418BCA" data-hc="#418BCA" data-s="25"></i>
                        </a>
                    </li>
                    <li>
                        <a href="/admin/payments">
                            <i class="livicon" data-name="money" title="Payments" data-loop="true" data-color="#37bc9b" data-hc="#37bc9b" data-s="25"></i>
                        </a>
                    </li>
                </ul>
            </div>
            <div class="clearfix"></div>

            <!-- Sidebar menu -->
            <ul id="menu" class="page-sidebar-menu">
                <li>
                    <a href="/admin/dashboard">
                        <i class="livicon" data-name="home" data-size="18" data-c="#418BCA" data-hc="#418BCA" data-loop="true"></i>
                        <span class="title">Dashboard</span>
                    </a>
                </li>

                <li>
                    <a href="/admin/users">
                        <i class="livicon" data-name="users" data-size="18" data-c="#00bc8c" data-hc="#00bc8c" data-loop="true"></i>
                        <span class="title">Users</span>
                    </a>
                </li>

                <li>
                    <a href="/admin/ads">
                        <i class="livicon" data-name="list-ul" data-size="18" data-c="#F89A14" data-hc="#F89A14" data-loop="true"></i>
                        <span class="title">Ads</span>
                    </a>
                </li>

                <li>
                    <a href="/admin/payments">
                        <i class="livicon" data-name="money" data-size="18" data-c="#00bc8c" data-hc="#00bc8c" data-loop="true"></i>
                        <span class="title">Payments</span>
                    </a>
                </li>

                <li>
                    <a href="/admin/send-emails">
                        <i class="livicon" data-name="mail" data-size="18" data-c="#418BCA" data-hc="#418BCA" data-loop="true"></i>
                        <span class="title">Send emails</span>
                    </a>
                </li>
            </ul><!-- end Sidebar menu -->

        </div>
    </section>
</aside>