<nav class="navbar navbar-static-top" role="navigation">

    <!-- Sidebar toggle button-->
    <div>
        <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
            <div class="responsive_nav"></div>
        </a>
    </div><!-- end Sidebar toggle button-->

    <div class="navbar-right">
        <ul class="nav navbar-nav">

            <!-- New ads -->
            <li class="dropdown notifications-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <i class="livicon" data-name="bell" data-loop="true" data-color="#e9573f" data-hovercolor="#e9573f" data-size="28"></i>
                    @if (count($newAds))
                        <span class="label label-warning">{{ count($newAds) }}</span>
                    @endif
                </a>
                <ul class=" notifications dropdown-menu">
                    <li class="dropdown-title"><strong>{{ count($newAds) }}</strong> new ads were posted</li>
                    <li>
                        <!-- inner menu: contains the actual data -->
                        <ul class="menu">
                            @foreach ($newAds as $ad)
                                <li>
                                    @if ($ad->photos->count())
                                        <img src="{{ getThumbnail($ad->photos[0]->uri, 'small') }}"
                                             class="img-circle center-block"
                                             alt="{{ $ad->photos[0]->desc ?: adTitle($ad) }}" />
                                    @else
                                        <i class="livicon info" data-n="car" data-s="20" data-c="white" data-hc="white"></i>
                                    @endif
                                    {!! Html::link(adLink($ad), adTitle($ad)) !!}
                                    <small class="pull-right">
                                        <span class="livicon paddingright_10" data-n="timer" data-s="10"></span>
                                        {{ $ad->created_at->diffForHumans() }}
                                    </small>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                    <li class="footer">
                        {!! Html::link('/admin/ads', 'View all') !!}
                    </li>
                </ul>
            </li><!-- end new Ads -->

            <!-- User dropdown menu -->
            <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    {!! Html::image('assets/admin/img/admin-default-avatar.png', 'Admin', ['class' => 'img-circle img-responsive pull-left', 'width' => '35', 'height' => '35']) !!}
                    <div class="riot">
                        <div>
                            Admin<span><i class="caret"></i></span>
                        </div>
                    </div>
                </a>

                <!-- Dropdown menu -->
                <ul class="dropdown-menu">
                    <!-- User image -->
                    <li class="user-header bg-light-blue">
                        {!! Html::image('assets/admin/img/admin-default-avatar.png', 'Admin', ['class' => 'img-circle img-responsive pull-left', 'width' => '90', 'height' => '90']) !!}
                        <p class="topprofiletext">Admin</p>
                    </li><!-- end User image -->

                    <!-- Menu Footer-->
                    <li class="user-footer text-center">
                        <a href="/logout">
                            <i class="livicon" data-name="sign-out" data-s="18"></i>
                            Logout
                        </a>
                    </li><!-- end Menu Footer-->
                </ul><!-- end Dropdown menu -->
            </li><!-- end User dropdown menu -->

        </ul>
    </div>
</nav>