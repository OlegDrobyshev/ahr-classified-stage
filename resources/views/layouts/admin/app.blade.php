<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>AHR Classified: admin</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <!-- Global styles -->
    {!! Html::style('/assets/admin/css/bootstrap.min.css') !!}
    {!! Html::style('/assets/admin/vendors/font-awesome/css/font-awesome.min.css') !!}
    {!! Html::style('/assets/admin/css/styles/black.css') !!}
    {!! Html::style('/assets/admin/css/panel.css') !!}
    {!! Html::style('/assets/admin/css/metisMenu.css') !!}
    {!! Html::style('/assets/admin/css/pages/toastr.css') !!}
    {!! Html::style('/assets/admin/vendors/toastr/toastr.css') !!}
    {!! Html::style('/assets/public/vendor/select2/select2.min.css') !!}

    <!-- Page level styles -->
    @yield('header')

    {!! Html::style('/assets/admin/css/custom.css') !!}
</head>

<body class="skin-josh">
    <header class="header">
        <a href="/admin/dashboard" class="logo">
            {!! Html::image('assets/admin/img/admin-ahr-logo.png', null, ['class' => 'full-width']) !!}
        </a>
        <!-- Navbar -->
        @include('layouts.admin.navbar')
    </header>

    <div class="wrapper row-offcanvas row-offcanvas-left">

        <!-- Left side column. contains the logo and sidebar -->
        @include('layouts.admin.left-column')

        <!-- Right side column -->
        @yield('content')

    </div>

    <a id="back-to-top" href="#" class="btn btn-primary btn-lg back-to-top" role="button" title="Return to top" data-toggle="tooltip" data-placement="left">
        <i class="livicon" data-name="plane-up" data-size="18" data-loop="true" data-c="#fff" data-hc="white"></i>
    </a>

    <!-- Global scripts -->
    {!! Html::script('/assets/admin/js/jquery-1.11.3.min.js') !!}
    {!! Html::script('/assets/admin/js/bootstrap.min.js') !!}
    {!! Html::script('/assets/admin/vendors/livicons/minified/raphael-min.js') !!}
    {!! Html::script('/assets/admin/vendors/livicons/minified/livicons-1.4.min.js') !!}
    {!! Html::script('/assets/admin/js/josh.js') !!}
    {!! Html::script('/assets/admin/js/metisMenu.js') !!}
    {!! Html::script('/assets/admin/vendors/daterangepicker/moment.min.js') !!}
    {!! Html::script('/assets/admin/vendors/holder/holder.js') !!}
    {!! Html::script('/assets/admin/vendors/toastr/toastr.min.js') !!}
    {!! Html::script('/assets/public/vendor/select2/select2.min.js') !!}

    <script>
        $( function() {
            /**
             * Ajax setup
             */
            $.ajaxSetup({
                headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' }
            });

            ( function() {
                var setActiveMenuItem = function( uri ) {
                    $( "ul#menu a[href='" + uri + "']" ).addClass( "active" );
                };
                setActiveMenuItem("/{{ Request::path() }}");

                /**
                 * Initialize select2 elements
                 */
                var select2Elems = $( ".select2" );

                $.each( select2Elems, function( k, elem ) {
                    var placeHolder = $( elem ).data( "placeholder" );
                    if ( ! placeHolder ) placeHolder = "Any";
                    $( elem ).select2( { placeholder: placeHolder } );
                } );
            } )();
        } );
    </script>

    <!-- Page level scripts -->
    @yield('footer')

</body>
</html>
