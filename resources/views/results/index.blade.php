@extends('layouts.public.app')

@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-2">
                <div class="box">
                    <div class="box-icon box-icon2">
                        <i class="livicon icon1" data-name="settings" data-size="55" data-loop="true" data-c="#f60" data-hc="#f60"></i>
                    </div>
                    <div class="info">
                        {!! Form::open(['id' => 'ads-search-form']) !!}
                            {!! csrf_field() !!}

                            <div class="col-md-12 no-padding filter-title">
                                <div class="col-md-4 col-xs-4 text-left no-padding"><strong>FILTER</strong></div>
                                <div class="col-md-8 col-xs-8 text-right no-padding">
                                    <a href="#" class="save-search">[save search]</a>
                                </div>
                                <div class="col-md-12 no-padding new-filter" style="display: none;">
                                    {!! Form::text('new-filter', null, ['class' => 'form-control']) !!}
                                </div>
                            </div>

                            <div class="col-md-12 no-padding">
                                <div class="active-filters">
                                    {{--<div class="active-filter">--}}
                                        {{--<span>Aston Martin</span>--}}
                                        {{--<a href="#" data-name="filter_name" data-value="22">x</a>--}}
                                    {{--</div>--}}
                                </div>
                                {!! Html::link('/', 'CLEAR FILTERS', ['class' => 'btn btn-primary full-width clear-filters', 'style' => 'display:none;']) !!}
                            </div>

                            <!-- Select Type Filters -->
                            <div class="select-filters">
                                @foreach ($filters['list']['select'] as $filter => $values)
                                    <div class="form-group">
                                        {!! Form::label($filter) !!}
                                        {!! Form::select($filter, $values, 0, ['class' => 'form-control select2']) !!}
                                    </div>
                                @endforeach
                            </div><!-- end Select Type Filters -->

                            <hr />

                            <!-- Sliders type filters -->
                            <div class="range-filters">
                                @foreach ($filters['list']['range'] as $title => $range)
                                    <div class="form-group">
                                        {!! Form::label($title) !!}
                                        {!! Form::text($title, '', [
                                            'class' => 'slider-range form-control full-width',
                                            'data-slider-handle' => 'round',
                                            'data-slider-min' => $range['min'],
                                            'data-slider-max' => $range['max'],
                                            'data-slider-step' => $range['step'],
                                            'data-slider-value' => '[' . $range['min'] . ',' . $range['max'] . ']'
                                        ]) !!}
                                    </div>
                                @endforeach
                            </div><!-- end Sliders type filters -->

                            <!-- Test type filters -->
                            <div class="text-filters">
                                @foreach ($filters['list']['text'] as $title => $placholder)
                                    <div class="form-group">
                                        {!! Form::label($title) !!}
                                        {!! Form::text($title, '', [
                                            'class' => 'form-control full-width',
                                        ]) !!}
                                    </div>
                                @endforeach
                            </div><!-- end Test type filters -->

                            {!! Html::link('/', 'SEARCH', ['class' => 'btn btn-primary full-width do-search']) !!}

                        {!! Form::close() !!}<!-- end List Filters -->
                    </div>
                </div>
            </div>

            <div class="col-md-10" id="ads-stage-wrap">
                <div class="box">
                    <div class="box-icon box-icon2">
                        <i class="livicon icon1" data-name="car" data-size="55" data-loop="true" data-c="#f60" data-hc="#f60"></i>
                    </div>
                    <div class="info" style="overflow: hidden;">

                        <!-- Ads stage -->
                        <div id="ads-stage">

                            <div class="col-md-12">
                                <div class="col-md-3 text-center saved-searches">
                                    <div class="form-group">
                                        {!! Form::select('', ['Your Saved Searches'], null, ['class' => 'full-width']) !!}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <h3 class="success text-center">We found <span class="total-found">{!! $ads->total() !!}</span> Vehicles</h3>
                                </div>
                                <div class="col-md-3 text-center saved-ads">
                                    <div class="form-group">
                                        {!! Form::select('', ['Your Saved Cars'], null, ['class' => 'select2 full-width']) !!}
                                    </div>
                                </div>
                            </div>

                            <!-- Loader -->
                            <div class="search-loader-wrap">
                                <div class="loader">{!! Html::image('assets/public/images/ajax-loader.gif') !!}</div>
                            </div><!-- end Loader -->

                            <!-- Ads -->
                            <div id="ads-list">
                                @foreach ($ads as $ad)
                                    <div class="col-md-12 col-sm-12">

                                        <!-- Ad top line -->
                                        <div class="text-left">
                                            <h4 class="border-success"></h4>
                                        </div><!-- end Ad top line -->

                                        <!-- Ad info -->
                                        <div class="col-md-12">

                                            <!-- Ad photo -->
                                            <div class="col-lg-5 col-md-5 col-sm-12">
                                                <a href="{{ adLink($ad) }}" target="_blank">
                                                    @if ($ad->photos->count())
                                                        <img src="{{ getThumbnail($ad->photos[0]->uri, 'large') }}"
                                                             class="img-thumbnail center-block"
                                                             alt="{{ $ad->photos[0]->desc ?: adTitle($ad) }}" />
                                                    @else
                                                        <img class="img-thumbnail center-block"
                                                             src="{{ asset('assets/public/images/no-photo.png') }}"
                                                             alt="{{ adTitle($ad) }}">
                                                    @endif
                                                </a>
                                            </div><!-- end ad photo -->

                                            <!-- Ad details -->
                                            <div class="col-lg-7 col-md-7 col-sm-12">

                                                <div class="row">
                                                    <div class="col-md-9 text-left">
                                                        <h4>
                                                            {!! Html::link(adLink($ad), adTitle($ad), ['target' => '_blank']) !!}
                                                        </h4>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <span class="pull-right priceText"><h4><b>${{ $ad->price }}</b></h4></span>
                                                    </div>
                                                </div>

                                                <div class="dotted-line"></div>

                                                <!-- Buttons group -->
                                                <div class="row ad-buttons">
                                                    <div class="col-sm-3">
                                                        <a href="{{ adLink($ad) . '#media' }}" class="btn btn-default btn-block btn-ad-photos" target="_blank">Photos ({{ $ad->photos->count() }})</a>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <span>
                                                            <a href="{{ adLink($ad) . '#contact' }}"
                                                               class="btn btn-default btn-block btn-ad-email"
                                                               target="_blank"
                                                               @if ( ! shouldDisplayContacts($ad->owner))
                                                                   disabled="disabled"
                                                               @endif>Email Seller</a>
                                                        </span>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <span>
                                                            <a href="/" data-ad-id="{{ $ad->ad_id }}" class="btn btn-default btn-block btn-ad-compare">Compare</a>
                                                        </span>
                                                    </div>
                                                    <div class="col-sm-3">
                                                        <span>
                                                            <a href="/" class="btn btn-default btn-block btn-ad-save" disabled="disabled">Save</a>
                                                        </span>
                                                    </div>
                                                </div><!-- end Buttons group -->

                                                <div class="dotted-line"></div>

                                                <!-- Car info -->
                                                <div class="row">
                                                    <div class="col-sm-5">
                                                        <p class="column-text"><strong>Stock #:</strong> {{ $ad->ad_id }}</p>
                                                        @if ($ad->extColor) <p class="column-text"><strong>Exterior Color:</strong> {{ $ad->extColor->name }}</p> @endif
                                                        @if ($ad->intColor) <p class="column-text"><strong>Interior Color:</strong> {{ $ad->intColor->name }}</p> @endif
                                                        @if ($ad->mileage) <p class="column-text"><strong>Miles:</strong> {{ $ad->mileage }}</p> @endif
                                                        @if ($ad->transmission) <p class="column-text"><strong>Transmission:</strong> {{ $ad->transmission->name }}</p> @endif
                                                        @if ($ad->driveType) <p class="column-text"><strong>Drive Type:</strong> {{ $ad->driveType->name }}</p> @endif
                                                        @if ($ad->fuelType) <p class="column-text"><strong>Fuel Type:</strong> {{ $ad->fuelType->name }}</p> @endif
                                                    </div>

                                                    <div class="col-sm-7">
                                                        <p class="column-text"><strong>{{ fullUserName($ad->owner) }}</strong></p>
                                                        @if (shouldDisplayContacts($ad->owner))
                                                            @if ($ad->address) <p class="column-text">{{ $ad->address }}</p> @endif
                                                            <p class="column-text">
                                                                @if ($ad->city) {{ $ad->city }}, @endif
                                                                @if($ad->state) {{ $ad->state }} @endif
                                                                @if ($ad->zip) {{ $ad->zip }} @endif
                                                            </p>
                                                            <p class="column-text">
                                                                @if ($ad->phone)
                                                                    <i class="livicon" data-name="phone" data-size="16" data-loop="true" data-c="#01bc8c" data-hc="#01bc8c"></i> {{ $ad->phone }}
                                                                @endif
                                                            </p>
                                                        @endif
                                                        <p class="column-text">
                                                            <br /><a href="{{ adLink($ad) }}" target="_blank"><i class="livicon" data-name="link" data-size="16" data-loop="true" data-c="#f60" data-hc="#f60"></i> View Details</a>
                                                        </p>
                                                    </div>
                                                </div><!-- end Car info -->

                                            </div><!-- end Ad details -->
                                        </div><!-- end Ad info -->
                                    </div>
                                @endforeach
                            </div><!-- end Ads -->

                            <!-- Pagination -->
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12 centered pagination">
                                        <hr/>
                                        <a href="{{ $ads->previousPageUrl() }}" class="btn btn-default prev-page @if ( ! $ads->previousPageUrl()) disabled @endif"><</a>
                                        Page <span class="current-page">{{ $ads->currentPage() }}</span>
                                        of <span class="total-pages">{{ $ads->lastPage() }}</span>
                                        <a href="{{ $ads->nextPageUrl() }}" class="btn btn-default next-page @if ( ! $ads->nextPageUrl()) disabled @endif">></a>
                                    </div>
                                </div>
                            </div><!-- end Pagination -->

                        </div><!-- end Ads stage -->

                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

@section('footer')
    {!! Html::script('/assets/public/js/compare-ads.js') !!}
    {!! Html::script('/assets/public/js/pages/results.js') !!}
    <script>
        $( function() {
            Results.init();
        } );
    </script>
@endsection