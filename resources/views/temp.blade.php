<html>
<head>
    {!! Html::script('/assets/admin/js/jquery-1.11.3.min.js') !!}
</head>
<body>
    <p>Total {{ $totalAds }}</p>
    Total Ads: <span class="affected-ads">0</span><br />
    Affected photos: <span class="affected-photos">0</span>
    <br />
    <p>Time: <b class="timer">0</b><b> sec</b></p>
    <div></div>

    <script>
        $( function() {
            var aa = $( ".affected-ads" ),
                ap = $( '.affected-photos' ),
                timer = $( '.timer' ),
                tData = {
                    offset: 0,
                    limit: 2
                },
                total = 836;

            $.ajaxSetup({
                headers: { 'X-CSRF-TOKEN': '{{ csrf_token() }}' }
            });

            var t = setInterval( function() {
                timer.html( parseInt(timer.html()) + 1 );
            }, 1000);

            function dos() {
                $.post( "/create-thumbs", tData, function (data) {
                    data = $.parseJSON( data );


                    aa.html( parseInt(aa.html()) + data.ads );
                    ap.html( parseInt(ap.html()) + data.photos );

                    tData.offset = tData.offset + tData.limit;

                    $( 'div' ).append( data.line + '<br />' );

                    if ( tData.offset <= total) {
                        dos();
                    } else {
                        clearInterval(t);
                    }
                } );
            }

            dos();
        } );
    </script>
</body>
</html>