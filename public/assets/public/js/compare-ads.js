var CompareAds = function() {
    return {
        init : function () {
            var compareCookieName = "compare",
                compareTotalSpan = $( "span.compare-total" );

            // Set jQuery cookie plugging defaults
            $.cookie.json = true;

            /**
             * Clear compare list
             */
            $( "a.compare-clear" ).on( "click", function( e ) {
                e.preventDefault();
                setCookie( compareCookieName, [] );
                window.location.reload();
            } );

            /**
             * Remove item from compare table
             */
            $( "a.compare-remove" ).on( "click", function( e ) {
                e.preventDefault();

                var compareList = getCookie( compareCookieName ),
                    adId = $( this ).data( "ad-id" );

                compareList.splice( compareList.indexOf( adId ), 1 );
                setCookie( compareCookieName, compareList );
                window.location.reload();
            } );

            /**
             * On "Compare" button click
             */
            $( "body" ).on( "click", "a.btn-ad-compare", function( e ) {
                e.preventDefault();

                var compareList = getCookie( compareCookieName ) ? getCookie( compareCookieName ) : [],
                    adId = $( this ).data( "ad-id" ),
                    adIdIndex = compareList.indexOf( adId );

                if ( adIdIndex >= 0 ) {
                    compareList.splice( adIdIndex, 1 );
                } else {
                    if ( compareList.length == 7 ) {
                        compareList = compareList.splice( 0, 6 );
                    }
                    compareList.push( adId );
                }

                $( this ).blur();

                flushCookie( compareCookieName );
                setCookie( compareCookieName, compareList );
                setPressedCompareButtons();
                updateTotalCompare();
            } );

            /**
             * Set compare buttons as "pressed" for compare items
             */
            function setPressedCompareButtons() {
                var compareButtons = $( "body" ).find( "a.btn-ad-compare" ),
                    compareList = getCookie( compareCookieName ),
                    $button = null,
                    adId = null;

                $.each( compareButtons, function( k, button ) {
                    $button = $( button );
                    adId = $button.data( "ad-id" );
                    if ( compareList.indexOf( adId ) >= 0 ) {
                        $button.addClass( "active" );
                    } else {
                        $button.removeClass( "active" );
                    }
                } );
            }

            /**
             * Update total compare items count
             */
            function updateTotalCompare() {
                var compareList = $.cookie( 'compare' ),
                    compareTotal = compareList ? compareList.length : 0;

                compareTotalSpan.html( compareTotal );
            }

            /**
             * Save value ot cookie
             *
             * @param key
             * @param value
             */
            function setCookie( key, value ) {
                $.cookie( key, value, { expires: 365, path: "/" } );
            }

            /**
             * Get from cookie
             *
             * @param key
             * @returns {*}
             */
            function getCookie( key ) {
                if ( key ) {
                    return $.cookie( key );
                }

                return $.cookie();
            }

            /**
             * Flush cookie
             */
            function flushCookie( key ) {
                $.removeCookie( key );
            }
        }
    }
}();

$( function() { CompareAds.init(); } );