var adCreate = function() {

    return {
        init : function ( options ) {

            var uploadPhotoUri = "/upload-ad-photo-ajax",
                updatePhotoUri = "/update-ad-photo-ajax",
                deletePhotoUri = "/delete-ad-photo-ajax",
                addVideoUri = "/add-ad-video-ajax",
                deleteVideoUri = "/delete-ad-video-ajax",
                getModelsByMakeIdUri = "/get-models-by-make-id-ajax",
                form = $( "form#create-ad-form" ),
                adMakeSelect = form.find( "select[name=make_id]" ),
                adModelSelect = form.find( "select[name=model_id]" ),
                addPhotoBtn = form.find( "a.add-photo-btn" ),
                addPhotoInput = form.find( "input.add-photo-input" ),
                addVideoBtn = form.find( "a.add-video-btn" ),
                addVideoBlock = form.find( "div.add-video-block" ),
                addVideoInput = addVideoBlock.find( "input" ),
                messageBlock = form.find( "div.ad-media-message" ),
                progress = form.find( "div.progress" ),
                adMediaWrap = form.find( "div.ad-media" ),
                progressTimer = null;

            /**
             * Filter model on make change event
             */
            adMakeSelect.on( "change", function() {
                var data = {
                    makeId: $( this ).val()
                };

                $.post( getModelsByMakeIdUri, data, function( data ) {
                    if ( ! $.isEmptyObject( data ) ) {
                        var html = '',
                            selected = null,
                            emptyList = true;

                        $.each( data, function( key, value ) {
                            if ( key > 0 ) {
                                emptyList = false;
                            }
                            selected = ( ! key ) ? "selected" : "";
                            html += "<option value='" + key + "' " + selected + ">" + value + "</option>";
                        } );

                        adModelSelect.html( html ).attr( "disabled", false );
                    } else {
                        adModelSelect.attr( "disabled", true );
                    }
                } );
            } );

            /**
             * Open media item for Edit
             */
            form.on( "click", "div.ad-media-item img, div.ad-media-item a.ad-photo-update", function( e ) {
                e.preventDefault();
                var mediaItem = $( this ).parents( "div.ad-media-item" );
                mediaItem
                    .siblings( "div.ad-media-item:not(.ad-media-closed)" )
                    .addClass( "ad-media-closed" );
                mediaItem
                    .toggleClass( "ad-media-closed" );
                scrollTo( mediaItem, 60 );
            } );

            /**
             * Focus on photo desc input when open media item
             */
            form.on( "click", "div.ad-media-item img", function( e ) {
                $( this ).siblings( "div" ).find( "input[type=text]" ).focus();
            } );

            /**
             * Add video button click
             */
            addVideoBtn.on( "click", function( e ) {
                e.preventDefault();
                hideMessage();
                showAddVideoBlock();
                scrollTo( addVideoBtn, 60 );
            } );

            /**
             * Close add video button click
             */
            addVideoBlock.find( "a.ad-add-video-close" ).on( "click", function( e ) {
                e.preventDefault();
                hideAddVideoBlock();
                addVideoInput.val( "" );
            } );

            /**
             * Add video to ad
             */
            addVideoBlock.find( "a.ad-add-video" ).on( "click", function( e ) {
                e.preventDefault();

                var videoURL = addVideoInput.val(),
                    videoObj = urlParser.parse(videoURL);

                if ( ! videoObj ) {
                    addVideoInput.addClass( "error-input" );
                } else {
                    addVideo( videoObj );
                }
            } );

            /**
             * Add video to ad ajax
             * @param video
             */
            function addVideo( video ) {
                var value = 0;
                setProgress( "0");
                hideAddVideoBlock();

                progressTimer = setInterval( function() {
                    setProgress( value + 10 + "%" );
                    if ( value == 100 ) {
                        clearInterval( progressTimer );
                    }
                }, 10 );

                $.post( addVideoUri, video, function( data ) {
                    showMessage( "Video added" );
                    addVideoItem( data );
                } );
            }

            /**
             * Add video item to media section
             */
            function addVideoItem( video ) {
                var html = "", item = null;
                html =
                    '<div class="ad-media-item ad-media-closed" data-type="video"> ' +
                        '<img src="' + video.thumbnail + '" alt="slider-image" class="img-responsive img-rounded" /> ' +
                        '<div class="ad-media-item-form">' +
                            '<input type="hidden" name="ad_video_id[]" value="' + video.video_id + '" />' +
                            '<div class="btn-group-vertical" role="group">' +
                                '<a href="#" class="btn btn-default ad-video-delete">Delete</a>' +
                            '</div> ' +
                        '</div> ' +
                    '</div>';
                item = $( html ).hide();
                adMediaWrap.append( html );
                item.fadeIn( 300 );
            }

            /**
             * Delete video
             */
            form.on( "click", "a.ad-video-delete", function( e ) {
                e.preventDefault();
                var $this = $( this ),
                    adVideoId = $this.parents( "div.ad-media-item-form" ).find( "input[type=hidden]" ).val();

                $.post( deleteVideoUri, { adVideoId: adVideoId }, function() {
                    showMessage( "Video deleted" );
                } );

                $this.parents( "div.ad-media-item" ).remove();
            } );

            /**
             * Add photo button click
             */
            addPhotoBtn.on( "click", function( e ) {
                e.preventDefault();
                addPhotoInput.click();
            } );

            /**
             * Add photo on change input event
             */
            addPhotoInput.on( "change", function() {
                var formData = new FormData();

                $.each( $( this )[0].files, function( key, value ) {
                    formData.append( "photos[]", value );
                } );

                hideMessage();
                hideAddVideoBlock();
                uploadPhotos( formData );
                $( this ).val( "" );
            } );

            /**
             * Update photo ajax
             */
            form.on( "click", "a.ad-photo-update", function( e ) {
                e.preventDefault();
                var mediaItemForm = $( this ).parents( "div.ad-media-item-form" ),
                    data = {
                        adPhotoId: mediaItemForm.find( "input[type=hidden]" ).val(),
                        desc: mediaItemForm.find( "input[type=text]" ).val()
                    };

                $.post( updatePhotoUri, data )
                    .done( function() {
                        showMessage( "Photo updated" );
                    } )
                    .fail( function() {
                        showMessage( "Oops, something went wrong . . ." );
                    } );
            } );

            /**
             * Update photo on Enter press
             */
            form.on( "keydown", "div.ad-media-item input[name=image_alt]", function( e ) {
                if ( e.keyCode == 13 ) {
                    $( this ).parents( "div.ad-media-item-form" ).find( "a.ad-photo-update" ).click();
                    return false;
                }
            } );

            /**
             * Delete photo ajax
             */
            form.on( "click", "a.ad-photo-delete", function( e ) {
                e.preventDefault();
                var $this = $( this ),
                    adPhotoId = $this.parents( "div.ad-media-item-form" ).find( "input[type=hidden]" ).val();

                $.post( deletePhotoUri, { adPhotoId: adPhotoId }, function() {
                    showMessage( "Photo deleted" );
                } );

                $this.parents( "div.ad-media-item" ).remove();
            } );

            /**
             * Upload photo ajax
             */
            function uploadPhotos( files ) {
                $.ajax( {
                    type: "POST",
                    url: uploadPhotoUri,
                    data: files,
                    cache: false,
                    contentType: false,
                    processData: false,
                    success: function( data ) {
                        hideProgressBar();
                        showMessage( data.message );
                        addPhotoItems( data.uploaded );
                    },
                    xhr: function() {
                        return xhrUploadProgress( files );
                    }
                } );
            }

            /**
             * Add photo item to media section
             */
            function addPhotoItems( images ) {
                var html = "", item = null;

                $.each( images, function( key, image ) {
                    html =
                        '<div class="ad-media-item ad-media-closed" data-type="photo"> ' +
                            '<img src="' + image.thumbnail + '" alt="slider-image" class="img-responsive img-rounded" /> ' +
                            '<div class="ad-media-item-form">' +
                                '<input type="hidden" name="ad_photo_id[]" value="' + image.photo_id + '" />' +
                                '<input type="text" name="image_alt" class="form-control" placeholder="Photo description" />' +
                                '<div class="btn-group-vertical" role="group">' +
                                    '<a href="#" class="btn btn-default ad-photo-update">Save</a>' +
                                    '<a href="#" class="btn btn-default ad-photo-delete">Delete</a>' +
                                '</div> ' +
                            '</div> ' +
                        '</div>';

                    item = $( html ).hide();
                    adMediaWrap.append( html );
                    item.fadeIn( 300 );
                } );
            }

            /**
             * Ajax setup
             */
            $.ajaxSetup( {
                headers: { 'X-CSRF-TOKEN': form.find( "input[name=_token]" ).val() }
            } );

            /**
             * Ajax start
             */
            $( document ).ajaxStart( function() {
                disableButtons();
                showProgressBar();
            } );

            /**
             * Ajax stop
             */
            $( document ).ajaxStop( function() {
                clearInterval( progressTimer );
                setProgress( "100%" );
                setTimeout( function() {
                    hideProgressBar();
                }, 500);
                enableButtons();
            } );

            /**
             * Track file upload progress via xhr object
             */
            function xhrUploadProgress ( files ) {
                var xhr = $.ajaxSettings.xhr(),
                    percent = "0%";

                if ( ! files ) {
                    hideProgressBar(); return xhr;
                }

                setProgress( percent );
                showProgressBar();

                // Add progress event handler
                xhr.upload.addEventListener( "progress", function( e ) {
                    percent = Math.round( e.loaded / e.total * 100 ) + "%";
                    setProgress( percent );
                } );

                return xhr;
            }

            /**
             * Set progress bar percentage
             * @param percent
             */
            function setProgress( percent ) {
                progress.find( "div" ).html( percent ).css( "width", percent );
            }

            /**
             * Show progress bar
             */
            function showProgressBar() {
                progress.show();
            }

            /**
             * Hide progress bar
             */
            function hideProgressBar() {
                progress.hide();
            }

            /**
             * Show add video block
             */
            function showAddVideoBlock() {
                addVideoBlock.show();
                addVideoBlock.find( "input" ).focus();
            }

            /**
             * Hide add video block
             */
            function hideAddVideoBlock() {
                addVideoBlock.find( "input" ).val( "" );
                addVideoBlock.hide();
            }

            /**
             * Show message
             */
            function showMessage( text ) {
                messageBlock.html( text ).fadeIn( 300 );
            }

            /**
             * Hide message
             */
            function hideMessage() {
                messageBlock.hide();
            }

            /**
             * Disabled submit buttons
             */
            function disableButtons() {
                form.find( "input[type=submit]" ).attr( "disabled", "disabled" );
            }

            /**
             * Enable submit buttons
             */
            function enableButtons() {
                form.find( "input[type=submit]" ).removeAttr( "disabled" );
            }

            /**
             * Scroll to element by ID
             */
            function scrollTo( element, offsetTop ) {
                element = (typeof element == "object") ? element : $( element );
                offsetTop = offsetTop ? offsetTop : 0;

                $( "html, body" ).animate( {
                    scrollTop: element.offset().top - offsetTop
                }, 800);
            }

            /**
             * Initialize TinyMCE
              */
            tinymce.init( {
                selector: "#ad_desc",
                plugins: [
                    "advlist autolink lists link charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime table contextmenu paste"
                ],
                toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link"
            } );

            // Initialize google map auto-complete
            // for address input
            adCreateAddress.init();
        }
    }
}();

/**
 * Address auto-complete input
 */
var adCreateAddress = function() {
    return {
        init : function () {
            var adressInput = $( "input#address" ),
                cityInput = $( "input#city" ),
                stateInput = $( "input#state" ),
                zipInput = $( "input#zip" ),
                placesAutocomplete = places( {
                    container: adressInput[0]
                } );

            /**
             * On address change event
             */
            placesAutocomplete.on( "change", function resultSelected( e ) {
                placesAutocomplete.setVal( e.suggestion.name || "" );
                cityInput.val( e.suggestion.city || "" );
                stateInput.val( e.suggestion.administrative || "" );
                zipInput.val( e.suggestion.postcode || "" );
            });

            /**
             * Clear address fileds
             */
            placesAutocomplete.on( "clear", function() {
                cityInput.val( "" );
                stateInput.val( "" );
                zipInput.val( "" );
            } );
        }
    }
}();