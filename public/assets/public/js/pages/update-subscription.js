var UpdateSubscription = function() {

    return {
        init : function ( options ) {

            var form = $( "form#update-subscription-form" ),
                card = form.find( "#card" ),
                cardBlock = form.find( "div.card-block" );

            /**
             * Credit Card init
             */
            card.card( {
                container: $( ".card-wrapper" )
            } );

            /**
             * Block submit button on form submit
             */
            form.on( "submit", function() {
                $( this ).find( "input[type=submit]" ).attr( "disabled", true );
            } );
        }
    };
}();