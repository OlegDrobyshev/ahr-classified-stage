var Results = function() {

    return {
        init: function( options ) {

            // Set jQuery cookie plugging defaults
            $.cookie.json = true;

            setPressedCompareButtons();

            // Init bootstrap-slider sliders
            $( "input.slider-range" ).slider();

            var searchForm = $( "#ads-search-form" ),
                adsStage = $( "div#ads-stage" ),
                adsList = adsStage.find( "div#ads-list" ),
                pagination = adsStage.find( "div.pagination" ),

                // Filters
                selectTypeFilters = searchForm.find( "div.select-filters select" ),
                rangeTypeFilters = searchForm.find( "div.range-filters input" ),
                textTypeFilters = searchForm.find( "div.text-filters input" ),
                activeFiltersWrap = searchForm.find( "div.active-filters" ),

                // Active elements
                doSearchElem = searchForm.find( "a.do-search" ),
                clearFiltersElem = searchForm.find( "a.clear-filters" ),
                prevPage = pagination.find( "a.prev-page" ),
                nextPage = pagination.find( "a.next-page" ),
                loader = adsStage.find( "div.search-loader-wrap" ),
                saveSearch = searchForm.find( "a.save-search" ),
                newFilterInput = searchForm.find( "div.new-filter input" ),
                savedSearchesDiv = adsStage.find( "div.saved-searches" ),
                savedSearchesElem = savedSearchesDiv.find( "select" ),

                // Info elements
                totalFound = adsStage.find( "span.total-found" ),
                currentPage = pagination.find( "span.current-page" ),
                totalPages = pagination.find( "span.total-pages" );

            var searchEnabled = true;

            /**
             * Save search click
             */
            saveSearch.on( "click", function( e ) {
                e.preventDefault();
                newFilterInput.parent().show().end().focus();
            } );

            /**
             * On enter key press in save search input
             */
            newFilterInput.on( "keydown", function( e ) {
                if ( e.keyCode != 13 ) return true;

                var totalAllowed = 3,
                    searches = getSavedSearchesFromCookie( totalAllowed ),
                    newSearch = {
                        name: $( this ).val().trim(),
                        value: getFilters()
                    };

                clearNewSearchInput();
                searches = searches.concat( newSearch );
                saveSearchesInCookie( searches );
                updateSavedSearchesList( searches );
            } );

            /**
             * Get saved searches
             * @returns {*}
             */
            function getSavedSearchesFromCookie( totalAllowed ) {
                var savedSearches = $.cookie( "saved-searches" ),
                    searches = savedSearches ? savedSearches : [];
                if ( searches != null ) {
                    if ( searches.length >= 3 ) {
                        searches.splice( 0, totalAllowed - 2 );
                    }
                }
                return searches;
            }

            /**
             * Clear new search input
             */
            function clearNewSearchInput() {
                newFilterInput.val( "" ).parent().hide();
            }

            /**
             * Save searches in cookie
             * @param searches
             */
            function saveSearchesInCookie( searches ) {
                $.cookie( "saved-searches", searches, { expires: 365 } );
            }

            /**
             * Save user search in cookie
             * @param searches
             */
            function updateSavedSearchesList( searches ) {
                var options = [ { id: 'none', text: "Saved Searches" } ];

                $.each( searches, function( key, search ) {
                    options.push( {
                        id: key,
                        text: search.name
                    } );
                } );

                savedSearchesElem.empty().select2( { data: options });
                savedSearchesDiv.css( "visibility", "visible" );
            }

            /**
             * Init saved searches
             */
            function initSavedSearches() {
                var searches = $.cookie( "saved-searches" );
                if ( searches != null ) {
                    updateSavedSearchesList( searches );
                    savedSearchesDiv.css( "visibility", "visible" );
                }
            }

            initSavedSearches();

            /**
             * Apply saved search
             */
            savedSearchesElem.on( "change", function() {
                var searchKey = $( this ).val(),
                    savedSearches = $.cookie( "saved-searches" );
                if ( searchKey == "none" ) {
                    clearFiltersElem.click();
                } else {
                    var filters = savedSearches[ searchKey ].value;
                    clearFilters();
                    doSearch( 1, filters );
                    setFilters( filters );
                }
            } );

            /**
             * Set filters
             * @param data
             */
            function setFilters( data ) {
                searchEnabled = false;

                $.each( data, function( k, filters )  {
                    switch ( k ) {

                        case "active":
                            $.each( filters, function( j, filter ) {
                                selectTypeFilters.filter( "select[name=" + filter.name + "]" ).select2( "val", filter.value );
                            } );
                            break;

                        case "range":
                            $.each( filters, function( j, filter ) {
                                var input = searchForm.find( "div.range-filters input[name=" + filter.name + "]" ).slider( "destroy" );
                                input.slider( {
                                    handle: input.attr( "data-slider-handle" ),
                                    max: parseFloat(input.attr( "data-slider-max" )),
                                    min: parseFloat(input.attr( "data-slider-min" )),
                                    step: parseFloat(input.attr( "data-slider-step" )),
                                    value: [parseInt( filter.value[0] ), parseInt( filter.value[1] )]
                                } );
                            } );
                            break;

                        case "text":
                            $.each( filters, function( j, filter ) {
                                textTypeFilters.filter( "input[name=" + filter.name + "]" ).val( filter.value );
                            } );
                            break;
                    }
                } );

                searchEnabled = true;
            }

            /**
             * Do search button click
             */
            doSearchElem.on( "click", function( e ) {
                e.preventDefault();
                doSearch();
            } );

            /**
             * When select type filter was changed
             */
            selectTypeFilters.on( "change", function() {
                if ( $( this ).val() != 0 ) {
                    setActiveFilter( $( this ) );
                    if ( searchEnabled ) { doSearch(); }
                }
            } );

            /**
             * When Enter press on text type filter
             */
            textTypeFilters.on( "keyup", function( e ) {
                if ( e.keyCode == 13) doSearch();
            } );

            /**
             * Clear active filters
             */
            clearFiltersElem.on( "click", function( e ) {
                e.preventDefault();
                clearFiltersElem.hide();
                clearFilters();
                doSearch();
            } );

            /**
             * Remove active filter
             */
            activeFiltersWrap.on( "click", "a", function( e ) {
                e.preventDefault();
                removeActiveFilterByLink( $( this ) );
                doSearch();
            } );

            /**
             * Pagination clicks
             */
            prevPage.add( nextPage ).on( "click", function( e ) {
                e.preventDefault();

                var nextPage = $( this ).attr( "href" ).split( "page=" )[1],
                    page = nextPage ? nextPage : 1;

                doSearch( page );
            } );

            /**
             * Do search function
             */
            function doSearch( page, filters ) {
                var searchFilters = filters ? filters : getFilters();
                page = page ? page : 1;

                scrollTop();

                $.post('/results-ajax?page=' + page, searchFilters, function( data ) {
                    if ( data ) { showAds( data ); }
                } );
            }

            /**
             * Show founded ads
             * @param data
             */
            function showAds( data ) {
                var html = parseAds( data.data );
                showPagination( data );
                adsList.html( html );
                setPressedCompareButtons();
            }

            /**
             * Show or hide pagination
             */
            function showPagination( data ) {
                totalFound.html( data.total );
                currentPage.html( data.current_page );
                totalPages.html( data.last_page ? data.last_page : 1 );
                prevPage.attr( "href", data.prev_page_url );
                nextPage.attr( "href", data.next_page_url );

                // Show or Hide pagination and set links
                if ( data.data.length ) {
                    if ( ! data.next_page_url ) {
                        nextPage.addClass( "disabled" );
                    } else {
                        nextPage.removeClass( "disabled" );
                    }

                    if ( ! data.prev_page_url ) {
                        prevPage.addClass( "disabled" );
                    } else {
                        prevPage.removeClass( "disabled" );
                    }
                    pagination.show();
                } else {
                    pagination.hide();
                }
            }

            /**
             * Get all filters values
             * @returns {{active: Array, select: Array, range: Array, text: Array}}
             */
            function getFilters() {
                return {
                    active: getActiveFilters(),
                    select: getSelectFilters(),
                    range: getRangeFilters(),
                    text: getTextFilters()
                };
            }

            /**
             * Get active filters
             * @returns {Array}
             */
            function getActiveFilters() {
                var data = [];

                activeFiltersWrap.find( "div.active-filter" ).each( function( k, elem ) {
                    var $this = $( this ).find( "a" ),
                        filter = {
                            name: $this.attr( "data-name" ),
                            value: $this.attr( "data-value" )
                        };
                    data.push( filter );
                } );

                return data;
            }

            /**
             * Get select type filters
             * @returns {Array}
             */
            function getSelectFilters() {
                var data = [];

                selectTypeFilters.each( function( k, elem ) {
                    if ( selectFilterDisabled( elem ) ) { return false; }

                    var $this = $( this ),
                        filter = {
                            name: $this.attr( "name" ),
                            value: $this.val()
                        };
                    if ( filter.value != 0 ) {
                        data.push( filter );
                    }
                } );

                return data;
            }

            /**
             * Get range type filters
             * @returns {Array}
             */
            function getRangeFilters() {
                var data = [];

                rangeTypeFilters.each( function( k, elem ) {
                    var $this = $( this ),
                        filter = {
                            name: $this.attr( "name" ),
                            value: $this.val().split( "," )
                        };
                    if ( filter.value ) data.push( filter );
                } );

                return data;
            }

            /**
             * Get text type filters
             * @returns {Array}
             */
            function getTextFilters() {
                var data = [];

                textTypeFilters.each( function( k, elem ) {
                    var $this = $( this ),
                        filter = {
                            name: $this.attr( "name" ),
                            value: $this.val()
                        };
                    if ( filter.value != '' ) {
                        data.push( filter );
                    }
                } );

                return data;
            }

            /**
             * Adds filter to active list
             * @param elem
             */
            function setActiveFilter( elem ) {
                var name = elem.attr( "name" ),
                    value = elem.val(),
                    label = elem.select2( "data" )[0].text;

                activeFiltersWrap.append(
                    "<div class='active-filter'> " + label +
                        "<a href='#' data-name='" + name + "' data-value='" + value + "'>x</a>" +
                    "</div>"
                );

                hideSelectFilter( elem );
                clearFiltersElem.show();
            }

            /**
             * Hides Select types filter
             * @param elem
             */
            function hideSelectFilter( elem ) {
                elem.parents( "div.form-group" ).addClass( "disabled" ).hide();
            }

            /**
             * Clear active filters
             */
            function clearFilters() {
                var filters = activeFiltersWrap.find( "div.active-filter" );

                $.each( filters, function( k, item ) {
                    removeActiveFilterByLink( $( item ).find( "a" ) );
                } );

                $.each( rangeTypeFilters, function( k, input ) {
                    //var elem = $( input ),
                    //    handle = elem.attr( "data-slider-handle" ),
                    //    min = parseFloat( elem.attr( "data-slider-min" ) ),
                    //    max = parseFloat( elem.attr( "data-slider-max" ) ),
                    //    step = parseFloat( elem.attr( "data-slider-step" ) );
                    //elem.siblings( "div.slider" ).slider( {
                    //    handle: handle,
                    //    max: max,
                    //    min: min,
                    //    step: step,
                    //    value: [min, max]
                    //} );
                } );

                $.each( textTypeFilters, function( k, input ) {
                    $( input ).val( "" );
                } );
            }

            /**
             * Remove active filter
             * @param elem
             */
            function removeActiveFilterByLink( elem ) {
                var name = elem.attr( "data-name" ),
                    select = selectTypeFilters.filter( "select[name=" + name + "]" );

                elem.parent().remove();
                select.select2( "val", "0" );
                select.parents( "div.form-group" ).removeClass( "disabled" ).show();

                if ( activeFiltersWrap.find( "div.active-filter" ).length == 0 ) {
                    clearFiltersElem.hide();
                }
            }

            /**
             * Check if select filter is disabled
             * @param selectElem
             * @returns {*|jQuery}
             */
            function selectFilterDisabled( selectElem ) {
                return $( selectElem ).parent( "div.form-group" ).hasClass( "disabled" );
            }

            /**
             * Scroll top to ads list
             */
            function scrollTop() {
                $( "html, body" ).animate( {
                    scrollTop: $( "#ads-stage-wrap" ).offset().top
                }, 800);
            }

            /**
             * Ajax setup
             */
            $.ajaxSetup({
                headers: { 'X-CSRF-TOKEN': searchForm.find( "input[name=_token]" ).val() }
            });

            /**
             * Ajax start
             */
            $( document ).ajaxStart( function() { loader.show(); } );

            /**
             * Ajax stop
             */
            $( document ).ajaxStop( function() { loader.hide(); } );

            /**
             * Parse search results
             * @param ads
             * @returns {string|*}
             */
            function parseAds( ads ) {
                var html = "", photo = "";

                $.each( ads, function( k, ad ) {

                    if ( ad.photos.length ) {
                        photo = '<img src="' + getThumbnail( ad.photos[ 0 ].uri, "large" ) + '"' +
                            'class="img-thumbnail center-block" ' +
                            'alt="' + adPhotoDesc( ad ) + '" />';
                    } else {
                        photo = '<img class="img-thumbnail center-block"' +
                            'src="assets/public/images/no-photo.png" ' +
                            'alt="' +  adPhotoDesc( ad ) + '" />';
                    }

                    html +=
                        '<div class="col-md-12 col-sm-12"> ' +
                            '<!-- Ad top line -->' +
                            '<div class="text-left"> ' +
                                '<h4 class="border-success"></h4> ' +
                            '</div> ' +
                            '<!-- Ad info -->' +
                            '<div class="col-md-12"> ' +
                                '<!-- Ad photo -->' +
                                '<div class="col-lg-5 col-md-5 col-sm-12"> ' +
                                    '<a href="' + adURI( ad ) + '" target="_blank"> ' +
                                        photo +
                                    '</a> ' +
                                '</div> ' +
                                '<!-- Ad details -->' +
                                '<div class="col-lg-7 col-md-7 col-sm-12"> ' +
                                    '<div class="row"> ' +
                                        '<div class="col-md-9 text-left"> ' +
                                            '<h4><a href="' + adURI( ad ) + '" target="_blank">' + ad.year + ' ' + ad.make.name + ' ' + ad.model.name + '</a></h4> ' +
                                        '</div> ' +
                                        '<div class="col-md-3"> ' +
                                            '<span class="pull-right priceText"><h4><b>$' + ad.price + '</b></h4></span> ' +
                                        '</div> ' +
                                    '</div> ' +
                                    '<div class="dotted-line"></div> ' +
                                    '<!-- Buttons group -->' +
                                    '<div class="row ad-buttons"> ' +
                                        '<div class="col-sm-3"> ' +
                                            '<a href="' + adURI( ad ) + '#media" class="btn btn-default btn-block btn-ad-media" target="_blank">Photos (' + ad.photos.length + ')</a> ' +
                                        '</div> ' +
                                        '<div class="col-sm-3"> ' +
                                            '<span> ' +
                                                '<a href="' + adURI( ad ) + '#contact" class="btn btn-default btn-block btn-ad-email" target="_blank"';
                                                if ( ! shouldDisplayContacts( ad.owner ) )
                                                {
                                                    html += 'disabled="disabled"';
                                                }
                                                html += '>Email Seller</a> ' +
                                            '</span> ' +
                                        '</div> ' +
                                        '<div class="col-sm-3"> ' +
                                            '<span> ' +
                                                '<a href="/" data-ad-id="' + ad.ad_id + '" class="btn btn-default btn-block btn-ad-compare">Compare</a> ' +
                                            '</span> ' +
                                        '</div> ' +
                                        '<div class="col-sm-3"> ' +
                                            '<span> ' +
                                                '<a href="/" class="btn btn-default btn-block btn-ad-save" disabled="disabled">Save</a> ' +
                                            '</span> ' +
                                        '</div> ' +
                                    '</div><!-- end Buttons group --> ' +
                                    '<div class="dotted-line"></div> ' +
                                    '<!-- Car info --> ' +
                                    '<div class="row"> ' +
                                        '<div class="col-sm-5"> ' +
                                            '<p class="column-text"><strong>Stock #:</strong> ' + ad.ad_id + '</p> ' +
                                            '<p class="column-text"><strong>Exterior Color:</strong> ' + ad.ext_color.name + '</p> ' +
                                            '<p class="column-text"><strong>Interior Color:</strong> ' + ad.int_color.name + '</p> ' +
                                            '<p class="column-text"><strong>Miles:</strong> ' + ad.mileage + '</p> ' +
                                            '<p class="column-text"><strong>Transmission:</strong> ' + ad.transmission.name + '</p> ' +
                                            '<p class="column-text"><strong>Drive Type:</strong> ' + ad.drive_type.name + '</p> ' +
                                            '<p class="column-text"><strong>Fuel Type:</strong> ' + ad.fuel_type.name + '</p> ' +
                                        '</div> ' +
                                        '<div class="col-sm-7"> ' +
                                            '<p class="column-text"><strong>' + ad.owner.surname + ' ' + ad.owner.first_name + '</strong></p> ';
                                            if ( shouldDisplayContacts( ad.owner ) ) {
                                                html +=
                                                    ( ad.address ? '<p class="column-text">' + ad.address + '</p> ' : '' ) +
                                                    '<p class="column-text">' + ( ad.city ? ad.city + ', ' : '' ) + ( ad.state ? ad.state + ', ' : '' ) + ( ad.zip ? ad.zip : '' ) + '</p> ' +
                                                    '<p class="column-text"> ' +
                                                        '<i class="livicon" data-name="phone" data-size="16" data-loop="true" data-c="#01bc8c" data-hc="#01bc8c" id="livicon-11" style="width: 16px; height: 16px;"><svg height="16" version="1.1" width="16" xmlns="http://www.w3.org/2000/svg" style="overflow: hidden; position: relative; left: -0.0625px;" id="canvas-for-livicon-11"><desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël 2.1.0</desc><defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs><path fill="#01bc8c" stroke="none" d="M29,10.5C28.422,10.115,27.221,9.483,26.6,9.2C23.746000000000002,7.895,19.700000000000003,7.3999999999999995,16,7.3999999999999995C12.3,7.3999999999999995,8.254,7.895,5.4,9.2C4.779,9.484,3.578,10.115,3,10.5C2.4,10.9,1.6,11.899000000000001,2.1,13.2L3.5,16L10,12.6L9.324,11.247C9.2,11,9.245,10.707,9.5,10.601C10.7,10.101,12.9,9.301,16,9.301C19.1,9.301,21.299,10.101,22.5,10.601C22.754,10.707,22.799,11.001000000000001,22.676,11.248000000000001L22,12.601L28.5,16.001L29.898,13.201C30.4,11.9,29.6,10.9,29,10.5Z" transform="matrix(0.5,0,0,0.5,0,0)" stroke-width="0" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><path fill="#01bc8c" stroke="none" d="M23.899,16.601C23.2,15.100999999999999,21.700000000000003,14.200999999999999,20.200000000000003,13.600999999999999V12.501C20.200000000000003,11.401,19.499000000000002,10.901,18.499000000000002,10.901H16H16H13.5C12.5,10.901,11.8,11.401,11.8,12.501V13.600999999999999C10.3,14.200999999999999,8.8,15.100999999999999,8.100000000000001,16.601L6.200000000000001,22.901V26.401C6.200000000000001,27.301,7.000000000000001,28.001,7.800000000000001,28.001H16H16H24.2C24.999,28.001,25.8,27.302,25.8,26.401V22.901L23.899,16.601ZM16,21.301C13.073,21.301,10.7,19.599999999999998,10.7,17.5C10.7,15.402000000000001,13.073,13.701,16,13.701S21.3,15.402000000000001,21.3,17.5C21.3,19.6,18.927,21.301,16,21.301ZM18.3,17.5C18.3,18.494,17.271,19.301,16,19.301C14.73,19.301,13.7,18.494,13.7,17.5C13.7,16.506,14.729999999999999,15.701,16,15.701C17.271,15.701,18.3,16.506,18.3,17.5ZM14.9,19.6C14.624,19.6,14.4,19.825000000000003,14.4,20.1C14.4,20.377000000000002,14.624,20.6,14.9,20.6S15.4,20.377000000000002,15.4,20.1C15.4,19.824,15.176,19.6,14.9,19.6ZM12.9,18.5C12.624,18.5,12.4,18.725,12.4,19S12.624,19.5,12.9,19.5S13.4,19.275,13.4,19S13.176,18.5,12.9,18.5ZM12.2,16.5C11.924,16.5,11.7,16.724,11.7,17C11.7,17.276,11.924,17.5,12.2,17.5S12.7,17.276,12.7,17C12.7,16.725,12.477,16.5,12.2,16.5ZM13.8,14.9C13.524000000000001,14.9,13.3,15.124,13.3,15.4S13.524000000000001,15.9,13.8,15.9S14.3,15.676,14.3,15.4S14.076,14.9,13.8,14.9ZM16,14.219C15.724,14.219,15.5,14.443,15.5,14.719S15.724,15.219,16,15.219S16.5,14.995,16.5,14.719S16.276,14.219,16,14.219ZM16.7,20.1C16.7,20.377000000000002,16.923,20.6,17.201,20.6C17.476,20.6,17.701,20.377000000000002,17.701,20.1C17.701,19.825000000000003,17.476,19.6,17.201,19.6C16.923,19.6,16.7,19.824,16.7,20.1ZM18.701,19C18.701,19.275,18.924,19.5,19.201,19.5C19.476,19.5,19.701,19.275,19.701,19S19.476,18.5,19.201,18.5C18.924,18.5,18.701,18.725,18.701,19ZM19.4,16.9C19.4,17.174999999999997,19.622999999999998,17.4,19.9,17.4C20.174999999999997,17.4,20.4,17.174999999999997,20.4,16.9C20.4,16.624,20.174999999999997,16.4,19.9,16.4C19.623,16.4,19.4,16.625,19.4,16.9ZM17.801,15.4C17.801,15.676,18.023999999999997,15.9,18.301,15.9C18.575999999999997,15.9,18.801,15.676,18.801,15.4S18.575999999999997,14.9,18.301,14.9C18.023,14.9,17.801,15.125,17.801,15.4Z" stroke-width="0" transform="matrix(0.5,0,0,0.5,0,0)" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path></svg></i> ' + ad.phone +
                                                    '</p> ';
                                            }
                                            html +=
                                            '<p class="column-text"> ' +
                                                '<br /><a href="' + adURI( ad ) + '" target="_blank"><i class="livicon" data-name="link" data-size="16" data-loop="true" data-c="#f60" data-hc="#f60" id="livicon-12" style="width: 16px; height: 16px;"><svg height="16" version="1.1" width="16" xmlns="http://www.w3.org/2000/svg" style="overflow: hidden; position: relative; left: -0.0625px;" id="canvas-for-livicon-12"><desc style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);">Created with Raphaël 2.1.0</desc><defs style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></defs><path fill="#ff6600" stroke="none" d="M17.53,10.939C17.298000000000002,10.239,17.579,9.435,18.135,8.877L20.984,6.0280000000000005C21.771,5.2410000000000005,23.047,5.2410000000000005,23.835,6.0280000000000005L25.971,8.164000000000001C26.758,8.950000000000001,26.758,10.226,25.971,11.013000000000002L23.123,13.862000000000002C22.592000000000002,14.392000000000001,21.722,14.684000000000001,21.048000000000002,14.500000000000002L18.848000000000003,16.711000000000002C20.422000000000004,18.284000000000002,22.973000000000003,18.284000000000002,24.547000000000004,16.711000000000002L28.819000000000003,12.437000000000001C30.393000000000004,10.864,30.393000000000004,8.312000000000001,28.819000000000003,6.740000000000001L25.259,3.18C23.686,1.6070000000000002,21.134,1.6070000000000002,19.560000000000002,3.18L15.288000000000002,7.454000000000001C13.715000000000002,9.027000000000001,13.715000000000002,11.578,15.288000000000002,13.151L17.53,10.939Z" stroke-width="0" transform="matrix(0.5,0,0,0.5,0,0)" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><path fill="#ff6600" stroke="none" d="M10.939,17.53C10.238,17.298000000000002,9.435,17.579,8.877,18.136000000000003L6.0280000000000005,20.985000000000003C5.2410000000000005,21.772000000000002,5.2410000000000005,23.048000000000002,6.0280000000000005,23.835000000000004L8.165000000000001,25.971000000000004C8.952000000000002,26.758000000000003,10.227,26.758000000000003,11.014000000000001,25.971000000000004L13.863000000000001,23.122000000000003C14.392000000000001,22.593000000000004,14.684000000000001,21.723000000000003,14.501000000000001,21.049000000000003L16.712,18.849000000000004C18.285,20.423000000000005,18.285,22.973000000000003,16.712,24.546000000000003L12.439,28.819000000000003C10.865,30.393000000000004,8.314,30.393000000000004,6.741,28.819000000000003L3.18,25.259C1.6070000000000002,23.686,1.6070000000000002,21.135,3.18,19.561L7.452999999999999,15.289C9.027,13.715,11.578,13.715,13.151,15.289L10.939,17.53Z" stroke-width="0" transform="matrix(0.5,0,0,0.5,0,0)" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path><path fill="#ff6600" stroke="none" d="M20.628,11.37C21.219,11.959999999999999,21.219,12.915999999999999,20.628,13.507L13.507,20.628999999999998C12.917,21.218999999999998,11.961,21.218999999999998,11.37,20.628999999999998C10.78,20.038999999999998,10.78,19.083,11.37,18.491999999999997L18.491999999999997,11.369999999999997C19.082,10.78,20.038,10.78,20.628,11.37Z" stroke-width="0" transform="matrix(0.5,0,0,0.5,0,0)" style="-webkit-tap-highlight-color: rgba(0, 0, 0, 0);"></path></svg></i>' +
                                                ' View Details</a>' +
                                            '</p> ' +
                                        '</div> ' +
                                    '</div><!-- end Car info --> ' +
                                '</div><!-- end Ad details --> ' +
                            '</div><!-- end Ad info --> ' +
                        '</div>';
                } );

                return html;
            }

            /**
             * Returns true if contacts should be visible
             *
             * @param user
             * @returns {boolean}
             */
            function shouldDisplayContacts( user ) {
                return user.status == 1
            }

            /**
             * Get thumbnail
             * (js version of getThumbnail php function in htlpers.php file)
             */
            function getThumbnail( uri, name ) {
                var thumbnailUri,
                    uriArr,
                    result;

                uriArr = uri.split( "." );
                thumbnailUri = uriArr[0] + "-" + name + "." + uriArr[1];

                $.ajax( {
                    url: thumbnailUri,
                    async: false,
                    dataType: "html"
                } ).done( function () {
                    result = true;
                } ).fail( function () {
                    result = false;
                } );

                if ( result ) {
                    return thumbnailUri;
                }

                return uriArr.join( "." );
            }

            /**
             * Return ad photo description
             *
             * @param ad
             * @returns {data.desc|*|window.Raphael.desc}
             */
            function adPhotoDesc( ad ) {
                if ( ad.photos.length && ad.photos[ 0 ].desc ) {
                    return ad.photos[ 0 ].desc;
                } else {
                    adTitle( ad )
                }
            }

            /**
             * Return ad title
             *
             * @param ad
             * @returns {string}
             */
            function adTitle( ad ) {
                return ad.year + ' ' + ad.make.name + ' ' + ad.model.name;
            }

            /**
             * Returns Ad uri
             *
             * @param ad
             * @returns {string}
             */
            function adURI( ad ) {
                return '/ad/' + ad.ad_id + '/' + ad.slug;
            }

            /**
             * Set compare buttons as "pressed" for compare items
             */
            function setPressedCompareButtons() {
                var compareButtons = $( "body" ).find( "a.btn-ad-compare" ),
                    compareList = $.cookie( "compare" ) ? $.cookie( "compare" ) : [],
                    $button = null,
                    adId = null;

                $.each( compareButtons, function( k, button ) {
                    $button = $( button );
                    adId = $button.data( "ad-id" );
                    if ( compareList.indexOf( adId ) >= 0 ) {
                        $button.addClass( "active" );
                    } else {
                        $button.removeClass( "active" );
                    }
                } );
            }
        }
    };
}();