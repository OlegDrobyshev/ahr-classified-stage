var Ad = function() {

    return {
        init : function ( options ) {

            var adWrap = $( "div.ad-detailed" ),
                adButtonsDiv = adWrap.find( "div.ad-buttons" ),
                adTabsWrap = adWrap.find( "div#ad-desc" ),
                adTabsUl = adTabsWrap.find( "ul.nav-tabs" ),
                mapInitialized = false,
                contactSellerForm = $( "form.contact-seller" ),
                contactSellerUrl = "/contact-seller-ajax",
                emailsentMessage = $( "h4.email-sent" );

            /**
             * Send email to seller ajax
             */
            contactSellerForm.on( "submit", function( e ) {
                e.preventDefault();
                var data = $( this ).serialize(),
                    submitBtn = contactSellerForm.find( "input[type=submit]" );

                submitBtn.addClass( "disabled" ).val( "Sending" );

                // Ajax call
                $.post( contactSellerUrl, data, function( data ) {
                    contactSellerForm.find( "div.with-error, div.has-error" ).removeClass( "with-error has-error" );
                    submitBtn.removeClass( "disabled" ).val( "Send" );

                    // Validation errors
                    if ( data.errors ) {
                        options.grecaptcha.reset();
                        $.each( data.errors, function( name, value ) {
                            contactSellerForm.find( "[name=" + name + "]" ).parents( "div.form-group" ).addClass( "with-error" );
                            if ( name == "g-recaptcha-response" ) {
                                contactSellerForm.find( "div.g-recaptcha>div" ).addClass( "has-error" );
                            }
                        } );
                    }

                    // Sending error
                    if ( ( data.status === false ) && ( data.errors.length == 0 ) ) {
                        emailsentMessage
                            .removeClass( "text-success" )
                            .addClass( "text-danger" )
                            .html( "Something went wrong, please try again later" )
                            .show();
                    }

                    // Successfully sent
                    if ( data.status ) {
                        contactSellerForm[0].reset();
                        contactSellerForm.hide();
                        emailsentMessage
                            .removeClass( "text-danger" )
                            .addClass( "text-success" )
                            .html( "Your message has been sent" )
                            .show();
                    }
                } );
            } );

            /**
             * Scroll to block if hash added
             */
            ( function scrollByHash() {
                var hash = window.location.hash.slice( 1 );

                if ( hash == "contact" ) {
                    scrollTo( "#ad-contact" );
                }

                if ( hash == "media" ) {
                    clickTab( "ad_media" );
                    scrollTo( "#ad-desc" );
                }
            } )();

            /**
             * Click on ad main photo
             */
            adWrap.find( "a.ad-main-photo" ).on( "click", function( e ) {
                e.preventDefault();
                clickTab( "ad_media" );
            } );

            /**
             * Click on Email Seller button
             */
            adButtonsDiv.find( "a.btn-ad-email" ).on( "click", function( e ) {
                e.preventDefault();
                scrollTo( "#ad-contact" );
            } );

            /**
             * Click on Photos button
             */
            adButtonsDiv.find( "a.btn-ad-media" ).on( "click", function( e ) {
                e.preventDefault();
                clickTab( "ad_media" );
            } );

            /**
             * Initialize Util Carousel
             */
            $( "#adMedia" ).utilCarousel( {
                responsiveMode : "itemWidthRange",
                itemWidthRange : [ 260, 320 ]
            } );

            /**
             * Scroll on tab links click
             */
            adTabsWrap.find( "a" ).on( "click", function() {
                scrollTo( "#ad-desc" );
            } );

            /**
             * Show ad on map
             */
            adTabsWrap.find( "a[href=#ad_map]" ).on( "click", function() {
                if ( ! mapInitialized ) {
                    initMap();
                    mapInitialized = true;
                }
            } );

            /**
             * Click ad details tab
             * @param tab
             */
            function clickTab( tab ) {
                adTabsUl.find( "a[href=#" + tab + "]" ).click();
            }

            /**
             * Scroll to element by ID
             */
            function scrollTo( selector ) {
                $( "html, body" ).animate( {
                    scrollTop: $( selector ).offset().top
                }, 800);
            }

            /**
             * Google Map Init map
             */
            function initMap() {
                var mapDiv = document.getElementById( "map" ),
                    address = mapDiv.dataset.address,
                    map = new google.maps.Map(document.getElementById( "map" ), {
                    zoom: 8,
                    center: {lat: -34.397, lng: 150.644}
                });
                var geocoder = new google.maps.Geocoder();

                geocodeAddress(geocoder, map, address);
            }

            /**
             * Google Map geocode address
             * @param geocoder
             * @param resultsMap
             */
            function geocodeAddress(geocoder, resultsMap, address) {
                geocoder.geocode({'address': address}, function(results, status) {
                    if (status === google.maps.GeocoderStatus.OK) {
                        resultsMap.setCenter(results[0].geometry.location);
                        var marker = new google.maps.Marker({
                            map: resultsMap,
                            position: results[0].geometry.location
                        });
                    } else {
                        alert('Geocode was not successful for the following reason: ' + status);
                    }
                });
            }

            /**
             * Approve button click
             */
            $( "a.ad-approval" ).on( "click", function( e ) {
                e.preventDefault();

                var $this = $( this ).attr( "disabled", true ),
                    data = {
                        disabled: ( $this.attr( "data-disabled" ) == 0 ) ? 1 : 0,
                        adId: $this.data( "ad-id" )
                    },
                    newClass = data.disabled ? "btn-success" : "btn-danger",
                    newText = data.disabled ? "Approve" : "Block";

                $.post( "/admin/approve-ad-ajax", data, function( response ) {
                    $this.removeClass( "btn-success btn-danger" ).addClass( newClass );
                    $this.attr( "data-disabled", data.disabled );
                    $this.html( newText );
                    $this.attr( "disabled", false );
                } );
            } );

            /**
             * Ajax setup
             */
            $.ajaxSetup( {
                headers: { 'X-CSRF-TOKEN': contactSellerForm.find( "input[name=_token]" ).val() }
            } );

        }
    }
}();