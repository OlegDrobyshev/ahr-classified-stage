var Registration = function() {

    return {
        init : function ( options ) {

            var form = $( "form#registration-form" ),
                plans = form.find( "div.plans a" ),
                planIdInput = form.find( "input[name=plan_id]" ),
                card = form.find( "#card" ),
                cardBlock = form.find( "div.card-block" );

            /**
             * Change subscription plan
             */
            plans.on( "click", function( e ) {
                e.preventDefault();
                var $this = $( this ),
                    planId = $this.data( "plan-id" );
                planIdInput.val( planId );
                plans.removeClass( "btn-success btn-default" ).addClass( "btn-default" );
                $this.removeClass( "btn-default" ).addClass( "btn-success" );
                if ( planId == 2 ) {
                    cardBlock.show();
                } else {
                    cardBlock.hide();
                }
            } );

            /**
             * Credit Card init
             */
            card.card( {
                container: $( ".card-wrapper" )
            } );

            /**
             * Block submit button on form submit
             */
            form.on( "submit", function() {
                $( this ).find( "input[type=submit]" ).attr( "disabled", true );
            } );
        }
    };
}();

/**
 * Address auto-complete input
 */
var adCreateAddress = function() {
    return {
        init : function () {
            var adressInput = $( "input#address" ),
                cityInput = $( "input#city" ),
                stateInput = $( "input#state" ),
                zipInput = $( "input#zip" ),
                placesAutocomplete = places( {
                    container: adressInput[0]
                } );

            /**
             * On address change event
             */
            placesAutocomplete.on( "change", function resultSelected( e ) {
                placesAutocomplete.setVal( e.suggestion.name || "" );
                cityInput.val( e.suggestion.city || "" );
                stateInput.val( e.suggestion.administrative || "" );
                zipInput.val( e.suggestion.postcode || "" );
            });

            /**
             * Clear address fileds
             */
            placesAutocomplete.on( "clear", function() {
                cityInput.val( "" );
                stateInput.val( "" );
                zipInput.val( "" );
            } );
        }
    }
}();