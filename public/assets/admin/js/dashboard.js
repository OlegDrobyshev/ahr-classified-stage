$( function () {

    /**
     * Edit price
     */
    ( function() {
        var editPriceBtn = $( "div.dashboard-box a.edit-price" ),
            subscriptionPrice = $( "div.dashboard-box #subscription-price" );

        /**
         * Click on edit price link
         */
        editPriceBtn.on( "click", function( e ) {
            e.preventDefault();
            subscriptionPrice.editable( "toggle" );
            e.stopPropagation();
        } );

        /**
         * Subscription price
         */
        subscriptionPrice.editable( {
            type: "text",
            pk: 1,
            url: "/admin/update-subscription-price-ajax",
            title: "Enter New Price",
            display: function( value, sourceData ) {
                if ( ! isNaN( value ) ) {
                    $( this ).text( "$" + parseFloat( value ) );
                }
            },
            success: function( data ) {
                showMessages( data );
            }
        } );
    } )();

    /**
     * Dashboard top boxes
     */
    ( function() {
        var useOnComplete = false,
            useEasing = false,
            useGrouping = false,
            options = {
                useEasing : useEasing, // toggle easing
                useGrouping : useGrouping, // 1,000,000 vs 1000000
                separator : ',', // character to use as a separator
                decimal : '.' // character to use as a decimal
            };

        var countUp = new CountUp("myTargetElement1", statStartValue( stat.users.allTime ), stat.users.allTime, 0, 6, options);
        countUp.start();
        countUp = new CountUp("myTargetElement2", statStartValue( stat.ads.allTime ), stat.ads.allTime, 0, 6, options);
        countUp.start();
        countUp = new CountUp("myTargetElement3", statStartValue( stat.profit.allTime ), stat.profit.allTime, 0, 6, options);
        countUp.start();
        countUp = new CountUp("myTargetElement1.1", statStartValue( stat.users.lastWeek ), stat.users.lastWeek, 0, 6, options);
        countUp.start();
        countUp = new CountUp("myTargetElement1.2", statStartValue( stat.users.lastMonth ), stat.users.lastMonth, 0, 6, options);
        countUp.start();
        countUp = new CountUp("myTargetElement2.1", statStartValue( stat.ads.lastWeek ), stat.ads.lastWeek, 0, 6, options);
        countUp.start();
        countUp = new CountUp("myTargetElement2.2", statStartValue( stat.ads.lastMonth ), stat.ads.lastMonth, 0, 6, options);
        countUp.start();
        countUp = new CountUp("myTargetElement31", statStartValue( stat.profit.lastWeek ), stat.profit.lastWeek, 0, 6, options);
        countUp.start();
        countUp = new CountUp("myTargetElement32", statStartValue( stat.profit.lastMonth ), stat.profit.lastMonth, 0, 6, options);
        countUp.start();
    } )();

    /**
     * Approve button click
     */
    $( "table.ads-dashboard-table" ).on( "click", "a.ad-approval", function( e ) {
        e.preventDefault();

        var $this = $( this ).attr( "disabled", true ),
            data = {
                disabled: ( $this.attr( "data-disabled" ) == 0 ) ? 1 : 0,
                adId: $this.data( "ad-id" )
            },
            classes = {
                "tr": data.disabled ? "disabled-row" : "enabled-row",
                "a": data.disabled ? "btn-success" : "btn-danger",
                "span": data.disabled ? "glyphicon-thumbs-up" : "glyphicon-thumbs-down"
            };

        $.post( "/admin/approve-ad-ajax", data, function( response ) {
            showMessages( response.messages );
            $this.parents( "tr" ).removeClass( "enabled-row disabled-row" ).addClass( classes.tr );
            $this.removeClass( "btn-success btn-danger" ).addClass( classes.a );
            $this.find( "span" ).removeClass( "glyphicon-thumbs-up glyphicon-thumbs-down" ).addClass( classes.span );
            $this.attr( "data-disabled", data.disabled );
            $this.attr( "disabled", false );
        } );
    } );

    /**
     * Toastr plugin options
     */
    toastr.options = {
        closeButton: true,
        debug: true,
        hideDuration: 300
    };

    /**
     * Show popup message
     *
     * @param messages
     */
    function showMessages( messages ) {
        if ( messages ) {
            $.each( messages, function( k, message ) {
                if ( message.type == "success" ) {
                    toastr.success( message.text, "Success" );
                } else if ( message.type == "warning" ) {
                    toastr.warning( message.text, "Warning" );
                } else if ( message.type == "danger" ) {
                    toastr.error( message.text, "Error" );
                } else if ( message.type == "info" ) {
                    toastr.info( message.text, "Info" );
                }
            } );
        }
    }

    /**
     * @param value
     * @returns {number}
     */
    function statStartValue( value ) {
        return value - Math.floor( value / 2 );
    }

    /**
     * Ads / Users chart
     */
    ( function() {
        var data = [ {
            label: "&nbsp;&nbsp;Users",
            data: chartsData.ads_users.users,
            color: "#418BCA"
        }, {
            label: "&nbsp;&nbsp;Ads",
            data: chartsData.ads_users.ads,
            color: "#F60"
        } ];

        var options = {
            xaxis: {
                min: xaxisMin,
                max: xaxisMax,
                mode: "time",
                tickSize: [ 1, "month" ],
                monthNames: [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ],
                tickLength: 0
            },
            yaxis: {

            },
            series: {
                lines: {
                    show: true,
                    fill: false,
                    lineWidth: 2
                },
                points: {
                    show: true,
                    radius: 4.5,
                    fill: true,
                    fillColor: "#ffffff",
                    lineWidth: 2
                }
            },
            grid: {
                hoverable: true,
                clickable: false,
                borderWidth: 0
            },
            legend: {
                container: '#basicFlotLegend',
                show: true
            },
            tooltip: true,
            tooltipOpts: {
                content: '%s: %y'
            }

        };

        var holder = $( "#line-chart" );

        if ( holder.length ) {
            $.plot( holder, data, options );
        }
    } )();

    /**
     * Profit chart
     */
    ( function() {
        $.plot( "#bar-chart", [ {
            data: chartsData.profit,
            label: "Paid",
            color: "#F89A14"
        } ], {
            series: {
                lines: {
                    show: true,
                    fill: true,
                    lineWidth: 2
                },
                points: {
                    show: true,
                    radius: 4.5,
                    fill: true,
                    fillColor: "#ffffff",
                    lineWidth: 2
                }
            },
            grid: {
                borderColor: "#ddd",
                borderWidth: 1,
                hoverable: ! 0
            },
            legend: {
                container: '#basicFlotLegend2',
                show: false
            },
            tooltip: true,
            tooltipOpts: {
                content: "%s: $%y"
            },
            xaxis: {
                tickColor: "#ddd",
                min: xaxisMin,
                max: xaxisMax,
                mode: "time",
                tickSize: [ 1, "month" ]
            },
            yaxis: {
                tickColor: "#ddd"
            },
            shadowSize: 0
        } );
    } )();
} );