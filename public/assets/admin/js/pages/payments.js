var Payments = function() {

    return {
        init : function ( options ) {

            var dataTable = $( "table#payments" ),
                dataTableAjaxSource = "/admin/payments/data-table-ajax-source",
                loader = $( "div.loader-wrap" ),
                filterGateway = $( "select.gateway-id" ),
                filterStatus = $( "select.status" ),
                filterDateRange = $( "input.date-range" ),
                clearFilters = $( "a.clear" ),
                shouldBeRefreshed = true;

            /**
             * Clear filters
             */
            clearFilters.on( "click", function( e ) {
                e.preventDefault();
                shouldBeRefreshed = false;
                filterGateway.select2( "val", null );
                filterStatus.select2( "val", null );
                filterDateRange.val( null );
                shouldBeRefreshed = true;
                refreshDataTable();
            } );

            /**
             * Filter by gateway
             */
            filterGateway.on( "change", function() {
                refreshDataTable();
            } );

            /**
             * Filter by status
             */
            filterStatus.on( "change", function() {
                refreshDataTable();
            } );

            /**
             * Initialize datetime picker
             */
            filterDateRange.daterangepicker( {
                locale: {
                    format: 'MM/DD/YYYY'
                }
            }, function( start, end, label ) {
                refreshDataTable();
            } );

            /**
             * Get DataTable Attributes
             */
            function getDataTableParams() {
                return {
                    "processing" : true,
                    "serverSide" : true,
                    "ajax" : {
                        method : "POST",
                        url : dataTableAjaxSource,
                        data: {
                            gatewayId: filterGateway.val(),
                            status: filterStatus.val(),
                            dateRange: filterDateRange.val()
                        }
                    },
                    "order": [ [ 5, "desc" ] ],
                    "aoColumnDefs" : [ {"bSortable" : false, "aTargets" : [ 0 ] } ],
                    "searching": false,
                    "aoColumns" : [
                        { "sClass" : "text-center" },
                        { "sClass" : "text-center" },
                        { "sClass" : "text-center" },
                        { "sClass" : "text-center" },
                        { "sClass" : "text-center" },
                        { "sClass" : "text-center" }
                    ]
                }
            }

            /**
             *  Initialize DataTable
             */
            dataTable.dataTable( getDataTableParams() );

            /**
             * Refresh DataTable
             */
            function refreshDataTable() {
                if ( shouldBeRefreshed ) {
                    dataTable.fnDestroy();
                    dataTable.dataTable( getDataTableParams() );
                }
            }

            $( document ).ajaxStart( function() { loader.show(); } );

            $( document ).ajaxStop( function() { loader.hide(); } );
        }
    };
}();