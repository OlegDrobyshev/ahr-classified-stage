var Users = function() {

    return {
        init : function ( options ) {

            var dataTable = $( "table#users" ),
                dataTableAjaxSource = "/admin/users/data-table-ajax-source",
                loader = $( "div.loader-wrap" ),
                deleteUserUri = "/admin/user-delete";

            /**
             * Toastr plugin options
             */
            toastr.options = {
                closeButton: true,
                debug: true,
                hideDuration: 300
            };

            /**
             * Delete user confirmation
             */
            dataTable.on( "click", "a.user-delete", function( e ) {
                if ( ! confirm( "Warning: all user ads will be deleted, this action can't be undone." ) ) return false;
                var $this = $( this ),
                    userId = $this.data( "user-id" );
                $.post( deleteUserUri, {  userId: userId }, function( data ) {
                    if ( data.status ) {
                        $this.parents( "tr" ).remove();
                    }
                    showMessages( data.messages );
                } );
            } );

            /**
             * Show popup message
             * @param messages
             */
            function showMessages( messages ) {
                if ( messages ) {
                    $.each( messages, function( k, message ) {
                        if ( message.type == "success" ) {
                            toastr.success( message.text, "Success" );
                        } else if ( message.type == "warning" ) {
                            toastr.warning( message.text, "Warning" );
                        } else if ( message.type == "danger" ) {
                            toastr.error( message.text, "Error" );
                        } else if ( message.type == "info" ) {
                            toastr.info( message.text, "Info" );
                        }
                    } );
                }
            }

            /**
             * Get DataTable Attributes
             */
            function getDataTableParams() {
                return {
                    "processing" : true,
                    "serverSide" : true,
                    "ajax" : {
                        method : "POST",
                        url : dataTableAjaxSource
                    },
                    "order": [ [ 6, "desc" ] ],
                    "aoColumnDefs" : [ {"bSortable" : false, "aTargets" : [ 0, 7 ] } ],
                    "aoColumns" : [
                        { "sClass" : "text-center" },
                        null, null,
                        { "sClass" : "text-center" },
                        { "sClass" : "text-center" },
                        { "sClass" : "text-center" },
                        { "sClass" : "text-center" },
                        { "sClass" : "text-center" }
                    ],
                    oLanguage : {
                        sSearch: 'Search By Surname'
                    }
                }
            }

            /**
             *  Initialize DataTable
             */
            dataTable.dataTable( getDataTableParams() );

            $( document ).ajaxStart( function() { loader.show(); } );

            $( document ).ajaxStop( function() { loader.hide(); } );

        }
    };
}();