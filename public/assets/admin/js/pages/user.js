var User = function() {

    return {
        init : function ( options ) {
            var updateUserStatusUri = "/admin/update-user-status-ajax",
                hash = window.location.hash;

            /**
             * Open tab if hash included in url
             */
            if ( hash ) {
                $( "ul.nav a[href=" + hash + "]" ).click();
            }

            /**
             * Change user status
             */
            $( "#status" ).editable( {
                url: updateUserStatusUri,
                params: function( params ) {
                    params.userId = options.userId;
                    return params;
                },
                source: [
                    { value: 0, text : "Disabled" },
                    { value: 1, text : "Active" },
                    { value: 2, text : "Expired" }
                ],
                display: function( value, sourceData ) {
                    var colors = { 0 : "text-danger", 1 : "text-success", 2 : "text-warning" },
                        elem = $.grep( sourceData, function( o ) { return o.value == value; } );

                    if ( elem.length ) {
                        $( this ).text( elem[ 0 ].text ).removeClass( "text-success text-danger text-warning" );
                        $( this ).text( elem[ 0 ].text ).addClass( colors[ value ] );
                    }
                },
                success: function( data ) {
                    showMessages( [ {
                        "type": "success",
                        "text": "User was successfully updated"
                    } ] );
                }
            } );

            /**
             * Toastr plugin options
             */
            toastr.options = {
                closeButton: true,
                debug: true,
                hideDuration: 300
            };

            /**
             * Show popup message
             * @param messages
             */
            function showMessages( messages ) {
                if ( messages ) {
                    $.each( messages, function( k, message ) {
                        if ( message.type == "success" ) {
                            toastr.success( message.text, "Success" );
                        } else if ( message.type == "warning" ) {
                            toastr.warning( message.text, "Warning" );
                        } else if ( message.type == "danger" ) {
                            toastr.error( message.text, "Error" );
                        } else if ( message.type == "info" ) {
                            toastr.info( message.text, "Info" );
                        }
                    } );
                }
            }

            if ( options.popUpMessages ) {
                showMessages( options.popUpMessages );
            }
        }
    };
}();