var Ads = function() {

    return {
        init : function ( options ) {

            var dataTable = $( "table#ads" ),
                dataTableAjaxSource = "/admin/ads/data-table-ajax-source",
                loader = $( "div.loader-wrap" ),
                filters = $( "div.ads-filters" ),
                filterMake = filters.find( "[name=make_id]" ),
                filterModel = filters.find( "[name=model_id]" ),
                filterStatus = filters.find( "[name=status]" ),
                filterSellerEmail = filters.find( "[name=email]" ),
                clearFiltersBtn = filters.find( "a.clear" ),
                searchEmailAjaxUri = "/admin/search-seller-email-ajax",
                shouldBeRefreshed = true;

            /**
             * Clear filters
             */
            clearFiltersBtn.on( "click", function( e ) {
                e.preventDefault();
                shouldBeRefreshed = false;
                filterMake.select2( "val", "" );
                filterModel.select2( "val", "" );
                filterStatus.select2( "val", "" );
                filterSellerEmail.select2( "val", "" );
                shouldBeRefreshed = true;
                refreshDataTable();
            } );

            /**
             * Filter by make
             */
            filterMake.on( "change", function() {
                var makeId = $( this ).val();
                updateModelsListAjax( makeId, filterModel );
                refreshDataTable();
            } );

            /**
             * Filter by model
             */
            filterModel.on( "change", function() {
                refreshDataTable();
            } );

            /**
             * Filter by status
             */
            filterStatus.on( "change", function() {
                refreshDataTable();
            } );

            /**
             * Filter by seller email
             */
            filterSellerEmail.select2( {
                ajax: {
                    url: searchEmailAjaxUri,
                    type: "POST",
                    delay: 250,
                    processResults: function (data) {
                        return {
                            results: data.items
                        };
                    }
                },
                escapeMarkup: function( markup ) {
                    return markup;
                }
            } ).on( "change", function() {
                refreshDataTable();
            } );

            /**
             * Update models list by makeId
             *
             * @param makeId
             * @param targetList
             */
            function updateModelsListAjax( makeId, targetList ) {
                $.post( "/admin/get-models-by-make-id-ajax", { makeId: makeId },
                    function( data ) {
                        var output = '',
                            selected = null,
                            emptyList = true;

                        $.each( data, function( key, value ) {
                            if ( key > 0 ) {
                                emptyList = false;
                            }
                            selected = ( key == "any" ) ? "selected" : "";
                            output += "<option value='" + key + "' " + selected + ">" + value + "</option>";
                        } );

                        if ( emptyList ) {
                            targetList.attr( "disabled", true );
                            return false;
                        }

                        targetList.html( output ).removeAttr( "disabled" );
                    }
                );
            }

            /**
             * Toastr plugin options
             */
            toastr.options = {
                closeButton: true,
                debug: true,
                hideDuration: 300
            };

            /**
             * Show popup message
             *
             * @param messages
             */
            function showMessages( messages ) {
                if ( messages ) {
                    $.each( messages, function( k, message ) {
                        if ( message.type == "success" ) {
                            toastr.success( message.text, "Success" );
                        } else if ( message.type == "warning" ) {
                            toastr.warning( message.text, "Warning" );
                        } else if ( message.type == "danger" ) {
                            toastr.error( message.text, "Error" );
                        } else if ( message.type == "info" ) {
                            toastr.info( message.text, "Info" );
                        }
                    } );
                }
            }

            /**
             * Approve button click
             */
            dataTable.on( "click", "a.ad-approval", function( e ) {
                e.preventDefault();

                var $this = $( this ).attr( "disabled", true ),
                    data = {
                        disabled: ( $this.attr( "data-disabled" ) == 0 ) ? 1 : 0,
                        adId: $this.data( "ad-id" )
                    };

                $.post( "/admin/approve-ad-ajax", data, function( response ) {
                    refreshDataTable();
                    showMessages( response.messages );
                } );
            } );

            /**
             * Get DataTable Attributes
             */
            function getDataTableParams() {
                return {
                    "processing" : true,
                    "serverSide" : true,
                    "ajax" : {
                        method : "POST",
                        url : dataTableAjaxSource,
                        data: {
                            makeId: getFilterValue( filterMake.val() ),
                            modelId: getFilterValue( filterModel.val() ),
                            status: getFilterValue( filterStatus.val() ),
                            userId: getFilterValue( filterSellerEmail.val() )
                        }
                    },
                    "order": [ [ 6, "desc" ] ],
                    "aoColumnDefs" : [ {"bSortable" : false, "aTargets" : [ 0, 7 ] } ],
                    "aoColumns" : [
                        { "sClass" : "text-center" },
                        { "sClass" : "text-center" },
                        { "sClass" : "text-center" },
                        { "sClass" : "text-center" },
                        { "sClass" : "text-center" },
                        null,
                        { "sClass" : "text-center" },
                        { "sClass" : "text-center" }
                    ],
                    oLanguage : {
                        sSearch: 'Search By Make'
                    }
                }
            }

            /**
             *  Initialize DataTable
             */
            dataTable.dataTable( getDataTableParams() );

            /**
             * Refresh DataTable
             */
            function refreshDataTable() {
                if ( shouldBeRefreshed ) {
                    dataTable.fnDestroy();
                    dataTable.dataTable( getDataTableParams() );
                }
            }

            /**
             * Get Filter value
             *
             * @param value
             * @returns {*}
             */
            function getFilterValue( value ) {
                value = $.trim( value );
                return ( value && ( value != "any" ) ) ? value : "";
            }

            $( document ).ajaxStart( function() { loader.show(); } );

            $( document ).ajaxStop( function() { loader.hide(); } );

        }
    };
}();