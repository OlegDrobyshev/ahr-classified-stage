var SendEmails = function() {

    return {
        init : function ( options ) {

            /**
             * Initialize TinyMCE
             */
            tinymce.init( {
                selector: "#body",
                plugins: [
                    "advlist autolink lists image link charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime table contextmenu paste"
                ],
                toolbar: "undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link"
            } );

            /**
             * Toastr plugin options
             */
            toastr.options = {
                closeButton: true,
                debug: true,
                hideDuration: 300
            };

            /**
             * Show popup message
             * @param messages
             */
            function showMessages( messages ) {
                if ( messages ) {
                    $.each( messages, function( k, message ) {
                        if ( message.type == "success" ) {
                            toastr.success( message.text, "Success" );
                        } else if ( message.type == "warning" ) {
                            toastr.warning( message.text, "Warning" );
                        } else if ( message.type == "danger" ) {
                            toastr.error( message.text, "Error" );
                        } else if ( message.type == "info" ) {
                            toastr.info( message.text, "Info" );
                        }
                    } );
                }
            }

            if ( options.popUpMessages ) {
                showMessages( options.popUpMessages );
            }

        }
    };
}();