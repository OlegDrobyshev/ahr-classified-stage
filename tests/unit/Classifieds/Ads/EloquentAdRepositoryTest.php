<?php

/**
 * Class EloquentAdRepository
 */
class EloquentAdRepositoryTest extends TestCase
{
    public $adModelMock;

    public function setUp()
    {
        parent::setUp();

        $this->adModelMock = Mockery::mock('Classifieds\Ads\Ad');
    }

    protected function bindMocks()
    {
        $this->app->instance('Classifieds\Ads\Ad', $this->adModelMock);
    }

    /**
     * @return mixed
     */
    protected function buildAdRepo()
    {
        return $this->app->make('Classifieds\Ads\EloquentAdRepository');
    }

    /**
     * Test set default params
     */
    public function test_default_values_for_filter()
    {
        // Prepare and expectations
        $this->adModelMock
            ->shouldReceive('with')
            ->with(['user', 'condition', 'make', 'model', 'intColor', 'extColor', 'transmission', 'driveType', 'fuelType'])
            ->once()
            ->andReturnSelf();

        $this->adModelMock
            ->shouldReceive('get')
            ->once()
            ->andReturn(factory('Classifieds\Ads\Ad', 3)->make());

        $this->bindMocks();
        $adRepo = $this->buildAdRepo();

        // Call
        $ads = $adRepo->filter([]);

        // Assertions
        $this->assertInstanceOf(Illuminate\Database\Eloquent\Collection::class, $ads);
    }

    /**
     * Test Laravel's simple paginate for filter method
     */
    public function test_simple_pagination_for_filter_method()
    {
        $items = factory('Classifieds\Ads\Ad')->make();
        $shouldReturn = $this->app->build('Illuminate\Pagination\Paginator', [$items, 1]);

        $this->adModelMock->shouldReceive('with')->once()->andreturnSelf();
        $this->adModelMock->shouldReceive('simplePaginate')->with(1)->once()->andreturn($shouldReturn);
        $this->bindMocks();

        $adRepo = $this->buildAdRepo();
        $data = $adRepo->filter(['simplePaginate' => 1]);

        $this->assertInstanceOf('Illuminate\Pagination\Paginator', $data);
    }

    /**
     * Test Laravel's paginate for filter method
     */
    public function test_pagination_for_filter_method()
    {
        $items = factory('Classifieds\Ads\Ad')->make();
        $shouldReturn = $this->app->build('Illuminate\Pagination\LengthAwarePaginator', [$items, 10, 1]);

        $this->adModelMock->shouldReceive('with')->once()->andreturnSelf();
        $this->adModelMock->shouldReceive('paginate')->with(1)->once()->andreturn($shouldReturn);
        $this->bindMocks();

        $adRepo = $this->buildAdRepo();
        $data = $adRepo->filter(['paginate' => 1]);

        $this->assertInstanceOf('Illuminate\Pagination\LengthAwarePaginator', $data);
    }

    /**
     * Test getUserAds method
     */
    public function test_get_user_ads_method()
    {
        $this->adModelMock->shouldReceive('whereUserId')->with(1)->once()->andReturnSelf();
        $this->adModelMock->shouldReceive('whereDisabled')->with(0)->once()->andReturnSelf();
        $this->adModelMock->shouldReceive('get')->once();
        $this->bindMocks();

        $adRepo = $this->buildAdRepo();
        $adRepo->getUserAds(1, 0);
    }

    /**
     * Test getUserAdsCount method
     */
    public function test_get_user_ads_count_method()
    {
        $this->adModelMock->shouldReceive('whereUserId')->with(1)->once()->andReturnSelf();
        $this->adModelMock->shouldReceive('whereDisabled')->with(0)->once()->andReturnSelf();
        $this->adModelMock->shouldReceive('get')->once()->andReturnSelf();
        $this->adModelMock->shouldReceive('count')->once();
        $this->bindMocks();

        $adRepo = $this->buildAdRepo();
        $adRepo->getUserAdsCount(1, 0);
    }

    /**
     * Test [getYearsRange, getPriceRange, getMilesRange] methods
     */
    public function test_get_ads_range_methods()
    {
        $methods = [
            'getYearsRange' => 'year',
            'getPriceRange' => 'price',
            'getMilesRange' => 'mileage'
        ];

        $keys = ['min', 'max', 'step', 'init-min'];

        foreach ($methods as $method => $column)
        {
            $this->adModelMock->shouldReceive('min')->with($column)->once();
            $this->adModelMock->shouldReceive('max')->with($column)->once();
            $this->bindMocks();

            $adRepo = $this->buildAdRepo();
            $range = $adRepo->{$method}();

            $this->assertInternalType('array', $range);
            $this->assertArrayKeysAndItemsTypes($range, $keys);
        }
    }

    /**
     * Tet filter method
     */
    public function testFilterMethod()
    {
        $defaults = [
            'relations' => ['user', 'condition', 'make', 'model', 'intColor', 'extColor', 'transmission', 'driveType', 'fuelType']
        ];

        $param = [
            'relations' => ['relation'],
            'where' => ['column' => 'value'],
            'whereBetween' => ['column' => [0, 1]],
            'whereLike' => ['column' => 'value'],
            'order' => ['column' => 'value'],
            'total' => 1,
        ];

        $withArrayWithDefaults = array_merge_recursive($param['relations'], $defaults['relations']);

        $this->adModelMock->shouldReceive('with')->with($withArrayWithDefaults)->once()->andreturnSelf();
        $this->adModelMock->shouldReceive('where')->with('column', 'value')->once()->andreturnSelf();
        $this->adModelMock->shouldReceive('whereBetween')->with('column', [0, 1])->once()->andreturnSelf();
        $this->adModelMock->shouldReceive('where')->with('column', 'LIKE', '%value%')->once()->andreturnSelf();
        $this->adModelMock->shouldReceive('orderBy')->with('column', 'value')->once()->andreturnSelf();
        $this->adModelMock->shouldReceive('limit')->with(1)->once()->andreturnSelf();
        $this->adModelMock->shouldReceive('get')->once()->andreturn(factory('Classifieds\Ads\Ad')->make());
        $this->bindMocks();

        $adRepo = $this->buildAdRepo();
        $data = $adRepo->filter($param);

        $this->assertInstanceOf('Classifieds\Ads\Ad', $data);
    }

}