<?php

/**
 * Class AdTest
 */
class AdTest extends TestCase {

    /**
     * Test Ad price get mutator
     */
    public function test_price_attr_mutator()
    {
        $ad = factory('Classifieds\Ads\Ad')->make([
            'price' => 1000
        ]);

        $this->assertEquals('1,000', $ad->price);
    }
}