<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Mockery\Mock;

class AdsControllerServiceTest extends TestCase
{

    private $class;

    /**
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function setUp()
    {
        parent::setUp();

        Artisan::call('migrate:refresh');

        $this->_makeUserAndSignIn();

        $this->class = $this->app->build('Classifieds\Services\AdsControllerService');
    }

    /**
     * Test if class has user property
     */
    public function testLoggedInUserPropertyExists()
    {
        $this->assertNotEmpty($this->class->user);
        $this->assertInstanceOf('Classifieds\Users\User', $this->class->user);
    }

    /**
     * Make user and sign in as this user
     */
    private function _makeUserAndSignIn()
    {
        $user = factory('Classifieds\Users\User', 1)->make();

        $this->be($user);
    }
}
