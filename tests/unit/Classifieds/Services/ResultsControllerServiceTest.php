<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

/**
 * Class ResultsControllerServiceTest
 */
class ResultsControllerServiceTest extends TestCase
{
    private $class;

    /**
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function setUp()
    {
        parent::setUp();

        Artisan::call('migrate:refresh');

        $this->class = $this->app->build('Classifieds\Services\ResultsControllerService');
    }

    /**
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function testGetFiltersList()
    {
        $keys = ['select', 'range', 'text'];

        $data = $this->class->getFiltersLists();

        $this->assertInternalType('array', $data);
        $this->assertArrayKeysAndItemsTypes($data, $keys, 'array');
    }

    /**
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function testGetSelectFilters()
    {
        $keys = ['cond', 'make', 'model', 'ext_color', 'int_color', 'transm', 'drive_type', 'fuel_type'];

        $data = $this->class->getSelectFilters();

        $this->assertInternalType('array', $data);
        $this->assertArrayKeysAndItemsTypes($data, $keys, 'array');
    }

    /**
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function testGetRangeFilters()
    {
        $keys = ['year', 'price', 'mileage'];

        $data = $this->class->getRangeFilters();

        $this->assertInternalType('array', $data);
        $this->assertArrayKeysAndItemsTypes($data, $keys, 'array');
    }

    /**
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function testGetTextFilters()
    {
        $data = $this->class->getRangeFilters();

        $this->assertInternalType('array', $data);
    }

    /**
     * Test setDefaultForFilters method
     */
    public function testSetDefaultForFilters()
    {
        $expectedData = [[0 => 'Any', 'key' => 'value']];

        $data = $this->class->setDefaultForFilters([['key' => 'value']]);

        $this->assertSame($expectedData, $data);
    }

    /**
     * Test parseFiltersValues method
     */
    public function testParseFiltersValues()
    {
        $input = [
            'text' => [['name' => 'key', 'value' => 'word']],
            'select' => [['name' => 'make', 'value' => 1]],
            'range' => [['name' => 'price', 'value' => [100, 200]]]
        ];

        $expected = [
            'where' => ['disabled' => 0, 'make_id' => 1],
            'whereBetween' => ['price' => [100, 200]],
            'whereLike' => ['key' => 'word'],
            'paginate' => 10
        ];

        $data = $this->class->parseFiltersValues($input);

        $this->assertSame($expected, $data);
    }

}
