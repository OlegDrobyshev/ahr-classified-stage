<?php

/**
 * Class AuthControllerTest
 */
class AuthControllerTest extends TestCase {

    public function tearDown()
    {
        parent::tearDown();

        Mockery::close();
    }

    /**
     * Test login page as guest user
     */
    public function test_login_page_as_guest()
    {
        $this->visit('/login')->assertResponseOk();
    }

    /**
     * Test login page as logged in user
     */
    public function test_login_page_as_logged_in_user()
    {
        $user = factory('Classifieds\Users\User')->make();

        $this->be($user);

        $this->call('GET', '/login');

        $this->assertRedirectedTo('/');
    }

    /**
     * Test register page as guest user
     */
    public function test_register_page_as_guest()
    {
        $this->visit('/register')->assertResponseOk();
    }

    /**
     * Test register page as logged in user
     */
    public function test_register_page_as_logged_in_user()
    {
        $user = factory('Classifieds\Users\User')->make();

        $this->be($user);

        $this->call('GET', '/register');

        $this->assertRedirectedTo('/');
    }

    /**
     * Test logout
     */
    public function test_logout()
    {
        Artisan::call('migrate:refresh');
        $user = factory('Classifieds\Users\User')->create();
        Auth::login($user);

        $response = $this->call('GET', '/logout');

        $this->assertFalse(Auth::check());
        $this->assertTrue($response->isRedirect());
        $this->assertRedirectedTo('/');
    }

    /**
     * Test success login attempt (POST)
     */
    public function test_success_login_attempt()
    {
        Artisan::call('migrate:refresh');

        $password = 'secret';

        $user = factory('Classifieds\Users\User')->create([
            'password' => bcrypt($password)]
        );

        $response = $this->call('POST', '/login', [
            'email' => $user->email,
            'password' => $password
        ]);

        $this->assertTrue(Auth::check(), 'Unable to login.');
        $this->assertTrue($response->isRedirection());
        $this->assertRedirectedTo('/');
    }

    /**
     * Test failed login attempt (POST)
     */
    public function test_failed_login_attempt()
    {
        $response = $this->call('POST', '/login', []);

        $this->assertFalse(Auth::check(), 'Shouldn\'t be able to login.');
        $this->assertTrue($response->isRedirection());
    }

    /**
     * Test success sign up (POST)
     */
    public function test_success_sign_up()
    {
        Artisan::call('migrate:refresh');

        $user = [
            'first_name' => 'John',
            'surname' => 'Doe',
            'email' => 'test@test.com',
            'plan_id' => 1,
            'password' => 'secret',
            'password_confirmation' => 'secret'
        ];

        $response = $this->call('POST', 'register', $user);

        $this->assertEquals(1, Classifieds\Users\User::count());
        $this->assertTrue($response->isRedirection());
    }

    /**
      * Test failed sign up (POST)
      */
    public function test_failed_sign_up()
    {
        Artisan::call('migrate:refresh');

        $response = $this->call('POST', 'register', []);

        $this->assertEquals(0, Classifieds\Users\User::count());
        $this->assertTrue($response->isRedirection());
    }

}