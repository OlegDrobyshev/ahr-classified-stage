<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PasswordController extends TestCase
{
    /**
     * Test reset password page
     */
    public function test_reset_password_page()
    {
        $this->visit('/password/reset')->assertResponseOk();
    }
}
