<?php

/**
 * Class HomeControllerTest
 */
class HomeControllerTest extends TestCase {

    public function tearDown()
    {
        Mockery::close();
    }

    /**
     * Test home page
     */
    public function testHomePage()
    {
        $this->visit('/')->assertResponseOk();
    }

}