<?php

/**
 * Class AdsControllerTest
 */
class AdsControllerTest extends TestCase
{
    function __construct()
    {
        $this->adRepoMock = Mockery::mock('Classifieds\Ads\EloquentAdRepository');
    }

    public function tearDown()
    {
        parent::tearDown();

        Mockery::close();
    }

    /**
     * Test user ads page as logged in user
     */
    public function test_user_ads_as_logged_in_user()
    {
        $user = factory('Classifieds\Users\User')->make();
        $this->be($user);

        $paginator = $this->app->make('Illuminate\Pagination\LengthAwarePaginator', [
            new Illuminate\Database\Eloquent\Collection, 2, 1]
        );
        $this->adRepoMock->shouldReceive('filter')->once()->andReturn($paginator);
        $this->adRepoMock->shouldReceive('getUserAdsCount')->twice();
        $this->app->instance('Classifieds\Ads\EloquentAdRepository', $this->adRepoMock);

        $response = $this->call('GET', '/ads');
//dd($response->getOriginalContent());
        $responseData = $response->getOriginalContent()->getData();

        $this->assertResponseOk();
        $this->assertViewHas('ads');
        $this->assertInstanceOf('Illuminate\Pagination\LengthAwarePaginator', $responseData['ads']);
        $this->assertGreaterThanOrEqual(0, $responseData['ads']->disabledAdsTotal);
        $this->assertGreaterThanOrEqual(0, $responseData['ads']->enabledAdsTotal);
    }
}