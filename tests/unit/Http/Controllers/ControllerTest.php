<?php

/**
 * Class ControllerTest
 */
class ControllerTest extends TestCase {

    /**
     * Test count from collection method
     */
    public function test_count_from_collection_method()
    {
        $mock = Mockery::mock('Classifieds\Ads\Ad');

        $mock->shouldReceive('where')->with('disabled', 1)->once()->andReturnSelf();
        $mock->shouldReceive('where')->with('user_id', 2)->once()->andReturnSelf();
        $mock->shouldReceive('count')->once()->andReturn(1);

        $class = $this->app->make(App\Http\Controllers\Controller::class);

        $params = ['disabled' => 1, 'user_id' => 2];
        $count = $class->getCountFromCollection($mock, $params);

        $this->assertEquals(1, $count);
    }

}