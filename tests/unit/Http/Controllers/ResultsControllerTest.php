<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

/**
 * Class ResultsControllerTest
 */
class ResultsControllerTest extends TestCase
{
    /**
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function setUp()
    {
        parent::setUp();

        Artisan::call('migrate:refresh');
    }

    /**
     * Test result page
     */
    public function test_index()
    {
        $keys = ['active', 'list'];

        $response = $this->call('GET', '/results');
        $data = $response->getOriginalContent()->getData();

        $this->assertResponseOk();
        $this->assertViewHasAll(['filters', 'ads']);
        $this->assertArrayKeysAndItemsTypes($data['filters'], $keys, 'array');
    }

    /**
     * Test getResultsAjax method
     */
    public function testGetResultsAjax()
    {
        $this->prepareAjaxJsonRequest();

        $response = $this->call('POST', '/results-ajax');

        $this->assertResponseOk();
        $this->assertJson($response->content());
    }
}
