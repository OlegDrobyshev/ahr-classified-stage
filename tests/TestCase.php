<?php

class TestCase extends Illuminate\Foundation\Testing\TestCase
{
    /**
     * The base URL to use while testing the application.
     *
     * @var string
     */
    protected $baseUrl = 'http://ahr-classified.app';

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(Illuminate\Contracts\Console\Kernel::class)->bootstrap();

        return $app;
    }

    public function setUp()
    {
        parent::setUp();

        $this->_prepareForTest();
    }

    private function _prepareForTest()
    {
        Session::clear();

        ini_set('display_errors', 1);
        ini_set('display_startup_errors', 1);
        error_reporting(E_ALL);
    }

    protected function prepareAjaxJsonRequest()
    {
        $this->withServerVariables([
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
            'HTTP_CONTENT_TYPE' => 'application/json',
            'HTTP_ACCEPT' => 'application/json'
        ]);
    }

    /**
     * @param $data
     * @param $keys
     * @param null $type
     */
    protected function assertArrayKeysAndItemsTypes($data, $keys, $type = null)
    {
        $this->assertEquals($keys, array_keys($data));

        if ($type)
        {
            foreach ($keys as $key)
            {
                $this->assertInternalType($type, $data[$key]);
            }
        }
    }

    /**
     * @param $response
     */
    protected function assertAndReturnResponseData($response)
    {
        $content = $response->getOriginalContent();

        $this->assertNotNull($content);

        return $content->getData();
    }
}
