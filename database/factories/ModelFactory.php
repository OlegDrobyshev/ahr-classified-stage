<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/**
 * User factory
 */
$factory->define(Classifieds\Users\User::class, function (Faker\Generator $faker) {
    return [
        'first_name' => $faker->firstName,
        'surname' => $faker->lastName,
        'email' => $faker->safeEmail,
        'password' => bcrypt(str_random(10)),
        'type' => 'seller',
        'address' => $faker->streetAddress,
        'city' => $faker->city,
        'state' => $faker->stateAbbr,
        'zip' => $faker->postcode,
        'phone' => $faker->phoneNumber,
        'plan_id' => $faker->numberBetween(1, 10),
        'status' => 1,
        'expires' => $faker->date()
//        'remember_token' => str_random(10),
    ];
});

/**
 * Body Styles
 */
$factory->define(Classifieds\BodyStyles\BodyStyle::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word
    ];
});

/**
 * Plan factory
 */
$factory->define(Classifieds\Plans\Plan::class, function (Faker\Generator $faker) {
    return [
        'sku' => $faker->word,
        'price' => $faker->randomFloat(2, 10, 1000)
    ];
});

/**
 * Gateways factory
 */
$factory->define(Classifieds\Gateways\Gateway::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->word
    ];
});

/**
 * Payment factory
 */
$factory->define(Classifieds\Payments\Payment::class, function (Faker\Generator $faker) {
    return [
        'user_id' => $faker->numberBetween(1, 6),
        'plan_id' => 2,
        'amount' => $faker->randomFloat(2, 10, 1000),
        'gateway_id' => 1,
        'status' => $faker->numberBetween(0, 1),
        'created_at' => $faker->dateTimeThisDecade
    ];
});

/**
 * Ad factory
 */
$factory->define(Classifieds\Ads\Ad::class, function (Faker\Generator $faker) {
    return [
        'user_id' => $faker->randomDigitNotNull,
        'cond_id' => $faker->randomDigitNotNull,
        'make_id' => $faker->randomDigitNotNull,
        'model_id' => $faker->randomDigitNotNull,
        'year' => $faker->year,
        'price' => $faker->numberBetween(100, 100000),
        'body_style_id' => $faker->numberBetween(1, 5),
        'mileage' => $faker->numberBetween(100, 10000),
        'ext_color_id' => $faker->randomDigitNotNull,
        'int_color_id' => $faker->randomDigitNotNull,
        'transm_id' => $faker->randomDigitNotNull,
        'drive_type_id' => $faker->randomDigitNotNull,
        'doors' => $faker->numberBetween(1, 10),
        'fuel_type_id' => $faker->randomDigitNotNull,
        'vin' => $faker->regexify('[A-Z0-9]{11,17}'),
        'address' => $faker->streetAddress,
        'city' => $faker->city,
        'state' => $faker->stateAbbr,
        'zip' => $faker->postcode,
        'phone' => $faker->phoneNumber,
        'desc' => $faker->text(700),
        'disabled' => $faker->numberBetween(0, 1),
        'viewed_by_admin' => rand(0, 1),
        'created_at' => $faker->dateTimeThisDecade,
        'updated_at' => $faker->dateTimeThisMonth,
    ];
});

/**
 * Ad Photo factory
 */
$factory->define(Classifieds\AdsPhotos\AdPhoto::class, function (Faker\Generator $faker) {
    return [
        'ad_id' => $faker->randomNumber(),
        'uri' => $faker->url,
        'desc' => $faker->text(30),
        'mime' => 'image/jpeg',
        'size' => $faker->randomNumber(),
        'created_at' => $faker->dateTimeThisDecade,
        'updated_at' => $faker->dateTimeThisMonth,
    ];
});

/**
 * Ad Video factory
 */
$factory->define(Classifieds\AdsVideos\AdVideo::class, function (Faker\Generator $faker) {
    $providers = ['youtube', 'vimeo'];

    return [
        'ad_id' => $faker->randomNumber(),
        'provider' => $providers[array_rand($providers)],
        'vid' => $faker->numberBetween(00000000, 99999999999),
        'url' => $faker->url,
        'image' => $faker->url,
        'created_at' => $faker->dateTimeThisDecade,
        'updated_at' => $faker->dateTimeThisMonth,
    ];
});

/**
 * Condition factory
 */
$factory->define(Classifieds\Conditions\Condition::class, function (Faker\Generator $faker) {
    $conditions = ['New', 'Used', 'Certified', 'Classic'];

    return [
        'name' => $conditions[array_rand($conditions)]
    ];
});

/**
 * Make factory
 */
$factory->define(Classifieds\Makes\Make::class, function (Faker\Generator $faker) {
   return [
       'name' => ucfirst($faker->word)
   ];
});

/**
 * Model factory
 */
$factory->define(Classifieds\Models\Model::class, function (Faker\Generator $faker) {
    return [
        'make_id' => $faker->numberBetween(1, 40),
        'name' => ucfirst($faker->word)
    ];
});

/**
 * Color factory
 */
$factory->define(Classifieds\Colors\Color::class, function (Faker\Generator $faker) {
    return [
        'name' => ucfirst($faker->colorName)
    ];
});

/**
 * Fuel type factory
 */
$factory->define(Classifieds\FuelTypes\FuelType::class, function (Faker\Generator $faker) {
    return [
        'name' => ucfirst($faker->word)
    ];
});

/**
 * Transmissison factory
 */
$factory->define(Classifieds\Transmissions\Transmission::class, function (Faker\Generator $faker) {
    return [
        'name' => ucfirst($faker->word)
    ];
});

/**
 * Drive type factory
 */
$factory->define(Classifieds\DriveTypes\DriveType::class, function (Faker\Generator $faker) {
    return [
        'name' => ucfirst($faker->word)
    ];
});