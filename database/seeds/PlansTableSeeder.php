<?php

/**
 * Class PlansTableSeeder
 */
class PlansTableSeeder extends BaseSeeder
{
    /**
     * @var array
     */
    private $plans = [
        'TRIAL' => 0,
        'PAID' => 9.99
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->plans as $plan => $price)
        {
            factory(Classifieds\Plans\Plan::class)->create([
                'sku' => $plan,
                'price' => $price
            ]);
        }
    }
}
