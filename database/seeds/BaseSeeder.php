<?php

use Illuminate\Database\Seeder;

class BaseSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // TODO: Implement run() method.
    }

    /**
     * @param $table
     * @param $key
     * @return mixed
     */
    protected function _getRandomValueFromDB($table, $key)
    {
        return DB::table($table)->orderByRaw("RAND()")->get()[0]->{$key};
    }
}