<?php

use Illuminate\Database\Seeder;

/**
 * Class BodyStylesTableSeeder
 */
class BodyStylesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Classifieds\BodyStyles\BodyStyle::class, 5)->create();
    }
}
