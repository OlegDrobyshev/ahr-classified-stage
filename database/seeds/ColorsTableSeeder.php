<?php

/**
 * Class ColorsTableSeeder
 */
class ColorsTableSeeder extends BaseSeeder
{
    /**
     * @var array
     */
    private $colors = [
        'Beige', 'Black', 'Blue', 'Bronze', 'Brown', 'Gold', 'Grey', 'Green', 'Orange',
        'Pink', 'Purple', 'Red', 'Silver', 'Turquoise', 'White', 'Yellow'];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->colors as $color)
        {
            factory(Classifieds\Colors\Color::class)->create([
                'name' => $color
            ]);
        }
    }
}
