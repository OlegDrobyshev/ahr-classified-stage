<?php

use Illuminate\Database\Seeder;

class ConditionsTableSeeder extends Seeder
{
    /**
     * @var array
     */
    private $conditions = ['New', 'Used', 'Certified', 'Classic'];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->conditions as $condition)
        {
            factory(Classifieds\Conditions\Condition::class)->create([
                'name' => $condition
            ]);
        }
    }
}
