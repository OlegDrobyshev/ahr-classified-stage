<?php

/**
 * Class FuelTypesTableSeeder
 */
class FuelTypesTableSeeder extends BaseSeeder
{
    /**
     * @var array
     */
    private $types = ['Gasoline', 'Diesel', 'Gas', 'Flexible-Fuel', 'Hybrid', 'Electric', 'Alternative'];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->types as $type)
        {
            factory(Classifieds\FuelTypes\FuelType::class)->create([
                'name' => $type
            ]);
        }
    }
}
