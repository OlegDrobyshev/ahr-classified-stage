<?php

/**
 * Class ModelsTableSeeder
 */
class ModelsTableSeeder extends BaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Classifieds\Models\Model::class, 10)->create();
    }
}
