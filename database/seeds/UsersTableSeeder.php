<?php

class UsersTableSeeder extends BaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory('Classifieds\Users\User')->create([
            'email' => 'oleg.develop@gmail.com',
            'password' => 'emka37eu',
            'type' => 'seller',
            'plan_id' => $this->_getRandomValueFromDB('plans', 'plan_id')
        ]);

        factory('Classifieds\Users\User')->create([
            'email' => 'admin@admin.com',
            'password' => 'emka37eu',
            'type' => 'admin',
            'plan_id' => $this->_getRandomValueFromDB('plans', 'plan_id')
        ]);

        factory(Classifieds\Users\User::class, 4)->create([
            'plan_id' => $this->_getRandomValueFromDB('plans', 'plan_id')
        ]);
    }
}
