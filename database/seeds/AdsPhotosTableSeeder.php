<?php

/**
 * Class AdsPhotosTableSeeder
 */
class AdsPhotosTableSeeder extends BaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 10; $i++)
        {
            factory(Classifieds\AdsPhotos\AdPhoto::class)->create([
                'ad_id' => $this->_getRandomValueFromDB('ads', 'ad_id')
            ]);
        }
    }
}
