<?php

/**
 * Class DriveTypesTableSeeder
 */
class DriveTypesTableSeeder extends BaseSeeder
{
    /**
     * @var array
     */
    private $types = ['FWD', '4WD', 'AWD', 'RWD', '2WD'];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->types as $type)
        {
            factory(Classifieds\DriveTypes\DriveType::class)->create([
                'name' => $type
            ]);
        }
    }
}
