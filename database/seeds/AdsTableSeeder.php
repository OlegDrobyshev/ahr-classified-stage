<?php

/**
 * Class AdsTableSeeder
 */
class AdsTableSeeder extends BaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 20; $i++)
        {
            factory(Classifieds\Ads\Ad::class)->create([
                'user_id' => $this->_getRandomValueFromDB('users', 'user_id'),
                'cond_id' => $this->_getRandomValueFromDB('conditions', 'cond_id'),
                'make_id' => $this->_getRandomValueFromDB('makes', 'make_id'),
                'model_id' => $this->_getRandomValueFromDB('models', 'model_id'),
                'ext_color_id' => $this->_getRandomValueFromDB('colors', 'color_id'),
                'int_color_id' => $this->_getRandomValueFromDB('colors', 'color_id'),
                'fuel_type_id' => $this->_getRandomValueFromDB('fuel_types', 'fuel_type_id'),
                'transm_id' => $this->_getRandomValueFromDB('transmissions', 'transm_id'),
                'drive_type_id' => $this->_getRandomValueFromDB('drive_types', 'drive_type_id')
            ]);
        }
    }
}
