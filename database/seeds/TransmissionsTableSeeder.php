<?php

/**
 * Class TransmissionsTableSeeder
 */
class TransmissionsTableSeeder extends BaseSeeder
{
    /**
     * @var array
     */
    private $types = ['Manual', 'Automatic', 'Semi-Automatic', 'CVT', 'DCT'];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->types as $type)
        {
            factory(Classifieds\Transmissions\Transmission::class)->create([
                'name' => $type
            ]);
        }
    }
}
