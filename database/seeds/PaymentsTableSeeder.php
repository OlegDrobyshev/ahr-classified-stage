<?php

use Illuminate\Database\Seeder;

/**
 * Class PaymentsTableSeeder
 */
class PaymentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Classifieds\Payments\Payment::class, 100)->create();
    }
}
