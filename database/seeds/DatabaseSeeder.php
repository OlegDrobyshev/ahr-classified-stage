<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(PlansTableSeeder::class);

        $this->call(UsersTableSeeder::class);

        $this->call(ConditionsTableSeeder::class);

        $this->call(MakesTableSeeder::class);

        $this->call(ModelsTableSeeder::class);

        $this->call(BodyStylesTableSeeder::class);

        $this->call(ColorsTableSeeder::class);

        $this->call(FuelTypesTableSeeder::class);

        $this->call(TransmissionsTableSeeder::class);

        $this->call(DriveTypesTableSeeder::class);

        $this->call(AdsTableSeeder::class);

        $this->call(GatewaysTableSeeder::class);

        $this->call(PaymentsTableSeeder::class);

        $this->call(AdsPhotosTableSeeder::class);

        $this->call(AdsVideosTableSeeder::class);
    }
}
