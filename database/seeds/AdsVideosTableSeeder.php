<?php

class AdsVideosTableSeeder extends BaseSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 10; $i++)
        {
            factory(Classifieds\AdsVideos\AdVideo::class)->create([
                'ad_id' => $this->_getRandomValueFromDB('ads', 'ad_id')
            ]);
        }
    }
}
