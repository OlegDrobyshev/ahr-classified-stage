<?php

/**
 * Class MakesTableSeeder
 */
class MakesTableSeeder extends BaseSeeder
{
    private $makes = [
        'Acura',
        'Alfa Romeo',
        'Aston Martin',
        'Audi',
        'BMW',
        'Bentley',
        'Buick',
        'Cadillac',
        'Chevrolet',
        'Chrysler',
        'Dodge',
        'Ferrari',
        'Fiat',
        'Ford',
        'Freightliner',
        'GMC',
        'Honda',
        'Hyundai',
        'Infiniti',
        'Jaguar',
        'Jeep',
        'Kia',
        'Lamborghini',
        'Land Rover',
        'Lexus',
        'Lincoln',
        'Lotus',
        'Maserati',
        'Mazda',
        'Mclaren',
        'Mercedes-Benz',
        'Mini',
        'Mitsubishi',
        'Mobility Ventures',
        'Nissan',
        'Porsche',
        'Ram',
        'Rolls-Royce',
        'Scion',
        'Smart',
        'Subaru',
        'Toyota',
        'Volkswagen',
        'Volvo'
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->makes as $make)
        {
            factory(Classifieds\Makes\Make::class)->create([
                'name' => $make
            ]);
        }
    }
}
