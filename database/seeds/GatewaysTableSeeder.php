<?php

use Illuminate\Database\Seeder;

/**
 * Class GatewaysTableSeeder
 */
class GatewaysTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory('Classifieds\Gateways\Gateway')->create([
            'name' => 'Authorize.Net SIM'
        ]);
    }
}
