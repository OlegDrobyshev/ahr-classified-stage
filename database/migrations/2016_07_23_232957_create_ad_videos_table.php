<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ads_videos', function (Blueprint $table) {
            $table->increments('video_id');
            $table->integer('ad_id')->unsigned()->nullable()->index();
            $table->string('provider', 20);
            $table->string('vid', 50);
            $table->string('url');
            $table->string('image');
            $table->timestamps();
        });

        Schema::table('ads_videos', function (Blueprint $table)
        {
            $table->foreign('ad_id')->references('ad_id')->on('ads')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ads_videos');
    }
}
