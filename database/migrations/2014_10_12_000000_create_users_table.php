<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('user_id');
            $table->string('first_name');
            $table->string('surname');
            $table->string('email')->unique();
            $table->string('password', 60);
            $table->string('type', 10)->index();
            $table->string('address')->index();
            $table->string('city')->index();
            $table->string('state')->index();
            $table->string('zip')->index();
            $table->string('phone')->index();
            $table->integer('plan_id')->unsigned()->nullable()->index();
            $table->tinyInteger('status', 0)->index()->comment('0 - disabled, 1 - enabled, 2 - expired');
            $table->date('expires')->index();
            $table->rememberToken();
            $table->timestamps();
        });

        Schema::table('users', function (Blueprint $table) {
            $table->foreign('plan_id')
                ->references('plan_id')->on('plans')
                ->onDelete('restrict')
                ->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
