<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdsPhotosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ads_photos', function (Blueprint $table) {
            $table->increments('photo_id');
            $table->integer('ad_id')->unsigned()->nullable()->index();
            $table->string('uri');
            $table->string('desc');
            $table->string('mime');
            $table->integer('size');
            $table->timestamps();
        });

        Schema::table('ads_photos', function (Blueprint $table)
        {
            $table->foreign('ad_id')->references('ad_id')->on('ads')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ads_photos');
    }
}
