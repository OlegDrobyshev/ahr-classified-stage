<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ads', function (Blueprint $table) {
            $table->increments('ad_id');
            $table->integer('user_id')->unsigned()->nullable()->index();
            $table->integer('cond_id')->unsigned()->nullable()->index();
            $table->integer('make_id')->unsigned()->nullable()->index();
            $table->integer('model_id')->unsigned()->nullable()->index();
            $table->integer('year')->index();
            $table->integer('price')->index();
            $table->integer('body_style_id')->unsigned()->nullable()->index();
            $table->integer('mileage')->index();
            $table->integer('ext_color_id')->unsigned()->nullable()->index();
            $table->integer('int_color_id')->unsigned()->nullable()->index();
            $table->integer('transm_id')->unsigned()->nullable()->index();
            $table->integer('drive_type_id')->unsigned()->nullable()->index();
            $table->integer('doors')->index();
            $table->integer('fuel_type_id')->unsigned()->nullable()->index();
            $table->string('vin')->index();
            $table->string('address')->index();
            $table->string('city')->index();
            $table->string('state')->index();
            $table->string('zip')->index();
            $table->string('phone')->index();
            $table->text('desc');
            $table->tinyInteger('disabled', 0);
            $table->tinyInteger('viewed_by_admin', 0);
            $table->timestamps();
        });

        Schema::table('ads', function (Blueprint $table) {
            $table->foreign('user_id')->references('user_id')->on('users')->onDelete('cascade');
            $table->foreign('cond_id')->references('cond_id')->on('conditions')->onDelete('set null');
            $table->foreign('make_id')->references('make_id')->on('makes')->onDelete('set null');
            $table->foreign('model_id')->references('model_id')->on('models')->onDelete('set null');
            $table->foreign('ext_color_id')->references('color_id')->on('colors')->onDelete('set null');
            $table->foreign('int_color_id')->references('color_id')->on('colors')->onDelete('set null');
            $table->foreign('drive_type_id')->references('drive_type_id')->on('drive_types')->onDelete('set null');
            $table->foreign('transm_id')->references('transm_id')->on('transmissions')->onDelete('set null');
            $table->foreign('fuel_type_id')->references('fuel_type_id')->on('fuel_types')->onDelete('set null');
            $table->foreign('body_style_id')->references('body_style_id')->on('body_styles')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ads');
    }
}
